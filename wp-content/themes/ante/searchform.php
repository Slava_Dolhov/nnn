<form action="<?php echo site_url() ?>" method="get" id="search-bar">
	<input type="text" placeholder="<?php _e('Enter search terms', 'Ante');?>" name="s" id="search" value="<?php the_search_query(); ?>">
	<button type="submit" id="search-submit" name="search"><i class="fa fa-mail-forward"></i></button>
</form>