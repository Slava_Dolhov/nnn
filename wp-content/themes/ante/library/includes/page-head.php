<?php
global $get_meta, $post;
if (empty($get_meta)) $get_meta = get_post_custom($post->ID);	
if (empty($get_meta)){
	$pageId = get_page_ID_by_page_template('homepage-template.php');
	$get_meta = get_post_custom($pageId);	
}

if(isset( $get_meta['_weblusive_post_head'][0]) && $get_meta['_weblusive_post_head'][0] != 'none' ):
		
	$orig_post = $post;
	
	if(isset( $get_meta['_weblusive_post_head'][0]) && $get_meta['_weblusive_post_head'][0] == 'promotext' ){
		if( !empty( $get_meta["_weblusive_promotext"][0] ) ){
			$promos = explode('*', $get_meta["_weblusive_promotext"][0]);
			$promonumber = count ($promos);
			?>
			<div id="banner">
				<div class="container">
					<ul class="bxslider">
						<?php for ($i=0; $i<$promonumber; $i++):?>
							<li>
								<p><?php echo  $promos[$i] ?></p>
							</li>
						<?php endfor?>
					</ul>
					<div class="clearfix"></div>
				</div>
			</div>
		<?php }
	}
	elseif(isset( $get_meta['_weblusive_post_head'][0]) && $get_meta['_weblusive_post_head'][0] == 'thumb' || (isset( $get_meta['_weblusive_post_head'][0]) && empty( $get_meta['_weblusive_post_head'][0] ) && weblusive_get_option( 'post_featured' ) ) ){
		?><div class="single-post-thumb">
			<?php the_post_thumbnail('full', array('class'=>'page-thumb') ); ?> 
		</div>
		<?php $thumb_caption = get_post(get_post_thumbnail_id())->post_excerpt;
			if( !empty($thumb_caption) ){ ?><div class="single-post-caption"><?php echo $thumb_caption ?></div> <?php 
		} 
	}
	elseif(isset( $get_meta['_weblusive_post_head'][0]) && $get_meta['_weblusive_post_head'][0] == 'lightbox' && has_post_thumbnail($post->ID)){

		$image_id = get_post_thumbnail_id($post->ID);  
		$image_url = wp_get_attachment_image_src($image_id,'large');  
		$image_url = $image_url[0];
		
		?>
		<style type="text/css">
			.content-dedicated #page-header{
				background: url(<?php echo $image_url; ?>) no-repeat center center fixed !important; 
				-webkit-background-size: cover !important;
				-moz-background-size: cover !important;
				-o-background-size: cover !important;
				background-size: cover !important;
			}
		</style>
		<div class="single-post-thumb head-lightbox">
			<a href="<?php echo $image_url; ?>" data-rel="prettyPhoto"><?php the_post_thumbnail('medium'); ?></a>
		</div>
		<?php $thumb_caption = get_post(get_post_thumbnail_id())->post_excerpt;
			if( !empty($thumb_caption) ){ ?><div class="single-post-caption"><?php echo $thumb_caption ?></div> <?php }
	}
	elseif(isset( $get_meta['_weblusive_post_head'][0]) && !empty( $get_meta['_weblusive_post_slider'][0] ) ){
	
		$speed = weblusive_get_option( 'slit_slider_speed' );
		$time = weblusive_get_option( 'slit_slider_time' );
	
		if( !$speed || $speed == ' ' || !is_numeric($speed)) $speed = 5000;
		if( !$time || $time == ' ' || !is_numeric($time))	$time = 500;
		
		$custom_slider_args = array( 'post_type' => 'weblusive_slider', 'p' => $get_meta['_weblusive_post_slider'][0] );
		$custom_slider = new WP_Query( $custom_slider_args );
		
		while ( $custom_slider->have_posts() ) : $custom_slider->the_post();
			$custom = get_post_custom($post->ID);
			$slider = isset ($custom["custom_slider"]) ? unserialize( $custom["custom_slider"][0]) : '';
			$number = count($slider);
				
			if( $slider ):?>
				<!--<script type="text/javascript">
					jQuery('#home-slider').flexslider({
						animation: "fade",
						controlNav: true,
						directionNav: false,
						slideshowSpeed: <?php //echo $speed?>,
						animationSpeed: <?php //echo $time?>
					});
				</script>-->
				<div class="home-text" data-0="top: 0%; opacity: 1" data-500="top: 50%; opacity: 0.1">
                    <div id="home-slider" class="flexslider">
						<ul class="slides">
							<?php foreach( $slider as $slide ): ?>
								<?php $image =  wp_get_attachment_image_src( $slide['id'], 'full'); ?>
								<li>
									<span class="home-text-header"><?php if( !empty( $slide['title'] ))  echo stripslashes( $slide['title'] )  ?></span>
									<span class="big-text"><?php if( !empty( $slide['caption'] ) ):?><?php echo stripslashes($slide['caption']) ; ?><?php endif; ?></span>
								</li>
							<?php endforeach; ?>
						</ul>
					</div>
					<div class="clearfix"></div>
				</div>
			<?php 
			else:
				?><p class="warning"><i class="icon-warning-sign"></i><?php _e('No slider items were found in the selected slider. Please make sure to create some via "Slider" section in your admin panel.', 'Ante');?></p><?php
			endif;
		endwhile;  ?>
	<?php }

	$post = $orig_post;
	wp_reset_query();
	
 endif; ?>
