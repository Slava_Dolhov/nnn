(function() {

	tinymce.create('tinymce.plugins.Addshortcodes', {
		init : function(ed, url) {
		
			//Add Panel
			ed.addButton('AddPanel', {
				title : 'Add Promo block',
				cmd : 'alc_panel',
				image : url + '/images/panel.png'
			});
			ed.addCommand('alc_panel', function() {
				ed.windowManager.open({file : url + '/ui.php?page=panel',width : 600 ,	height : 450 ,	inline : 1});
			});	
			
			//Add Team member
			ed.addButton('Teammember', {
				title : 'Add Team member',
				cmd : 'alc_teammember',
				image : url + '/images/teammember.png'
			});
			ed.addCommand('alc_teammember', function() {
				ed.windowManager.open({file : url + '/ui.php?page=teammember',width : 600 ,	height : 450 ,	inline : 1});
			});	
			
			//Add Section
			ed.addButton('Section', {
				title : 'Add Section',
				cmd : 'alc_section',
				image : url + '/images/section.png'
			});
			ed.addCommand('alc_section', function() {
				ed.windowManager.open({file : url + '/ui.php?page=section',width : 600, height : 450 ,	inline : 1});
			});	
			
			//Add Timeline
			ed.addButton('Timeline', {
				title : 'Add Timeline',
				cmd : 'alc_timeline',
				image : url + '/images/timeline.png'
			});
			ed.addCommand('alc_timeline', function() {
				ed.windowManager.open({file : url + '/ui.php?page=Timeline',width : 600, height : 450 ,	inline : 1});
			});	
			
			//Add Section
			ed.addButton('Counter', {
				title : 'Add counter',
				cmd : 'alc_counter',
				image : url + '/images/counter.png'
			});
			ed.addCommand('alc_counter', function() {
				ed.windowManager.open({file : url + '/ui.php?page=Counter',width : 600, height : 450 ,	inline : 1});
			});	
			
			//Carousel
			ed.addButton('Carousel', {
				title : 'Add carousel content slider',
				cmd : 'alc_carousel',
				image : url + '/images/carousel.png'
			});
			ed.addCommand('alc_carousel', function() {
				ed.windowManager.open({file : url + '/ui.php?page=carousel',width : 600 ,	height : 350 ,	inline : 1});
			});
			//Add Service 
			ed.addButton('serviceblock', {
				title : 'Add Services',
				cmd : 'alc_servicegroup',
				image : url + '/images/service.png'
			});
			ed.addCommand('alc_servicegroup', function() {
				ed.windowManager.open({file : url + '/ui.php?page=serviceblock',width : 600 ,	height : 450 ,	inline : 1});
			});	
                        
            //Add Progress bar
			ed.addButton('Progress', {
				title : 'Add Progress bar',
				cmd : 'alc_progress',
				image : url + '/images/progress.png'
			});
			ed.addCommand('alc_progress', function() {
				ed.windowManager.open({file : url + '/ui.php?page=progress',width : 600 ,	height : 375 ,	inline : 1});
			});
                        
            //Add Dropdown Buttons
			ed.addButton('Dropdown', {
				title : 'Add Dropdown Button',
				cmd : 'alc_dropdown',
				image : url + '/images/dropdown.png'
			});
			ed.addCommand('alc_dropdown', function() {
				ed.windowManager.open({file : url + '/ui.php?page=dropdown',width : 600 ,	height : 375 ,	inline : 1});
			});
			
			//AddButtons
			ed.addButton('AddButton', {
				title : 'Add Button',
				cmd : 'alc_button',
				image : url + '/images/button.png'
			});
			ed.addCommand('alc_button', function() {
				ed.windowManager.open({file : url + '/ui.php?page=button',width : 600 ,	height : 450 ,	inline : 1});
			});
			
			
			//Add Tabs
			ed.addButton('Tabs', {
				title:' Add Tabs',
				image:url+'/images/tabs.png',
				cmd:'alc_tabs'
			});
			ed.addCommand('alc_tabs', function(){
				ed.windowManager.open({file:url+'/ui.php?page=tabs', width:600, height:350, inline:1});
			});
			
			//Add Horizontal Navigation
			ed.addButton('Horizontal navigation', {
				title : 'Add Horizontal navigation',
				cmd : 'alc_hornav',
				image : url + '/images/hornav.png'
			});
			ed.addCommand('alc_hornav', function() {
				ed.windowManager.open({file : url + '/ui.php?page=hornav',width : 600 ,	height : 375 ,	inline : 1});
			});
           
			//Add Toggle
			ed.addButton('Toggle', {
				title : 'Add Toggle Block',
				cmd : 'alc_toggle',
				image : url + '/images/toggle.png'
			});
			ed.addCommand('alc_toggle', function() {
				ed.windowManager.open({file : url + '/ui.php?page=toggle',width : 600 ,	height : 375 ,	inline : 1});
			});
			
			//Add Accordion
			ed.addButton('Accordion', {
				title : 'Add Accordion Block',
				cmd : 'alc_accordion',
				image : url + '/images/accordion.png'
			});
			ed.addCommand('alc_accordion', function() {
				ed.windowManager.open({file : url + '/ui.php?page=accordion',width : 600 ,	height : 375 ,	inline : 1});
			});
                        
			//Add Testimonial
			ed.addButton('Testimonial', {
				title : 'Add Testimonial',
				cmd : 'alc_testimonial',
				image : url + '/images/testimonial.png'
			});
			ed.addCommand('alc_testimonial', function() {
				ed.windowManager.open({file : url + '/ui.php?page=testimonial',width : 600 ,	height : 420 ,	inline : 1});
			});
                        
			//Add alert box
			ed.addButton('Alert', {
				title : 'Add alert box',
				cmd : 'alc_alert',
				image : url + '/images/alert.png'
			});
			ed.addCommand('alc_alert', function() {
				ed.windowManager.open({file : url + '/ui.php?page=alert',width : 600 ,	height : 400 ,	inline : 1});
			});
			

			//Add Video
			ed.addButton('Video', {
				title : 'Add video',
				cmd : 'alc_video',
				image : url + '/images/video.png'
			});
			ed.addCommand('alc_video', function() {
				ed.windowManager.open({file : url + '/ui.php?page=video',width : 600 ,	height : 260 ,	inline : 1});
			});

			//Add Twitter
			ed.addButton('Twitter', {
				title : 'Add Twitter listing',
				cmd : 'alc_twitter',
				image : url + '/images/twitter.png'
			});
			ed.addCommand('alc_twitter', function() {
				ed.windowManager.open({file : url + '/ui.php?page=twitter',width : 600 ,	height : 375 ,	inline : 1});
			});
                        
			//Add Slider
			ed.addButton('Slider', {
				title : 'Add Slider',
				cmd : 'alc_slider',
				image : url + '/images/slider.png'
			});
			ed.addCommand('alc_slider', function() {
				ed.windowManager.open({file : url + '/ui.php?page=slider',width : 600 ,	height : 375 ,	inline : 1});
			});

			
			//Featured block
			ed.addButton('Fblock',{
					title:'Add featured block',
					image: url+'/images/fblock.png',
					cmd: 'alc_fblock'
			});
			ed.addCommand('alc_fblock',function(){
				ed.windowManager.open({file:url+'/ui.php?page=fblock', width:600, height:350, inline:1});
			});
			
			//add Title block
			ed.addButton('Tblock', {
				title: 'Title block',
				image:url+'/images/tblock.png',
				cmd: 'alc_tblock'
			});
			ed.addCommand('alc_tblock', function(){
				ed.windowManager.open({file:url+'/ui.php?page=tblock', width:600, height:400, inline:1});
			});
			
			//Add Reveal box
			ed.addButton('Reveal', {
				title: 'Add modal box',
				image: url+'/images/reveal.png',
				cmd:'alc_reveal'
			});
			ed.addCommand('alc_reveal', function(){
				ed.windowManager.open({file:url+'/ui.php?page=reveal', width:600, height:400, inline:1});
			});
			
			
                        
			//Social Buttons
			 ed.addButton('SocialButton', {
				title:' Add Social Button',
				image:url+'/images/social.png',
				cmd:'alc_social'
			});
			ed.addCommand('alc_social', function(){
				ed.windowManager.open({file:url+'/ui.php?page=social', width:600, height:350, inline:1});
			});
			
			//Add Side Navigation
			ed.addButton('Sidenav', {
				title:'Add Side Navigation',
				image:url+'/images/sidenav.png',
				cmd:'alc_sidenav'
			});
			ed.addCommand('alc_sidenav', function(){
				ed.windowManager.open({file:url+'/ui.php?page=sidenav', width:600, height:350, inline:1});
			});
		                	
		},
		getInfo : function() {
			return {
				longname : "Weblusive Shortcodes",
				author : 'Weblusive',
				authorurl : 'http://www.weblusive.com/',
				infourl : 'http://www.weblusive.com/',
				version : "1.0"
			};
		}
	});
	tinymce.PluginManager.add('AnteShortcodes', tinymce.plugins.Addshortcodes);	
	
})();

