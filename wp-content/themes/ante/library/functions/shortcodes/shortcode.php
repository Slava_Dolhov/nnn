<?php
define ( 'JS_PATH' , get_template_directory_uri().'/library/functions/shortcodes/shortcode.js');


add_action('admin_head','html_quicktags');
function html_quicktags() {

	$output = "<script type='text/javascript'>\n
	/* <![CDATA[ */ \n";
	wp_print_scripts( 'quicktags' );

	$buttons = array();
	
	/*$buttons[] = array(
		'name' => 'raw',
		'options' => array(
			'display_name' => 'raw',
			'open_tag' => '\n[raw]',
			'close_tag' => '[/raw]\n',
			'key' => ''
	));*/
	
	$buttons[] = array(
		'name' => 'one_whole',
		'options' => array(
			'display_name' => 'Full width',
			'open_tag' => '\n[one_whole]',
			'close_tag' => '[/one_whole]\n',
			'key' => ''
	));
	
	$buttons[] = array(
		'name' => 'one_third',
		'options' => array(
			'display_name' => 'one third',
			'open_tag' => '\n[one_third]',
			'close_tag' => '[/one_third]\n',
			'key' => ''
	));
		
	$buttons[] = array(
		'name' => 'two_third',
		'options' => array(
			'display_name' => 'two third',
			'open_tag' => '\n[two_third]',
			'close_tag' => '[/two_third]\n',
			'key' => ''
	));	
	
	$buttons[] = array(
		'name' => 'one_half',
		'options' => array(
			'display_name' => 'one half',
			'open_tag' => '\n[one_half]',
			'close_tag' => '[/one_half]\n',
			'key' => ''
	));	
	
	$buttons[] = array(
		'name' => 'one_fourth',
		'options' => array(
			'display_name' => 'one fourth',
			'open_tag' => '\n[one_fourth]',
			'close_tag' => '[/one_fourth]\n',
			'key' => ''
	));	
	
	$buttons[] = array(
		'name' => 'three_fourth',
		'options' => array(
			'display_name' => 'three fourth',
			'open_tag' => '\n[three_fourth]',
			'close_tag' => '[/three_fourth]\n',
			'key' => ''
	));
	
	$buttons[] = array(
		'name' => 'one_sixth',
		'options' => array(
			'display_name' => 'one sixth',
			'open_tag' => '\n[one_sixth]',
			'close_tag' => '[/one_sixth]\n',
			'key' => ''
	));
	
	$buttons[] = array(
		'name' => 'five_twelveth',
		'options' => array(
			'display_name' => 'five twelveth',
			'open_tag' => '\n[five_twelveth]',
			'close_tag' => '[/five_twelveth]\n',
			'key' => ''
	));
	
	$buttons[] = array(
		'name' => 'seven_twelveth',
		'options' => array(
			'display_name' => 'seven twelveth',
			'open_tag' => '\n[seven_twelveth]',
			'close_tag' => '[/seven_twelveth]\n',
			'key' => ''
	));
        
        $buttons[] = array(
		'name' => 'one_twelveth',
		'options' => array(
			'display_name' => 'one twelveth',
			'open_tag' => '\n[one_twelveth]',
			'close_tag' => '[/one_twelveth]\n',
			'key' => ''
	));
        
        $buttons[] = array(
		'name' => 'eleven_twelveth',
		'options' => array(
			'display_name' => 'eleven twelveth',
			'open_tag' => '\n[eleven_twelveth]',
			'close_tag' => '[/eleven_twelveth]\n',
			'key' => ''
	));
        
        $buttons[] = array(
		'name' => 'five_sixth',
		'options' => array(
			'display_name' => 'five sixth',
			'open_tag' => '\n[five_sixth]',
			'close_tag' => '[/five_sixth]\n',
			'key' => ''
	));
	
	$buttons[] = array(
		'name' => 'row',
		'options' => array(
			'display_name' => 'Insert Row',
			'open_tag' => '\n[row]',
			'close_tag' => '[/row]\n',
			'key' => ''
	));
	
	$buttons[] = array(
		'name' => 'clear',
		'options' => array(
			'display_name' => 'Clear Float',
			'open_tag' => '[clear /]',
			'close_tag' => '',
			'key' => ''
	));
			
	for ($i=0; $i <= (count($buttons)-1); $i++) {
		$output .= "edButtons[edButtons.length] = new edButton('ed_{$buttons[$i]['name']}'
			,'{$buttons[$i]['options']['display_name']}'
			,'{$buttons[$i]['options']['open_tag']}'
			,'{$buttons[$i]['options']['close_tag']}'
			,'{$buttons[$i]['options']['key']}'
		); \n";
	}
	
	$output .= "\n /* ]]> */ \n
	</script>";
	echo $output;
}

function Ante_addbuttons() {
	if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') )
		return;

	if ( get_user_option('rich_editing') == 'true') {
		add_filter("mce_external_plugins", "add_alc_custom_tinymce_plugin");
		add_filter('mce_buttons_3', 'register_alc_custom_button');
	}
}
function register_alc_custom_button($buttons) {
	array_push(
		$buttons,
		"AddButton",
		"Dropdown",
		"|",
		"Section",
		"Tabs",
		"Toggle",
		"Accordion",
		"|",
		"Timeline",
		"Counter",
		"|",
		"Horizontal navigation",
		"Sidenav",
		"|",
		"Slider",
		"Carousel",
		"|",
		"AddPanel",
        "Progress",
		"Fblock",
		"serviceblock",
		"Testimonial",
		"Teammember",	
		"Tblock",		
		"|",		
		"Alert",
		"Reveal",
		"Video",
		"|",	
		"SocialButton", 
		"Twitter"
		); 
	return $buttons;
} 
function add_alc_custom_tinymce_plugin($plugin_array) {
	$plugin_array['AnteShortcodes'] = JS_PATH;
	return $plugin_array;
}
add_action('init', 'Ante_addbuttons');




/********************* PANEL **********************/

function alc_panel( $atts, $content = null ) {
 extract(shortcode_atts(array(
		"icon" => '' 
	), $atts));	
	$out = '<section class="quote promo-box"><blockquote>';
	if ($icon) $out.='<i class="fa '.$icon.'"></i>';$out.= do_shortcode ($content).'<div class="clear"></div></blockquote></section>'; 
    return $out;
}
add_shortcode('panel', 'alc_panel');

/**************************************************/


/********** FULL-WIDTH BLOCK **********************/

function alc_section( $atts, $content = null ) {
	extract(shortcode_atts(array(
		'type'=>'',
		'style' => '',
		'bgcolor'=>'',
		'bgrepeat'=>'',
		'bg'=>'',
		'cover' => '',
		'topmargin' => '',
	), $atts));
	$GLOBALS['sbcount']=0;
	
	do_shortcode ($content);
	
	$out = '
	<div class="container '.$topmargin.'">
		<div class="row">
			<div class="col-sm-12">';
				if ($type === 'custom')
				{
					if ($bg)
					{
						$bg = 'background-image:url('.$bg.');';
					}	
					if ($bgcolor)
					{
						$bgcolor = 'background-color:'.$bgcolor.';';	
					}
					if ($bgrepeat)
					{
						$bgrepeat = 'background-repeat:'.$bgrepeat.';';	
					}
					
					$out.='<div class="fullwidth-box '.$cover.' '.$type.' '.$style.'" style="'.$bgcolor.' '.$bg.' '.$bgrepeat.'">'.do_shortcode($content).'</div>';
				}
				else 
				{
					$out.= '<div class="fullwidth-box '.$type.' '.$style.'">'.do_shortcode($content).'</div>';
				}
				$out.= '
			</div>
		</div>
	</div>';
	return $out;
}
add_shortcode('section', 'alc_section');

/***************** PROGRESS BAR *******************/

function alc_progressbar( $atts, $content = null ) {
    extract(shortcode_atts(array(
		"type" => '',
		"meter" => '',
		"style" => '',
		"animated" => '',
		"class" => '',
		"title" => ''
	), $atts));	
	$out = '
	<div class="progress fadeLeft '.$style.' '.$animated.'">
		<div class="progress-bar '.$type.' '.$class.'" role="progressbar" aria-valuenow="'.$meter.'" aria-valuemin="0" aria-valuemax="100" style="width:'.$meter.'%">
			<span class="sr-only">'.$title.'</span>
			'.$title.' &middot; '.$meter.'%
		</div>
	</div>';
    return $out;
}
add_shortcode('progressbar', 'alc_progressbar');

/************************************************/

function alc_carousel( $atts, $content ){
	$GLOBALS['caritem_count'] = 0;
	extract(shortcode_atts(array(
		'title' => '',
		'type' => 'custom',
		'automatic' => 'false',
		'min' => '1',
		'max' => '6',
		'slwidth'=>'193',
		'slmargin'=>'0'
	), $atts));
	$randomId = mt_rand(0, 100000);
	$panes = array();	
	$return = '';
	do_shortcode ($content);
        if($type=='logo_slide') { $mc='partners';} else{$mc='';}
	if(isset( $GLOBALS['caritems']) && is_array( $GLOBALS['caritems'] ) ){
		$return.='
		<div class="width-carousel '.$mc.'">
		   <h4 class="title-carousel cat-title">'.$title.'</h4>
			<ul class="customcar-'.$randomId.'" id="'.$type.'" >';
				foreach( $GLOBALS['caritems'] as $item ){
					$panes[] = '<li>'.$item['content'].'</li>'; 
				}
				unset ($GLOBALS['caritems']);
				$return.=implode( "\n", $panes ).'
			</ul>
		</div>
		<script type="text/javascript">
			jQuery(document).ready(function(){
			jQuery(".customcar-'.$randomId.'").bxSlider({ 
				slideWidth: '.$slwidth.',
				minSlides: '.$min.',
				maxSlides: '.$max.',
				auto: '.$automatic.',
				slideMargin: '.$slmargin.'    
			});
			})
		</script>';
	}
	return $return;
}

add_shortcode('carousel', 'alc_carousel' );
/***/

function alc_caritem( $atts, $content ){
	extract(shortcode_atts(array(
	'title' => '',
	), $atts));
	$x = $GLOBALS['caritem_count'];
	$GLOBALS['caritems'][$x] = array('title' => $title, 'content' =>  do_shortcode ($content) );
	$GLOBALS['caritem_count']++;	
}
add_shortcode( 'caritem', 'alc_caritem' );

/************************************************/


/******************* Timeline *******************/

function alc_timeline( $atts, $content ){
	
	$GLOBALS['tmi_count'] = 0;
	$randomId = mt_rand(0, 100000);
	$return = '';
	do_shortcode( $content );
	$counter = 1;
	if( is_array( $GLOBALS['timeline_items'] ) ){
		foreach( $GLOBALS['timeline_items'] as $tmitem ){
			$timeline_items[] = '
			<div class="story-item">
				<div class="story-item-content fade'.$tmitem['direction'].'">
					<div class="story-item-wrap">
						<div class="line-arrow"></div>
						<span class="si-year">'.$tmitem['year'].'</span>
						<span class="si-date">'.$tmitem['month'].'</span>
						<span class="si-header">'.$tmitem['title'].'</span><br/> 
						<p class="si-desc">'.do_shortcode($tmitem['content']).'</p>
					</div>
				</div>
			</div>';
		}
		
		$return.= '
		<div class="row">
			<div class="col-sm-12">
				<div class="story">
					<div class="timeline"></div>
					'.implode( "\n", $timeline_items ).'
				</div>
			</div>
		</div>';
	}
	return $return;
}
add_shortcode( 'timeline', 'alc_timeline' );
/*****************/


function alc_timeline_item( $atts, $content ){
	extract(shortcode_atts(array(
	'year' => '',
	'month' => '',
	'title' => '',
	'direction' => 'Right'
	), $atts));
	
	$x = $GLOBALS['tmi_count'];
	$GLOBALS['timeline_items'][$x] = array( 'year' => $year, 'month' => $month, 'title' => $title, 'direction'=> $direction, 'content' =>  $content );
	
	$GLOBALS['tmi_count']++;
}

add_shortcode( 'timeline_item', 'alc_timeline_item' );
/************************************************/




/*************** Dropdown buttons ***************/

function alc_dropbutton_group( $atts, $content ){
	extract(shortcode_atts(array(
		'title' => '',
		'type'	=> ''
	), $atts));
	$GLOBALS['dropbutton_count'] = 0;
	$randomId = mt_rand(0, 100000);
	$return = '';
	do_shortcode( $content );
	$counter = 1;
	if( is_array( $GLOBALS['dropbuttons'] ) ){
		foreach( $GLOBALS['dropbuttons'] as $dropbutton ){
			$dropbuttons[] = '<li><a href="'.$dropbutton['url'].'">'.do_shortcode($dropbutton['content']).'</a></li>';
			if ($dropbutton['divider'] == 1)
			{
				$dropbuttons[] = '<li class="divider"></li>';
			}
		}
		
		if ($type == 'split')
		{
			$return.='
			<div class="btn-group">
				<button class="btn">'.$title.'</button>
				<button class="btn dropdown-toggle" data-toggle="dropdown">
				<span class="caret"></span>
				</button>';
		}
		else
		{	
			$return.= '
			<div class="btn-group">
				 <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">'.$title.'<span class="caret"></span></a>';
		}
		$return.= '<ul class="dropdown-menu" id="'.$randomId.'">'.implode( "\n", $dropbuttons ).'</ul>';
		$return.= '</div>';
	}
	return $return;
}
add_shortcode( 'dropbuttongroup', 'alc_dropbutton_group' );
/*****************/


function alc_dropbutton( $atts, $content ){
	extract(shortcode_atts(array(
	'title' => '',
	'url' => '',
	'divider' => '',
	), $atts));
	
	$x = $GLOBALS['dropbutton_count'];
	$GLOBALS['dropbuttons'][$x] = array( 'title' => $title, 'url' => $url, 'divider' => $divider, 'content' =>  $content );
	
	$GLOBALS['dropbutton_count']++;
}

add_shortcode( 'dropbutton', 'alc_dropbutton' );
/************************************************/

/******************* BUTTONS ********************/

function alc_button( $atts, $content = null ) {
	extract(shortcode_atts(array(
		'size' => 'medium',
		'link' => '#',
        'type' => '',
		'color' => 'btn-default',
        'position'=>'',
        'status'=>'',
        'fullwidth'=>'',
		'target' => '',
        'icon'=>''
	), $atts));

	$target = ($target) ? ' target="_blank"' : '';
 
	$out = '<a href="'.$link.'"'.$target.' class="btn '.$type.' '.$size.' '.$color.' '.$position.' '.$status.' '.$fullwidth.'"><i class="fa '.$icon.'"></i>'.do_shortcode($content).'</a>';
    return $out;
}
add_shortcode('button', 'alc_button');

/************************************************/

/******************TABS****************************/
function alc_tab_group( $atts, $content ){		
	$GLOBALS['tab_count'] = 0;	
	do_shortcode( $content );
	$randomId = mt_rand(0, 100000);
	$counter = 0;
	$return = '<div class="tab tabgroup">';
		if( is_array( $GLOBALS['tabs'] ) ){
			foreach( $GLOBALS['tabs'] as $tab ){
				$active = ($counter == 0) ? 'active' : '';
				$tabs[] = '<li class="'.$active.'"><a href="#tabs-'.$randomId.'" data-toggle="tab">'.$tab['title'].'</a></li>';                
				$tabcontent[] = '<div id="tabs-'.$randomId.'" class="tab-pane '.$active.'">'.do_shortcode($tab['content']).'</div>';	
				$randomId++;
				$counter ++;
			}
			$return.= '<ul class="nav nav-tabs">'.implode( "\n", $tabs ).'</ul>';
			$return.= '<div class="tab-content">'.implode( "\n", $tabcontent ).'</div>';
			
		}
	$return.='</div>';
	return $return;
}
add_shortcode( 'tabgroup', 'alc_tab_group' );

/***********/

function alc_tab( $atts, $content ){
	extract(shortcode_atts(array(
	'title' => 'Tab %d',
	), $atts));
	
	$x = $GLOBALS['tab_count'];
	$GLOBALS['tabs'][$x] = array( 'title' => sprintf( $title, $GLOBALS['tab_count'] ), 'content' =>  $content );
	
	$GLOBALS['tab_count']++;
}
add_shortcode( 'tab', 'alc_tab' );


/************************************************/


/*****************Horizontal Navigation***********/
function alc_hornav_group( $atts, $content ){
	extract(shortcode_atts(array(
		'title' => '',
	), $atts));
	$GLOBALS['hornav_count'] = 0;
	do_shortcode( $content );
	$return = '
	<div class="navbar navbar-shortcode navbar-collapse collapse">
		<div class="navbar-inner">';
			if (!empty($title)) $return.=' <a class="brand" href="#">'.$title.'</a>';
			$return.='<nav class="main-nav"><ul class="nav navbar-nav">';				
				if( is_array( $GLOBALS['hornavs'] ) ){
					foreach( $GLOBALS['hornavs'] as $hornav ){
						$hornavs[] = ' 
						<li><a href="'.$hornav['link'].'">'.$hornav['title'].'</a></li>';	
					}
				}
				$return.=implode( "\n", $hornavs );
				$return.= '
			</ul></nav>
		</div>
	</div>';
	return $return;
}
add_shortcode( 'hornavgroup', 'alc_hornav_group' );


function alc_hornav( $atts, $content ){
	extract(shortcode_atts(array(
	'title' => 'Nav %d',
	'link'	=> ''
	), $atts));
	
	$x = $GLOBALS['hornav_count'];
	$GLOBALS['hornavs'][$x] = array( 'title' => sprintf( $title, $GLOBALS['hornav_count'] ), 'content' =>  $content, 'link' =>  $link );
	
	$GLOBALS['hornav_count']++;
}
add_shortcode( 'hornav', 'alc_hornav' );

/*************************************************/


/***************** Service  Group ****************/
function alc_servicegroup( $atts, $content ){
	
	$GLOBALS['sb_count'] = 0;
	$sblocks = array();
	do_shortcode( $content );
	$return = '<div class="row bottom-gap">';
	
	if( is_array( $GLOBALS['serviceblocks'] ) ){
		foreach( $GLOBALS['serviceblocks'] as $serviceblock ){
			$sblocks[] = ' 
			<div class="'.$serviceblock['colsize'].'">
				<div class="service fadeLeft">
					<div class="service-icon">
					  <i class="fa '.$serviceblock['icon'].'"></i>
					</div>
					<div class="service-header">
					  '.$serviceblock['title'].'
					</div>
					<div class="service-desc">
					  '.do_shortcode($serviceblock['content']).'
					</div>
					<div class="service-btn"><a href="'.$serviceblock['link'].'" class="btn btn-link">'.$serviceblock['caption'].'</a></div>
				</div>
			</div>';	
		}
		unset ($GLOBALS['serviceblocks']);
		$return.=implode( "\n", $sblocks );
		$return.= '<div class="clearfix"></div></div>';
		
	}
	return $return;
}
add_shortcode( 'servicegroup', 'alc_servicegroup' );

/***********/

function alc_serviceblock( $atts, $content ){
	extract(shortcode_atts(array(
	'title' => 'Service block %d',
	'link'	=> '#',
	'icon'  => '',
	'caption' => 'Read More',
	'colsize' => 'col-sm-4'
	), $atts));
	
	$x = $GLOBALS['sb_count'];
	$GLOBALS['serviceblocks'][$x] = array( 'title' => sprintf( $title, $GLOBALS['sb_count'] ), 'content' =>  $content, 'colsize' => $colsize, 'link' =>  $link, 'icon' =>  $icon, 'caption' =>  $caption);
	
	$GLOBALS['sb_count']++;
}
add_shortcode( 'serviceblock', 'alc_serviceblock' );


/*************************************************/


/****************** TOGGLES *********************/

function alc_toggle_group( $atts, $content ){
	
	$GLOBALS['toggle_count'] = 0;
	
	do_shortcode( $content );
	if( is_array( $GLOBALS['toggles'] ) ){
		foreach( $GLOBALS['toggles'] as $toggle ){
			$toggles[] = '
			<div class="toggle-wrapper panel-group">
				<div class="panel panel-default">
					<div class="panel-heading">
						<a href="#" class="toggle-trigger">'.$toggle['title'].'</a>                  
					</div>
					<div class="toggle-container">
						<div class="panel-body">'.do_shortcode($toggle['content']).'</div>
					</div>                  
				</div>
			</div>';	
		}
		$return = implode( "\n", $toggles );
	}
	return $return;

}
add_shortcode( 'togglegroup', 'alc_toggle_group' );


function alc_toggle( $atts, $content ){
	extract(shortcode_atts(array(
	'title' => 'toggle %d',
	), $atts));
	
	$x = $GLOBALS['toggle_count'];
	$GLOBALS['toggles'][$x] = array( 'title' => sprintf( $title, $GLOBALS['toggle_count'] ), 'content' =>  $content );
	
	$GLOBALS['toggle_count']++;
}
add_shortcode( 'toggle', 'alc_toggle' );
/************************************************/


/***************** ACCORDION ********************/


function alc_accordion_group( $atts, $content ){
	
	$GLOBALS['accordion_count'] = 0;
	$counter = $randomId = mt_rand(0, 100000);
	do_shortcode( $content );
	if( is_array( $GLOBALS['accordions'] ) ){
		foreach( $GLOBALS['accordions'] as $accordion ){
			$active = ($counter == $randomId) ? '' : 'collapsed';
			$in = ($counter == $randomId) ? 'in' : '';
			$accordions[] = '	
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<a class="accordion-toggle '.$active.'" data-toggle="collapse" data-parent="#accordion'.$randomId.'" href="#collapse'.$counter.'">
						'.$accordion['title'].'
					</a>
				</div>
				<div id="collapse'.$counter.'" class="panel-collapse collapse '.$in.'">
					<div class="panel-body">'.do_shortcode($accordion['content']).'</div>
				</div>
			</div>';	
			$counter++;
		}
		$return = ' <div class="panel-group fadeRight" id="accordion'.$randomId.'">'.implode( "\n", $accordions ).'</div>';
	}
	return $return;
}

add_shortcode( 'accordiongroup', 'alc_accordion_group' );
/***************/

function alc_accordion( $atts, $content ){
	extract(shortcode_atts(array(
	'title' => 'accordion %d',
	), $atts));
	
	$x = $GLOBALS['accordion_count'];
	$GLOBALS['accordions'][$x] = array( 'title' => sprintf( $title, $GLOBALS['accordion_count'] ), 'content' =>  $content );
	
	$GLOBALS['accordion_count']++;
}

add_shortcode( 'accordion', 'alc_accordion' );
/************************************************/


/*************** TESTIMONIALS ********************/

function alc_testimonial_group( $atts, $content ){
	
	$GLOBALS['testimonial_count'] = 0;
	$counter = 0;
	do_shortcode( $content );
	
	$return = '
	 <div class="flexslider testimonials-slider">';
		if( is_array( $GLOBALS['testimonials'] ) ){
			foreach( $GLOBALS['testimonials'] as $testimonial ){
				$testimonials[] = '		
					<li>
						<div class="testimonial">
							<blockquote>
								<p>'.do_shortcode($testimonial['content']).'</p>
								<small>
									<img class="author-avatar" src="'.$testimonial['photo'].'" alt="">
									'.$testimonial['name'].'
									<span class="occupation">'.$testimonial['position'].'</span>	
								</small>
							</blockquote>
						</div>
					</li>';	
				$counter++;
			}
			$return.= '<ul class="slides">'.implode( "\n", $testimonials ).'</ul>';
		}
	$return.='</div>';
	return $return;
}

add_shortcode( 'testimonialgroup', 'alc_testimonial_group' );

function alc_testimonial( $atts, $content ){
	extract(shortcode_atts(array(
	'name' => '',
	'photo' => '',
	'position' => ''
	), $atts));
	
	$x = $GLOBALS['testimonial_count'];
	$GLOBALS['testimonials'][$x] = array('name' => $name, 'photo' => $photo,  'position' => $position, 'content' =>  $content );
	
	$GLOBALS['testimonial_count']++;
}

add_shortcode( 'testimonial', 'alc_testimonial' );

/************************************************/

/******************* Alertbox *******************/

function alc_alert( $atts, $content = null ) {
     extract(shortcode_atts(array(
		"type"=>'',
		"title" => '',
	), $atts));	
	$out = '
	<div class="alert '.$type.' alert-dismissable">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<strong>'.$title.'</strong>
		'.do_shortcode($content).'
	</div>';
   return $out;
}
add_shortcode('alert', 'alc_alert');

/************************************************/


/***********  VIDEOS  ****************/

function alc_video($atts, $content=null) {
	extract(
		shortcode_atts(array(
			'site' => 'youtube',
			'id' => '',
			'width' => '',
			'height' => '',
			'autoplay' => '0'
		), $atts)
	);
	if ( $site == "youtube" ) { $src = 'http://www.youtube.com/embed/'.$id.'?autoplay='.$autoplay; }
	else if ( $site == "vimeo" ) { $src = 'http://player.vimeo.com/video/'.$id.'?autoplay='.$autoplay; }
	else if ( $site == "dailymotion" ) { $src = 'http://www.dailymotion.com/embed/video/'.$id.'?autoplay='.$autoplay; }
	else if ( $site == "veoh" ) { $src = 'http://www.veoh.com/static/swf/veoh/SPL.swf?videoAutoPlay='.$autoplay.'&permalinkId='.$id; }
	else if ( $site == "bliptv" ) { $src = 'http://a.blip.tv/scripts/shoggplayer.html#file=http://blip.tv/rss/flash/'.$id; }
	else if ( $site == "viddler" ) { $src = 'http://www.viddler.com/embed/'.$id.'e/?f=1&offset=0&autoplay='.$autoplay; }
	
	if ( $id != '' ) {
		return '<div class="flex-video"><iframe width="'.$width.'" height="'.$height.'" src="'.$src.'" class="vid iframe-'.$site.'"></iframe></div>';
	}
}
add_shortcode('video','alc_video');

/************************************************/


/****************** SLIDER ********************/
function alc_slider( $atts, $content ){
	$GLOBALS['slideritem_count'] = 0;
	extract(shortcode_atts(array(
		'interval' => '5000',
		'animation' => 'fade'
	), $atts));
	do_shortcode( $content );
		
	if( is_array( $GLOBALS['sitems'] ) ){
		$icount = 0;
		foreach( $GLOBALS['sitems'] as $item ){
			$title = $item['title'] == '' ? '' : '<p class="flex-caption">'.$item['title'].'</p>';
			$panes[] = '<li><img src="'.$item['image'].'" alt="'.$item['title'].'" />'.$title.'</li>';   		
			$icount ++ ;
		}
		$randomId = mt_rand(0, 100000);
		$return ='<div class="flexslider" id="flexslider-'.$randomId.'"><ul class="slides">'.implode( "\n", $panes ).'</ul></div>
		<script type="text/javascript">
			jQuery(document).ready(function() {
				jQuery("#flexslider-'.$randomId.'").flexslider({ useCSS: false, animationLoop: false, slideshowSpeed:'.$interval.', animation: \''.$animation.'\'});
			});
		</script>';	
	}
	return $return;
}
add_shortcode('slider', 'alc_slider' );

/****/



function alc_slideritem( $atts, $content ){
	extract(shortcode_atts(array(
		'image' => '',
		'title' => '',
	), $atts));
	
	$x = $GLOBALS['slideritem_count'];
	$GLOBALS['sitems'][$x] = array( 'image' => $image, 'title' => $title, 'content' =>  $content );
	
	$GLOBALS['slideritem_count']++;
	
}
add_shortcode( 'slideritem', 'alc_slideritem' );

/************************************************/


/*************** FEATURED BLOCK *****************/

function alc_fblock($atts, $content=NULL){
    extract(shortcode_atts(array(
		'title'=>'',
		'icon'=>'',	
		'image'=>'',
		'color' => '',
		'animation' => 'fadeLeft'	
    ), $atts));
	$link = isset ($link) ? $link : '#';
	$graphics = empty($image)  ? '<i class="fa '.$icon.' fblock-icon '.$animation.'" style="color:'.$color.'"></i>' : '<img alt="" src="'.$image.'" class="'.$animation.'" />';
	$out='
	<div class="icon-box">
		'.$graphics.'
		<div class="icon-box-content">
			<h5>'.$title.'</h5>
			<p>'.do_shortcode($content).'</p>
		</div>
	</div>';

    return $out;
}
add_shortcode('fblock', 'alc_fblock');

/**************************************************/


/***************** TITLE BLOCK ********************/

function alc_tblock($atts, $content=NULL){
    extract(shortcode_atts(array(
        'title'=>'', 
        'image'=>''
    ), $atts));
    
    $icon = isset ($image) ? '<i class="fa icon '.$image.'"></i>' : '';
    $out ='<h2 class="heading">'.$icon.'<span>'.$title.'</span></h2><div class="clearfix"></div>';
    
    return $out;
}

add_shortcode('tblock', 'alc_tblock');

/**************************************************/


/******************* REVEAL BOX *******************/

function alc_reveal($atts, $content=NULL){
    extract(shortcode_atts(array(
        'type'=>'',
        'size'=>'',
        'color'=>'',
        'button'=>'', 
        'revtitle'=>'',
    ), $atts));
    $randomId=  mt_rand(0, 100000);
    
    $out='<a href="#myModal'.$randomId.'"  role="button" data-toggle="modal" class="btn '.$type.' '.$color.' '.$size.'">'.$button.'</a>';
    $out.='
	<div id="myModal'.$randomId.'"  class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">	
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 id="myModalLabel">'.$revtitle.'</h3>
				</div>
				<div class="modal-body">
					<p>'.do_shortcode($content).'</p>
				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
				</div>
			</div>
		</div>
	</div>';	
return $out;
}

add_shortcode('reveal', 'alc_reveal');

/*************************************************/


/************** Twitter shortcode ****************/

function alc_twitter($atts, $content=NULL){
    extract(shortcode_atts(array(
		'title' => '',
        'twitter_username'=>'',
        'consumer_key'=>'',
        'consumer_secret'=>'',
        'access_token'=>'', 
        'access_token_secret'=>'',
		'limit' => 3
    ), $atts));
    
	$transName = 'list_tweets_shortcode';
	$cacheTime = 20;
	$out = '
	<section class="section section-inverse section-bg-overlay section-bg-attached home-bg fullwidth-box">
		<div class="container">
		  <div class="row">
			<div class="col-sm-12">
			  <h5 class="small-header sh-center bottom-gap">
				<span>'.$title.'</span>
			  </h5>
			</div>
		  </div>';
	if( !empty($twitter_username) && !empty($consumer_key) && !empty($consumer_secret) && !empty($access_token) && !empty($access_token_secret)  ){
	    if(false === ($twitterData = get_transient($transName) ) ){
			$twitterConnection = new TwitterOAuth( $consumer_key , $consumer_secret , $access_token , $access_token_secret	);
			$twitterData = $twitterConnection->get('statuses/user_timeline', array('screen_name'=> $twitter_username, 'count'=> $limit));
			if($twitterConnection->http_code != 200)
			{
				$twitterData = get_transient($transName);
			}
	       
	        set_transient($transName, $twitterData, 60 * $cacheTime);
	    }

		if( !empty($twitterData) && is_array($twitterData)  && !isset($twitterData['error'])){
			$i=0;
			$hyperlinks = true;
			$encode_utf8 = true;
			$twitter_users = true;
			$update = true;
			$out.='
			<div class="row">
				<div class="col-sm-12">
					<div class="flexslider tweet">
						<ul class="tweet_list">';
							foreach($twitterData as $item){
								$msg = $item->text;
								$permalink = 'http://twitter.com/#!/'. $twitter_username .'/status/'. $item->id_str;
								if($encode_utf8) $msg = utf8_encode($msg);
								$link = $permalink;
								$out.='<li><span class="tweet_text">';
									//if ($hyperlinks) {    $msg = hyperlinks($msg); }
									//if ($twitter_users)  { $msg = twitter_users($msg); }
									$out.=$msg.'</span>';
									
									if($update) {
									  $time = strtotime($item->created_at);
									  if ( ( abs( time() - $time) ) < 86400 )
										$h_time = sprintf( __('%s ago', 'Ante'), human_time_diff( $time ) );
									  else
										$h_time = date(__('Y/m/d', 'Ante'), $time);
										sprintf( __('%s', 'twitter-for-wordpress', 'Ante'),' <span class="tweet_time">' . $h_time . '</span>' );
									 }
								$out.='</li>';
								$i++;
								if ( $i >= $limit ) break;
							}
							$out.='	
						</ul>
					</div>
				</div>
			</div>';
		}
		else{ 
			$out='<p class="loading">'.__('Sorry , Twitter seems down or responds slowly.', 'Ante').'</p>'; 
		}
	}
	else{
		$out.='<a href="http://twitter.com/'.$twitter_username.'" class="twitter-title">'.$title.'</a>';
		$out.='<p>'.__('You need to Setup Twitter API OAuth settings first', 'Ante').'</p>';
	}
	$out.='
		</div>
	 </section>';
		
	return $out;
}

add_shortcode('twitter', 'alc_twitter');

/*************************************************/


/****************** COUNTER BOX ******************/

function alc_counter($atts, $content=NULL){
    extract(shortcode_atts(array(
        'direction'=>'Top',
        'timeout'=>'1200',
        'icon'=>'fa-calendar',
        'start'=>'0', 
        'end'=>'10',
		'speed'=>'2000',
		'title' => ''
    ), $atts));
    
	$out='
	<div class="count-item fade'.$direction.'">
		<div class="count-header"><i class="fa '.$icon.'"></i></div>
		<span class="count-number counter" data-from="'.$start.'" data-to="'.$end.'" data-speed="'.$speed.'">'.$start.'</span>
		<div class="count-subject">'.$title.'</div>
	</div>';	
return $out;
}

add_shortcode('counter', 'alc_counter');

/*************************************************/


/*************** SOCIAL BUTTONS *****************/
function alc_social($atts, $content=NULL){
    $GLOBALS['socbuttoncount']=0;
	$out = '';
	do_shortcode ($content);
    if(isset($GLOBALS['soc_buttons']) && is_array($GLOBALS['soc_buttons'])){
        foreach ($GLOBALS['soc_buttons'] as $soc){
			if($soc['skype']){
				$soc_buttons[]='<a href="skype:'.$soc['skype'].'" target="_blank" class="fa '.$soc['icon'].'"></a>';
			}
			else{
				$soc_buttons[]='<a href="'.$soc['link'].'" target="_blank" class="fa '.$soc['icon'].'"></a>';
			}
        }
        $out='<div class="social-icons">'.implode("\n", $soc_buttons).'</div>';
		unset ($GLOBALS['soc_buttons']);
    }
    return $out;
}

add_shortcode('social', 'alc_social');

/*********************/
function alc_soc_button($atts, $content=NULL){
    extract(shortcode_atts(array(
        'icon'=>'',
        'link'=>'',
		'skype' => ''
    ), $atts));
    //do_shortcode ($content);
    $x= $GLOBALS['socbuttoncount'];
    $GLOBALS['soc_buttons'][$x]=array('icon'=> $icon, 'link'=>$link, 'skype'=> $skype);
    $GLOBALS['socbuttoncount']++;

} 

add_shortcode('soc_button', 'alc_soc_button');
/**************************************************/


/***************** TEAM MEMBERS *******************/
function al_teammember($atts, $content=NULL){
    extract(shortcode_atts(array(
        'name'=>'',
        'size'=>'big',
		'photo'=>'',
		'info' => ''
    ), $atts));
	$GLOBALS['sbcount']=0;
	
	do_shortcode ($content);
	$out = '
	<div class="team-member-'.$size.' fadeBottom">
		<div class="team-member-overlay">
			<h5>'.$name.'</h5>
			<p>'.do_shortcode($info).'</p>';
			if(is_array($GLOBALS['tmsocbuttons'])){
				foreach ($GLOBALS['tmsocbuttons'] as $soc){
					$tmsocbuttons[]='<a href="'.$soc['link'].'" target="_blank" class="fa '.$soc['icon'].'"></a>';
				}
				unset ($GLOBALS['tmsocbuttons']);
				$out.='<div class="social-icons">'.implode("\n", $tmsocbuttons).'</div>';
			}
		$out.='</div>
		<img src="'.$photo.'" alt="" />
	</div>';
    return $out;
}

add_shortcode('teammember', 'al_teammember');

function al_tmsocbutton($atts, $content=NULL){
    extract(shortcode_atts(array(
        'icon'=>'',
        'link'=>''
    ), $atts));
    
	$x = $GLOBALS['sbcount'] ;
    $GLOBALS['tmsocbuttons'][$x]=array('icon'=> $icon, 'link'=>$link, 'content' => $content);
    $GLOBALS['sbcount']++;
} 

add_shortcode('tmsocbutton', 'al_tmsocbutton');


/**************************************************/



/********************Side Navigation*************/

function alc_sidenav( $atts, $content ){
	
	$GLOBALS['sideitem_count'] = 0;
	
	do_shortcode( $content );
	if( is_array( $GLOBALS['sideitems'] ) ){
		foreach( $GLOBALS['sideitems'] as $sideitem ){
			$sideitems[] = '<li><a href="'.$sideitem['link'].'"><i class="fa fa-caret-right"></i>'.do_shortcode($sideitem['content']).'</a></li>';	
		}
		$return = '<ul class="nav nav-list bs-docs-sidenav affix">'.implode( "\n", $sideitems ).'</ul>';
	}
	return $return;

}
add_shortcode( 'sidenav', 'alc_sidenav' );
/************/

function alc_sideitem( $atts, $content ){
	extract(shortcode_atts(array(
		'title' => 'sideitem %d',
        'link' => ''    
	), $atts));
	
	$x = $GLOBALS['sideitem_count'];
	$GLOBALS['sideitems'][$x] = array( 'title' => sprintf( $title, $GLOBALS['sideitem_count'] ), 'link'=>$link, 'content' =>  $content );
	
	$GLOBALS['sideitem_count']++;
}
add_shortcode( 'sideitem', 'alc_sideitem' );


/******************* COLUMNS ********************/

function Ante_one_whole( $atts, $content = null ) {
	extract(shortcode_atts(array('class' => ''), $atts));
	return '<div class="col-md-12 '.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_whole', 'Ante_one_whole');

function Ante_one_half( $atts, $content = null ) {
	extract(shortcode_atts(array('class' => ''), $atts));
	return '<div class="col-md-6 '.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_half', 'Ante_one_half');

function Ante_one_third( $atts, $content = null ) {
	extract(shortcode_atts(array('class' => ''), $atts));
	return '<div class="col-md-4 '.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_third', 'Ante_one_third');

function Ante_two_third( $atts, $content = null ) {
	extract(shortcode_atts(array('class' => ''), $atts));
	return '<div class="col-md-8 '.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('two_third', 'Ante_two_third');

function Ante_one_fourth( $atts, $content = null ) {
	extract(shortcode_atts(array('class' => ''), $atts));
	return '<div class="col-md-3 '.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_fourth', 'Ante_one_fourth');

function Ante_three_fourth( $atts, $content = null ) {
	extract(shortcode_atts(array('class' => ''), $atts));
	return '<div class="col-md-9 '.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('three_fourth', 'Ante_three_fourth');

function Ante_one_sixth( $atts, $content = null ) {
	extract(shortcode_atts(array('class' => ''), $atts));
	return '<div class="col-md-2 '.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_sixth', 'Ante_one_sixth');

function Ante_five_twelveth( $atts, $content = null ) {
	extract(shortcode_atts(array('class' => ''), $atts));
	return '<div class="col-md-5 '.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('five_twelveth', 'Ante_five_twelveth');

function Ante_seven_twelveth( $atts, $content = null ) {
	extract(shortcode_atts(array('class' => ''), $atts));
	return '<div class="col-md-7 '.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('seven_twelveth', 'Ante_seven_twelveth');


function Ante_one_twelveth( $atts, $content = null ) {
	extract(shortcode_atts(array('class' => ''), $atts));
	return '<div class="col-md-1 '.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_twelveth', 'Ante_one_twelveth');

function Ante_eleven_twelveth( $atts, $content = null ) {
	extract(shortcode_atts(array('class' => ''), $atts));
	return '<div class="col-md-11 '.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('eleven_twelveth', 'Ante_eleven_twelveth');

function Ante_five_sixth( $atts, $content = null ) {
	extract(shortcode_atts(array('class' => ''), $atts));
	return '<div class="col-md-10 '.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('five_sixth', 'Ante_five_sixth');

function Ante_row( $atts, $content = null ) {
	extract(shortcode_atts(array('class' => ''), $atts));
	return '<div class="row '.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('row', 'Ante_row');

/************************************************/


/***************** CLEAR ************************/

function alc_clear($atts, $content = null) {	
	return '<div class="clearfix"></div>';
}
add_shortcode('clear', 'alc_clear');


/******** SHORTCODE SUPPORT FOR WIDGETS *********/

if (function_exists ('shortcode_unautop')) {
	add_filter ('widget_text', 'shortcode_unautop');
}
add_filter ('widget_text', 'do_shortcode');

/************************************************/
?>