<?php
$page = htmlentities($_GET['page']);

?>
<!DOCTYPE html>
<head>
	<script type="text/javascript" src="../../../../../../wp-includes/js/jquery/jquery.js"></script>
	<script type="text/javascript" src="../../../../../../wp-includes/js/tinymce/tiny_mce_popup.js"></script>
	
	<link rel='stylesheet' href='shortcode.css' type='text/css' media='all' />
<?php
if( $page == 'panel' ){
?>
	<script type="text/javascript">
		var AddPanel = {
			e: '',
			init: function(e) {
				AddPanel.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {

				var PanelIcon = jQuery('#PanelIcon').val();
				var PanelContent = jQuery('#PanelContent').val();

				var output = '[panel ';
		
				if(PanelIcon) {
					output += 'icon="'+PanelIcon+'" ';
				}
				
				
				output += ']'+PanelContent+'[/panel]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(AddPanel.init, AddPanel);

	</script>
	<title>Add Promo block</title>

</head>
<body>
<form id="GalleryShortcode">
	<p>
		<label for="PanelIcon">Icon :</label>
		<input id="PanelIcon" name="PanelIcon" type="text" value="" />
		<small>Choose your icon from <a href="http://fontawesome.io/icons/" target="blank">this list</a>.</small>
	</p>
	
	<p>
		<label for="PanelContent">Content : </label>
		<textarea id="PanelContent" name="PanelContent" col="20"></textarea>
	</p>
	<p><a class="add" href="javascript:AddPanel.insert(AddPanel.e)">insert into post</a></p>
</form>
<!--/*************************************/ -->
<?php } elseif( $page == 'carousel' ){ ?>
	
	<script type="text/javascript">
		var Carousel = {
			e: '',
			init: function(e) {
				Carousel.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
			
				var output = "[carousel ";
				var title = jQuery('#carouselTitle').val();
                                var type = jQuery('#carouselType').val();
				var auto = jQuery('#carouselAuto').val();
				var min = jQuery('#carouselMin').val();
				var max = jQuery('#carouselMax').val();
				var slwidth = jQuery('#carouselWidth').val();
                                var slmargin = jQuery('#carouselMargin').val();
				
				if(title) {
					output += 'title="'+title+'"';
				}
				if(type) {
					output += ' type="'+type+'"';
				}
				if(auto) {
					output += ' automatic="'+auto+'"';
				}
				if(min) {
					output += ' min="'+min+'"';
				}
				if(max) {
					output += ' max="'+max+'"';
				}
                                if(slwidth) {
					output += ' slwidth="'+slwidth+'"';
				}
				if(slmargin) {
					output += ' slmargin="'+slmargin+'"';
				}
				output += "]";
				
				jQuery("textarea[id^=carousel_content]").each(function(intIndex, objValue) {
					output +='[caritem]'+jQuery(this).val()+'[/caritem]';
				});
				
				output += '[/carousel]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(Carousel.init, Carousel);

		jQuery(document).ready(function() {
			jQuery("#add-carousel").click(function() {
				jQuery('#SlideShortcodeContent').append('<p><label for="carousel_content[]">Slide Content</label><textarea style="height:100px; width:400px;" id="carousel_content[]" name="carousel_content[]" type="text" value="" ></textarea></p>');
			});
		});
		
	</script>
	<title>Add Carousel slide</title>

</head>
<body>

<form id="CarouselShortcode">
<div id="SlideShortcodeContent">
	<p>
		<label for="carouselTitle">Carousel title</label>
		<input id="carouselTitle" name="carouselTitle" type="text" value="" />
	</p>
	<p>
		<label for="carouselType">Carousel type</label>
		<select id="carouselType" name="carouselType">
			<option value="logo_slide">Logo slider</option>
			<option value="custom_slide">Custom content</option>
		</select>
	</p>
	<p>
		<label for="carouselAuto">Automatic sliding</label>
		<select id="carouselAuto" name="carouselAuto">
			<option value="false">No</option>
			<option value="true">Yes</option>
		</select>
	</p>
	
	<p>
		<label for="carouselMin">Min. visible items</label>
		<input id="carouselMin" name="carouselMin" type="text" value="1" />
	</p>
	<p>
		<label for="carouselMax">Max. visible items</label>
		<input id="carouselMax" name="carouselMax" type="text" value="6" />
	</p>
        <p>
		<label for="carouselWidth">Slide width</label>
		<input id="carouselWidth" name="carouselWidth" type="text" value="" />
	</p>
	<p>
		<label for="carouselMargin">Slide margin</label>
		<input id="carouselMargin" name="carouselMargin" type="text" value="" />
	</p>
	<p>
		<label for="carousel_content[]">Slide Content</label>
		<textarea style="height:100px; width:400px;" id="carousel_content[]" name="carousel_content[]" type="text" value="" ></textarea>
	</p>
</div>
	<strong><a style="cursor: pointer;" id="add-carousel">+ Add slide</a></strong>
	<p><a class="add" href="javascript:Carousel.insert(Carousel.e)">insert into post</a></p>
</form>
<!--/*************************************/ -->
<?php } elseif( $page == 'progress' ){
?>
	<script type="text/javascript">
		var AddProgress = {
			e: '',
			init: function(e) {
				AddProgress.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {

				var ProgressType = jQuery('#ProgressType').val();
				var ProgressTitle = jQuery('#ProgressTitle').val();
				var ProgressAnim = jQuery('#ProgressAnim').val();
				var ProgressStyle = jQuery('#ProgressStyle').val();
				var ProgressMeter = jQuery('#ProgressMeter').val();
				var ProgressClass = jQuery('#ProgressClass').val();
				
				var output = '[progressbar ';
		
				if(ProgressType) {
					output += 'type="'+ProgressType+'" ';
				}
				if(ProgressTitle) {
					output += 'title="'+ProgressTitle+'" ';
				}
				if(ProgressMeter) {
					output += 'meter="'+ProgressMeter+'" ';
				}
				if(ProgressAnim) {
					output += 'animated="'+ProgressAnim+'" ';
				}
				if(ProgressStyle) {
					output += 'style="'+ProgressStyle+'" ';
				}
				if(ProgressClass) {
					output += 'class="'+ProgressClass+'" ';
				}
				
				output += '/]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(AddProgress.init, AddProgress);

	</script>
	<title>Add Progress bar</title>

</head>
<body>
<form id="GalleryShortcode">
	<p>
		<label for="ProgressType">Type :</label>
		<select id="ProgressType" name="ProgressType">
			<option value="progress-bar-info">Regular (blue)</option>
			<option value="progress-bar-success">Success (green)</option>
			<option value="progress-bar-danger">Danger (red)</option>
			<option value="progress-bar-warning">Warning (orange)</option>		
		</select>
	</p>
	<p>
		<label for="ProgressTitle">Title :</label>
		<input id="ProgressTitle" name="ProgressTitle" type="text" value="" />
	</p>
	<p>
		<label for="ProgressStyle">Style :</label>
		<select id="ProgressStyle" name="ProgressStyle">
			<option value="">Regular</option>
			<option value="progress-striped">Striped</option>	
		</select>
	</p>
	<p>
		<label for="ProgressAnim">Animated :</label>
		<select id="ProgressAnim" name="ProgressAnim">
			<option value="">No</option>
			<option value="active">Yes</option>
		</select>
	</p>
	
	<p>
		<label for="ProgressMeter">Progress meter :</label>
		<select id="ProgressMeter" name="ProgressMeter">
			<option value="10">10%</option>
			<option value="20">20%</option>
			<option value="30">30%</option>
			<option value="40">40%</option>
			<option value="50">50%</option>
			<option value="60">60%</option>
			<option value="70">70%</option>
			<option value="80">80%</option>
			<option value="90">90%</option>
			<option value="100">100%</option>
		</select>
	</p>
	<p>
		<label for="ProgressClass">Additional CSS class :</label>
		<input id="ProgressClass" name="ProgressClass" type="text" value="" />
	</p>
	
	<p><a class="add" href="javascript:AddProgress.insert(AddProgress.e)">insert into post</a></p>
</form>

<!--/************** COUNTER **********/ --> 

<?php } elseif( $page == 'Counter' ){
?>
	<script type="text/javascript">
		var AddCounter = {
			e: '',
			init: function(e) {
				AddCounter.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {

				var counterDirection = jQuery('#counterDirection').val();
				var counterTimeout = jQuery('#counterTimeout').val();
				var counterIcon = jQuery('#counterIcon').val();
				var counterStart = jQuery('#counterStart').val();
				var counterEnd = jQuery('#counterEnd').val();
				var counterSpeed = jQuery('#counterSpeed').val();
				var counterTitle = jQuery('#counterTitle').val();
				
				var output = '[counter ';
		
				if(counterDirection) {
					output += 'direction="'+counterDirection+'" ';
				}
				if(counterTimeout) {
					output += 'timeout="'+counterTimeout+'" ';
				}
				if(counterIcon) {
					output += 'icon="'+counterIcon+'" ';
				}
				if(counterStart) {
					output += 'start="'+counterStart+'" ';
				}
				if(counterEnd) {
					output += 'end="'+counterEnd+'" ';
				}
				if(counterSpeed) {
					output += 'speed="'+counterSpeed+'" ';
				}
				if(counterTitle) {
					output += 'title="'+counterTitle+'" ';
				}
				
				output += '/]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(AddCounter.init, AddCounter);

	</script>
	<title>Add counter</title>

</head>
<body>
<form id="GalleryShortcode">
	<p>
		<label for="counterDirection">Animation Direction :</label>
		<select id="counterDirection" name="counterDirection">
			<option value="Top">Top</option>
			<option value="Right">Right</option>
			<option value="Bottom">Bottom</option>
			<option value="Left">Left</option>		
		</select>
	</p>
	<p>
		<label for="counterTitle">Title :</label>
		<input id="counterTitle" name="counterTitle" type="text" value="" />
	</p>
	<p>
		<label for="counterTimeout">Delay (in milliseconds) :</label>
		<input id="counterTimeout" name="counterTimeout" type="text" value="" />
	</p>
	<p>
		<label for="counterIcon">Button Icon :</label>
		<input id="counterIcon" name="counterIcon" type="text"/>
        <small>Choose your icon from <a href="http://fontawesome.io/icons/" target="blank">this list</a>.</small>
	</p>
	<p>
		<label for="counterStart">Start value :</label>
		<input id="counterStart" name="counterStart" type="text" value="" />
	</p>
	<p>
		<label for="counterEnd">End value :</label>
		<input id="counterEnd" name="counterEnd" type="text" value="" />
	</p>
	<p>
		<label for="counterSpeed">Animation speed :</label>
		<input id="counterSpeed" name="counterSpeed" type="text" value="" />
	</p>
	
	<p><a class="add" href="javascript:AddCounter.insert(AddCounter.e)">insert into post</a></p>
</form>
<!--/*************************************/ -->

<?php } elseif( $page == 'section' ){
?>
	<script type="text/javascript">
		var AddSection = {
			e: '',
			init: function(e) {
				AddSection.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {

				var sectionType = jQuery('#sectionType').val();
				var sectionMargin = jQuery('#sectionMargin').val();
				var sectionDiff = jQuery('#sectionDiff').val();
				var sectionInverse = jQuery('#sectionInverse').val();
				var sectionOverlay = jQuery('#sectionOverlay').val();
				var sectionfixedbg = jQuery('#sectionfixedbg').val();
				var sectionPadding = jQuery('#sectionPadding').val();
				var sectionBgcolor = jQuery('#sectionBgcolor').val();
				var sectionBgimage = jQuery('#sectionBgimage').val();
				var sectionBgrepeat = jQuery('#sectionBgrepeat').val();
				var sectionClass = jQuery('#sectionClass').val();
				var sectionCover = jQuery('#sectionCover').val();
				
				var output = '[section ';
		
				if(sectionType) {
					output += 'type="'+sectionType+'" ';
				}
				
				if(sectionCover) {
					output += 'cover="'+sectionCover+'" ';
				}
				if(sectionMargin) {
					output += 'topmargin="'+sectionMargin+'" ';
				}
				output += 'style="';
				if(sectionPadding) {
					output += sectionPadding+' ';
				}
				if(sectionDiff) {
					output += sectionDiff+' ';
				}
				if(sectionInverse) {
					output += sectionInverse+' ';
				}
				if(sectionOverlay) {
					output += sectionOverlay+' ';
				}
				if(sectionfixedbg) {
					output += sectionfixedbg+' ';
				}
				
				output += '"';
				
				if(sectionBgcolor) {
					output += ' bgcolor="'+sectionBgcolor+'" ';
				}
				
				if(sectionBgimage) {
					output += 'bg="'+sectionBgimage+'" ';
				}
				
				if(sectionBgrepeat) {
					output += 'bgrepeat="'+sectionBgrepeat+'" ';
				}
				
				if(sectionClass) {
					output += 'class="'+sectionClass+'" ';
				}
				
				output += '][/section]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(AddSection.init, AddSection);

	</script>
	<title>Add Section</title>

</head>
<body>
<form id="GalleryShortcode">
	<p>
		<label for="sectionType">Type :</label>
		<select id="sectionType" name="sectionType">
			<option value="number-box">Type 1</option>
			<option value="testimonial-box">Type 2</option>
			<option value="device-box">Type 3</option>
			<option value="custom">Custom (Set own bg image/color)</option>		
		</select>
	</p>
	
	<p>
		<label for="sectionMargin">Add margin :</label>
		<input type="checkbox" name="sectionMargin" id="sectionMargin" value="fbb-container" />
	</p>
	<p>
		<label for="sectionPadding">Add padding :</label>
		<input type="checkbox" name="sectionPadding" id="sectionPadding" value="section" />
	</p>

	
	<p>
		<label for="sectionDiff">Add differentiation :</label>
		<input type="checkbox" id="sectionDiff" name="sectionDiff" value="section-diff" />
	</p>
	<p>
		<label for="sectionInverse">Add inversion :</label>
		<input type="checkbox" name="sectionInverse" id="sectionInverse" value="section-inverse" />
	</p>
	<p>
		<label for="sectionOverlay">Add overlay :</label>
		<input type="checkbox" name="sectionOverlay" id="sectionOverlay" value="section-bg-overlay" />
	</p>
	<p>
		<label for="sectionfixedbg">Fixed background :</label>
		<input type="checkbox" name="sectionfixedbg" id="sectionfixedbg" value="section-bg-attached" />
	</p>
	<p>
		<label for="sectionCover">Background cover:</label>
		<select id="sectionCover" name="sectionCover">
			<option value="">Enabled</option>
			<option value="no-cover">Disabled</option>	
		</select>
		
	</p>
	<p>
		<label for="sectionBgcolor">Custom background color :</label>
		<input id="sectionBgcolor" name="sectionBgcolor" type="text" value="" />
	</p>
	<p>
		<label for="sectionBgimage">Custom background image :</label>
		<input id="sectionBgimage" name="sectionBgimage" type="text" value="" />
	</p>
	<p>
		<label for="sectionBgrepeat">Custom background repeat :</label>
		<select id="sectionBgrepeat" name="sectionBgrepeat">
			<option value="no-repeat">No repeat</option>
			<option value="repeat-x">Repeat-x</option>
			<option value="repeat-y">Repeat-y</option>
			<option value="repeat">Repeat</option>		
		</select>
	</p>
	
	<p>
		<label for="sectionClass">Additional CSS class :</label>
		<input id="sectionClass" name="sectionClass" type="text" value="" />
	</p>
	
	<p><a class="add" href="javascript:AddSection.insert(AddSection.e)">insert into post</a></p>
</form>
<!--/*************************************/ --> 



<?php } elseif( $page == 'dropdown' ){ ?>

	<script type="text/javascript">
		var DropdownButton = {
			e: '',
			init: function(e) {
				DropdownButton.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
			
				var output = "[dropbuttongroup";
				var Type = jQuery('#Type').val();
				var Title = jQuery('#Title').val();
				
				if(Type) {
					output+= ' type="'+Type+'"';
				}
				
				if(Title) {
					output+= ' title="'+Title+'"';
				}
				
				output += "]";
				
				jQuery("input[id^=dropbutton_title]").each(function(intIndex, objValue) {
					output +='[dropbutton';
					output += ' title="'+jQuery(this).val()+'"';
					var obj1 = jQuery('input[id^=dropbutton_url]').get(intIndex);
					output += ' url= "'+obj1.value+'"';
					
					var obj2 = jQuery('input[id^=dropbutton_divider]').get(intIndex);
					output += ' divider= "'+obj2.value+'"]';
									
					var obj = jQuery('input[id^=Content]').get(intIndex);
					output += obj.value;
					output += "[/dropbutton]";
				});
				
				
				output += '[/dropbuttongroup]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(DropdownButton.init, DropdownButton);

		jQuery(document).ready(function() {
			jQuery("#add-dropbutton").click(function() {
				jQuery('#DropbuttonShortcodeContent').append('<p><label for="dropbutton_title[]">Item Title</label><input id="dropbutton_title[]" name="dropbutton_title[]" type="text" value="" /></p><p><label for="dropbutton_url[]">Item URL</label><input id="dropbutton_url[]" name="dropbutton_url[]" type="text" value="" /></p><p><label for="Content[]">Item Content</label><input id="Content[]" name="Content[]" type="text" value="" /></p><p><label for="dropbutton_divider[]">Insert divider after item</label><input id="dropbutton_divider[]" name="dropbutton_divider[]" type="checkbox" value="1" /></p>	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />');
			});
		});
		
	</script>
	<title>Add Dropdown button</title>

</head>
<body>
<form id="DropbuttonShortcode">
<div id="DropbuttonShortcodeContent">
	<p>
		<label for="Title">Title</label>
		<input id="Title" name="Title" type="text" value="" />
	</p>
	<p>
		<label for="Type">Type :</label>
		<select id="Type" name="Type">
			<option value="">Default</option>
			<option value="split">Split</option>
		</select>		
	</p>
	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />
	<p>
		<label for="dropbutton_title[]">Item Title</label>
		<input id="dropbutton_title[]" name="dropbutton_title[]" type="text" value="" />
	</p>
	<p>
		<label for="dropbutton_url[]">Item URL</label>
		<input id="dropbutton_url[]" name="dropbutton_url[]" type="text" value="" />
	</p>
	<p>
		<label for="Content[]">Item Content</label>
		<input id="Content[]" name="Content[]" type="text" value="" />
	</p>
	<p>
		<label for="dropbutton_divider[]">Insert divider after item</label>
		<input id="dropbutton_divider[]" name="dropbutton_divider[]" type="checkbox" value="" />
	</p>
	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />
</div>
	<strong><a style="cursor: pointer;" id="add-dropbutton">+ Add Item</a></strong>
	<p><a class="add" href="javascript:DropdownButton.insert(DropdownButton.e)">insert into post</a></p>
</form>
<!--/*************************************/ --> 

<?php
} elseif( $page == 'button' ){
 ?>
 	<script type="text/javascript">
		var AddButton = {
			e: '',
			init: function(e) {
				AddButton.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {

				var ButtonColor = jQuery('#ButtonColor').val();
				var ButtonSize = jQuery('#ButtonSize').val();
				var ButtonType = jQuery('#ButtonType').val();
				var ButtonLink = jQuery('#ButtonLink').val();
				var ButtonPos = jQuery('#ButtonPosition').val();
				var ButtonStatus = jQuery('#ButtonStatus').val();
				var ButtonWidth = jQuery('#ButtonWidth').val();
				var ButtonText = jQuery('#ButtonText').val();
				var ButtonTarget = jQuery('#ButtonTarget').val();
                var ButtonIcon = jQuery('#ButtonIcon').val();

				var output = '[button ';
				
				if(ButtonColor) {
					output += 'color="'+ButtonColor+'" ';
				}
				if(ButtonSize) {
					output += 'size="'+ButtonSize+'" ';
				}
				if(ButtonType) {
					output += 'type="'+ButtonType+'" ';
				}
				if(ButtonPos){
						output += 'position="'+ButtonPos+'" ';
				}
				if(ButtonStatus){
						output += 'status="'+ButtonStatus+'" ';
				}
				if(ButtonWidth){
						output += 'fullwidth="'+ButtonWidth+'" ';
				}
				if(ButtonLink) {
					output += 'link="'+ButtonLink+'" ';
				} 
				if(ButtonIcon){
						output += 'icon="'+ButtonIcon+'" ';
				}
				if(ButtonTarget) {
					output += 'target="_blank" ';
				}
                               


				output += ']'+ButtonText+'[/button]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(AddButton.init, AddButton);

	</script>
	<title>Add Buttons</title>

</head>
<body>
<form id="GalleryShortcode">
	<p>
		<label for="ButtonColor">Button Color:</label>
		<select id="ButtonColor" name="ButtonColor">
			<option value="">Default</option>
			<option value="btn-primary">Primary</option>
			<option value="btn-info">Info</option>
			<option value="btn-success">Success</option>
			<option value="btn-warning">Warning</option>
			<option value="btn-danger">Danger</option>
			<option value="btn-inverse">Inverse</option>
			<option value="btn-link">Link</option>
		</select>
	</p>
	<p>
		<label for="ButtonSize">Button Size :</label>
		<select id="ButtonSize" name="ButtonSize">
			<option value="btn-large">Large</option>
			<option value="">Default</option>
			<option value="btn-small">Small</option>
			<option value="btn-mini">Mini</option>	
		</select>
	</p>
	<p>
		<label for="ButtonType">Button shape:</label>
		<select id="ButtonType" name="ButtonType">
			<option value="">Round (default)</option>
			<option value="square">Square</option>
		</select>
	</p>
        <p>
		<label for="ButtonPosition">Button Position:</label>
		<select id="ButtonPosition" name="ButtonPosition">
			<option value="">Left</option>
			<option value="right">Right</option>
		</select>
	</p>
        <p>
		<label for="ButtonStatus">Button Status:</label>
		<select id="ButtonStatus" name="ButtonStatus">
			<option value="">Enabled</option>
			<option value="disabled">Disabled</option>
		</select>
	</p>
        <p>
		<label for="ButtonWidth">Button Width:</label>
		<select id="ButtonWidth" name="ButtonWidth">
			<option value="">Normal</option>
			<option value="btn-block">Expand to full width</option>
		</select>
	</p>
	<p>
		<label for="ButtonLink">Button Link :</label>
		<input id="ButtonLink" name="ButtonLink" type="text" value="http://" />
	</p>
	<p>
		<label for="ButtonTarget">Open Link in a new window : </label>
		<input id="ButtonTarget" name="ButtonTarget" type="checkbox"  />
	</p>
	</p>
	<p>
		<label for="ButtonText">Button Text :</label>
		<input id="ButtonText" name="ButtonText" type="text" value="" />
	</p>
    <p>
		<label for="ButtonIcon">Button Icon :</label>
		<input id="ButtonIcon" name="ButtonIcon" type="text"/>
        <small>Choose your icon from <a href="http://fontawesome.io/icons/" target="blank">this list</a>.</small>
	</p>

	<p><a class="add" href="javascript:AddButton.insert(AddButton.e)">insert into post</a></p>
</form>
<!--/*************************************/ -->


<!--/**************/ TABS ****************/ -->

<?php } elseif( $page == 'tabs' ){ ?>

	<script type="text/javascript">
		var tabs = {
			e: '',
			init: function(e) {
				tabs.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
			
				var output = "[tabgroup]";
				
				jQuery("input[id^=tab_title]").each(function(intIndex, objValue) {
					output +='[tab title="'+jQuery(this).val()+'"]';
					var obj = jQuery('textarea[id^=Content]').get(intIndex);
					output += obj.value;
					output += "[/tab]";
				});
				
				
				output += '[/tabgroup]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(tabs.init, tabs);

		jQuery(document).ready(function() {
			jQuery("#add-tab").click(function() {
				jQuery('#TabShortcodeContent').append('<p><label for="tab_title[]">Tab Title</label><input id="tab_title[]" name="tab_title[]" type="text" value="" /></p><p><label for="Content[]">Tab Content</label><textarea  style="height:100px;  width:400px;" id="Content[]" name="Content[]" type="text" value=""></textarea></p>	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />');
			});
		});

	</script>
	<title>Add Tabs</title>

</head>
<body>
<form id="GalleryShortcode">
<div id="TabShortcodeContent">
	<p>
		<label for="tab_title[]">Tab Title</label>
		<input id="tab_title[]" name="tab_title[]" type="text" value="" />
	</p>
	<p>
		<label for="Content[]">Tab Content</label>
		<textarea style="height:100px; width:400px;" id="Content[]" name="Content[]" type="text" value="" ></textarea>
	</p>
	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />
</div>
	<strong><a style="cursor: pointer;" id="add-tab">+ Add Tab</a></strong>
	<p><a class="add" href="javascript:tabs.insert(tabs.e)">insert into post</a></p>
</form>
<!--/*************************************/ -->


<!--/************ TIMELINE ***************/ -->

<?php } elseif( $page == 'Timeline' ){ ?>

	<script type="text/javascript">
		var timeline = {
			e: '',
			init: function(e) {
				timeline.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
			
				var output = "[timeline]";
				
				jQuery("select[id^=timelineDirection]").each(function(intIndex, objValue) {
					output +='[timeline_item direction="'+jQuery(this).val()+'" ';
					var year = jQuery('input[id^=timelineYear]').get(intIndex);
					output += 'year= "'+year.value+'" ';
					var month = jQuery('input[id^=timelineMonth]').get(intIndex);
					output += 'month= "'+month.value+'" ';
					var title = jQuery('input[id^=timelineTitle]').get(intIndex);
					output += 'title= "'+title.value+'" ';
					output += ']';
					var text = jQuery('textarea[id^=timelineText]').get(intIndex);
					output+=text.value;
					output += "[/timeline_item]";
				});
				
				
				output += '[/timeline]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(timeline.init, timeline);

		jQuery(document).ready(function() {
			jQuery("#add-timeline").click(function() {
				jQuery('#TimelineShortcodeContent').append('<p><label for="timelineTitle">Title</label><input id="timelineTitle[]" name="timelineTitle[]" type="text" value="" /></p><p><label for="timelineYear[]">Year</label><input id="timelineYear[]" name="timelineYear[]" type="text" value="" /></p><p><label for="timelineMonth[]">Month</label><input id="timelineMonth[]" name="timelineMonth[]" type="text" value="" /></p><p><label for="timelineDirection[]">Animation direction</label><select id="timelineDirection[]" name="timelineDirection[]" type="text" value=""><option value="Right">Right</option><option value="Left">Left</option></select></p><p><label for="timelineText[]">Text</label><textarea id="timelineText[]" style="height:100px; width:400px;" name="timelineText[]"></textarea></p><hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />');
			});
		});

	</script>
	<title>Add Timeline</title>

</head>
<body>
<form id="GalleryShortcode">
<div id="TimelineShortcodeContent">
	<p>
		<label for="timelineTitle">Title</label>
		<input id="timelineTitle[]" name="timelineTitle[]" type="text" value="" />
	</p>
	<p>
		<label for="timelineYear[]">Year</label>
		<input id="timelineYear[]" name="timelineYear[]" type="text" value="" />
	</p>
	<p>
		<label for="timelineMonth[]">Month</label>
		<input id="timelineMonth[]" name="timelineMonth[]" type="text" value="" />
	</p>
	<p>
		<label for="timelineDirection[]">Animation direction</label>
		<select id="timelineDirection[]" name="timelineDirection[]" type="text" value="">
			<option value="Right">Right</option>
			<option value="Left">Left</option>
		</select>
	</p>
	<p>
		<label for="timelineText[]">Text</label>
		<textarea id="timelineText[]" style="height:100px; width:400px;" name="timelineText[]"></textarea>
	</p>
	
	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />
</div>
	<strong><a style="cursor: pointer;" id="add-timeline">+ Add timeline item</a></strong>
	<p><a class="add" href="javascript:timeline.insert(timeline.e)">insert into post</a></p>
</form>
<!--/*************************************/ -->
<?php } elseif( $page == 'hornav' ){ ?>

	<script type="text/javascript">
		var hornav = {
			e: '',
			init: function(e) {
				hornav.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
			
				var output = "[hornavgroup";
				
				var maintitle = jQuery('#hntitle').val();

				if(maintitle) {
					output += ' title="'+maintitle+'" ';
				}
				output += "]";
				
				jQuery("input[id^=hornav_title]").each(function(intIndex, objValue) {
					output +='[hornav title="'+jQuery(this).val()+'" ';
					var obj2 = jQuery('input[id^=hornav_link]').get(intIndex);
					output += 'link= "'+obj2.value+'"]';
					output += "[/hornav]";
				});
				
				
				output += '[/hornavgroup]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(hornav.init, hornav);

		jQuery(document).ready(function() {
			jQuery("#add-hornav").click(function() {
				jQuery('#HornavShortcodeContent').append('<p><label for="hornav_title[]">Title</label><input id="hornav_title[]" name="hornav_title[]" type="text" value="" /></p><p><label for="hornav_link[]">URL</label><input id="hornav_link[]" name="hornav_link[]" type="text" value="" /></p>	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />');
			});
		});

	</script>
	<title>Add Horizontal Navigation</title>

</head>
<body>
<form id="GalleryShortcode">
<div id="HornavShortcodeContent">
	<p>
		<label for="hntitle">Header title</label>
		<input id="hntitle" name="hntitle" type="text" value="" />
	</p>
	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />
	<p>
		<label for="hornav_title[]">Title</label>
		<input id="hornav_title[]" name="hornav_title[]" type="text" value="" />
	</p>
	<p>
		<label for="hornav_link[]">URL</label>
		<input id="hornav_link[]" name="hornav_link[]" type="text" value="" />
	</p>
	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />
</div>
	<strong><a style="cursor: pointer;" id="add-hornav">+ Add navigation item</a></strong>
	<p><a class="add" href="javascript:hornav.insert(hornav.e)">insert into post</a></p>
</form>
<!--/*************************************/ -->


<?php } elseif( $page == 'serviceblock' ){ ?>

	<script type="text/javascript">
		var serviceblock = {
			e: '',
			init: function(e) {
				serviceblock.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
			
				var output = "[servicegroup]";
				
				jQuery("input[id^=sb_title]").each(function(intIndex, objValue) {
					output +='[serviceblock title="'+jQuery(this).val()+'" ';
					var link = jQuery('input[id^=sb_link]').get(intIndex);
					output += 'link= "'+link.value+'" ';
					var caption = jQuery('input[id^=sb_urltitle]').get(intIndex);
					output += 'caption= "'+caption.value+'" ';
					
					var icon = jQuery('input[id^=sb_icon]').get(intIndex);
					output += 'icon= "'+icon.value+'" ';
					
					var size = jQuery('select[id^=sb_size]').get(intIndex);
					output += 'colsize= "'+size.value+'" ';
					output+=']';
					var text = jQuery('textarea[id^=sb_content]').get(intIndex);
					output += text.value;
					output += "[/serviceblock]";
				});
				
				
				output += '[/servicegroup]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(serviceblock.init, serviceblock);

		jQuery(document).ready(function() {
			jQuery("#add-sb").click(function() {
				jQuery('#SbShortcodeContent').append('<p><label for="sb_title[]">Title</label><input id="sb_title[]" name="sb_title[]" type="text" value="" /></p><p><label for="sb_content[]">Content</label><textarea style="height:100px; width:400px;" id="sb_content[]" name="sb_content[]" type="text" value="" ></textarea></p><p><label for="sb_icon[]">Icon</label><input id="sb_icon[]" name="sb_icon[]" type="text" value="" /></p><p><label for="sb_link[]">URL</label><input id="sb_link[]" name="sb_link[]" type="text" value="" /></p><p><label for="sb_urltitle[]">URL title</label><input id="sb_urltitle[]" name="sb_urltitle[]" type="text" value="" /></p><p><label for="sb_size[]">Column size</label><select id="sb_size[]" name="sb_size[]"><option value="col-sm-6">1/2</option><option value="col-sm-4">1/3</option><option value="col-sm-3">1/4</option><option value="col-sm-2">1/6</option></select></p><hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />');
			});
		});

	</script>
	<title>Add Service block</title>

</head>
<body>
<form id="GalleryShortcode">
<div id="SbShortcodeContent">
	<p>
		<label for="sb_title[]">Title</label>
		<input id="sb_title[]" name="sb_title[]" type="text" value="" />
	</p>
	<p>
		<label for="sb_content[]">Content</label>
		<textarea style="height:100px; width:400px;" id="sb_content[]" name="sb_content[]" type="text" value="" ></textarea>
	</p>
	<p>
		<label for="sb_icon[]">Icon</label>
		<input id="sb_icon[]" name="sb_icon[]" type="text" value="" />
	</p>
	<p>
		<label for="sb_link[]">URL</label>
		<input id="sb_link[]" name="sb_link[]" type="text" value="" />
	</p>
	<p>
		<label for="sb_urltitle[]">URL title</label>
		<input id="sb_urltitle[]" name="sb_urltitle[]" type="text" value="" />
	</p>
	<p>
		<label for="sb_size[]">Column size</label>
		<select id="sb_size[]" name="sb_size[]">
			<option value="col-sm-6">1/2</option>
			<option value="col-sm-4">1/3</option>
			<option value="col-sm-3">1/4</option>
			<option value="col-sm-2">1/6</option>
		</select>
	</p>
	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />
</div>
	<strong><a style="cursor: pointer;" id="add-sb">+ Add service block</a></strong>
	<p><a class="add" href="javascript:serviceblock.insert(serviceblock.e)">insert into post</a></p>
</form>



<?php } elseif( $page == 'toggle' ){ ?>

	<script type="text/javascript">
		var toggle = {
			e: '',
			init: function(e) {
				toggle.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
			
				var output = "[togglegroup]";
				
				jQuery("input[id^=toggle_title]").each(function(intIndex, objValue) {
					output +='[toggle title="'+jQuery(this).val()+'"]';
					var obj = jQuery('textarea[id^=Content]').get(intIndex);
					output += obj.value;
					output += "[/toggle]";
				});
				
				
				output += '[/togglegroup]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(toggle.init, toggle);

		jQuery(document).ready(function() {
			jQuery("#add-toggle").click(function() {
				jQuery('#ToggleShortcodeContent').append('<p><label for="toggle_title[]">Toggle Title</label><input id="toggle_title[]" name="toggle_title[]" type="text" value="" /></p><p><label for="Content[]">Toggle Content</label><textarea  style="height:100px;  width:400px;" id="Content[]" name="Content[]" type="text" value=""></textarea></p>	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />');
			});
		});

	</script>
	<title>Add Toggle</title>

</head>
<body>
<form id="TogglesShortcode">
<div id="ToggleShortcodeContent">
	<p>
		<label for="toggle_title[]">Toggle Title</label>
		<input id="toggle_title[]" name="toggle_title[]" type="text" value="" />
	</p>
	<p>
		<label for="Content[]">Toggle Content</label>
		<textarea style="height:100px; width:400px;" id="Content[]" name="Content[]" type="text" value="" ></textarea>
	</p>
	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />
</div>
	<strong><a style="cursor: pointer;" id="add-toggle">+ Add Toggle</a></strong>
	<p><a class="add" href="javascript:toggle.insert(toggle.e)">insert into post</a></p>
</form>
<!--/*************************************/ -->

<?php } elseif( $page == 'accordion' ){ ?>

	<script type="text/javascript">
		var accordion = {
			e: '',
			init: function(e) {
				accordion.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
			
				var output = "[accordiongroup]";
				
				jQuery("input[id^=accordion_title]").each(function(intIndex, objValue) {
					output +='[accordion title="'+jQuery(this).val()+'"]';
					var obj = jQuery('textarea[id^=Content]').get(intIndex);
					output += obj.value;
					output += "[/accordion]";
				});
				
				
				output += '[/accordiongroup]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(accordion.init, accordion);

		jQuery(document).ready(function() {
			jQuery("#add-accordion").click(function() {
				jQuery('#accordionShortcodeContent').append('<p><label for="accordion_title[]">accordion Title</label><input id="accordion_title[]" name="accordion_title[]" type="text" value="" /></p><p><label for="Content[]">accordion Content</label><textarea  style="height:100px;  width:400px;" id="Content[]" name="Content[]" type="text" value=""></textarea></p>	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />');
			});
		});

	</script>
	<title>Add accordion</title>

</head>
<body>
<form id="accordionsShortcode">
<div id="accordionShortcodeContent">
	<p>
		<label for="accordion_title[]">Title</label>
		<input id="accordion_title[]" name="accordion_title[]" type="text" value="" />
	</p>
	<p>
		<label for="Content[]">Content</label>
		<textarea style="height:100px; width:400px;" id="Content[]" name="Content[]" type="text" value="" ></textarea>
	</p>
	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />
</div>
	<strong><a style="cursor: pointer;" id="add-accordion">+ Add accordion tab</a></strong>
	<p><a class="add" href="javascript:accordion.insert(accordion.e)">insert into post</a></p>
</form>
<!--/*************************************/ -->

<?php } elseif( $page == 'testimonial' ){ ?>
	<script type="text/javascript">
		var Testimonial = {
			e: '',
			init: function(e) {
				Testimonial.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {

				var output = "[testimonialgroup]";
				
				jQuery("input[id^=authorName]").each(function(intIndex, objValue) {
					output +='[testimonial name="'+jQuery(this).val()+'"';
					var photo = jQuery('input[id^=authorPhoto]').get(intIndex);
					if (photo) output += ' photo="'+photo.value+'" ';
					var position = jQuery('input[id^=authorPosition]').get(intIndex);
					if (position) output += ' position="'+position.value+'"';
					output += "]";
					var obj = jQuery('textarea[id^=Content]').get(intIndex);
					output += obj.value;
					output += "[/testimonial]";
				});
				
				
				output += '[/testimonialgroup]';
				
				
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(Testimonial.init, Testimonial);
		jQuery(document).ready(function() {
			jQuery("#add-testimonial").click(function() {
				jQuery('#testimonialShortcodeContent').append('<p><label for="authorName[]">Author Name</label><input id="authorName[]" name="authorName[]" type="text" value="" /></p><p><label for="authorPosition[]">Author Position</label><input id="authorPosition[]" name="authorPosition[]" type="text" value="" /></p><p><label for="authorPhoto[]">Author Photo URL</label><input id="authorPhoto[]" name="authorPhoto[]" type="text" value="" /></p><p><label for="Content[]">Text</label><textarea  style="height:100px;  width:400px;" id="Content[]" name="Content[]" type="text" value=""></textarea></p>	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />');
			});
		});

	</script>
	<title>Insert Testimonial</title>

</head>
<body>
<form id="GalleryShortcode">
	<div id="testimonialShortcodeContent">
		<p>
			<label for="authorName[]">Author Name</label>
			<input id="authorName[]" name="authorName[]" type="text" value="" />
		</p>
		<p>
			<label for="authorPosition[]">Author Position</label>
			<input id="authorPosition[]" name="authorPosition[]" type="text" value="" />
		</p>
		<p>
			<label for="authorPhoto[]">Author Photo URL</label>
			<input id="authorPhoto[]" name="authorPhoto[]" type="text" value="" />
		</p>
		<p>
			<label for="Content[]">Text : </label>
			<textarea id="Content[]" name="Content[]" style="width:400px; height:100px"></textarea>
		</p>
		<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />
	</div>
	<strong><a style="cursor: pointer;" id="add-testimonial">+ Add another testimonial</a></strong>
	<p><a class="add" href="javascript:Testimonial.insert(Testimonial.e)">insert into post</a></p>
</form>
<!--/*************************************/ -->

<?php } elseif( $page == 'alert' ){ ?>

	<script type="text/javascript">
		var alert = {
			e: '',
			init: function(e) {
				alert.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
                            
                var alertTitle=jQuery('#alertTitle').val();
				var alertType = jQuery('#alertType').val();
				var Content = jQuery('#Content').val();

				var output = '[alert ';
				
				if(alertTitle){
					output+= 'title="'+alertTitle+'" ';
				}
				if(alertType) {
					output += 'type="'+alertType+'"';
				}
			
				output += ']'+Content+'[/alert]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(alert.init, alert);

	</script>
	<title>Add Alert box</title>

</head>
<body>
<form id="GalleryShortcode">
	<p>
		<label for="alertType">Type :</label>
		<select id="alertType" name="alertType">
			<option value="alert-warning">Warning (Yellow)</option>
			<option value="alert-success">Success (Green)</option>
			<option value="alert-error">Error (Red)</option>
			<option value="alert-info">Info (Blue)</option>
		</select>
	</p>
	<p>
		<label for="alertTitle">Title :</label>
		<input type="text" id="alertTitle" name="alertTitle" />
	</p>
	<p>
		<label for="Content">Content : </label>
		<textarea id="Content" name="Content" col="20"></textarea>
	</p>
	
	
	<p><a class="add" href="javascript:alert.insert(alert.e)">insert into post</a></p>
</form>
<!--/*************************************/ -->

<?php } elseif( $page == 'video' ){ ?>

	<script type="text/javascript">
		var Video = {
			e: '',
			init: function(e) {
				Video.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {

				var site = jQuery('#site').val();
				var id = jQuery('#id').val();
				var width = jQuery('#width').val();
				var height = jQuery('#height').val();
				var autoplay = jQuery('#autoplay').val();

				var output = '[video ';
				
				if(id) {
					output += 'id="'+id+'" ';
				}
				
				if(site) {
					output += ' site="'+site+'" ';
				}
				
				if(width) {
					output += ' width="'+width+'" ';
				}
				if(height) {
					output += ' height="'+height+'" ';
				}
				
				if(autoplay) {
					output += ' autoplay="'+autoplay+'" ';
				}

				output += ' /]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(Video.init, Video);

	</script>
	<title>Add Video</title>

</head>
<body>
<form id="GalleryShortcode">
	<p>
		<label for="site">Website : </label>
		<select id="site" name="site">
			<option value="youtube">Youtube</option>
			<option value="vimeo">Vimeo</option>
			<option value="dailymotion">Dailymotion</option>
			<option value="bliptv">BlipTV</option>
			<option value="veoh">Veoh</option>
			<option value="viddler">Viddler</option>
		</select>
	</p>
	<p>
		<label for="id">Id (Copy the ID from video URL here) :</label>
		<input id="id" name="id" type="text" value="" />
	</p>
	<p>
		<label for="width">Width :</label>
		<input style="width:40px;" id="width" name="width" type="text" value="" />
	</p>
	<p>
		<label for="height">Height :</label>
		<input style="width:40px;"  id="height" name="height" type="text" value="" />
	</p>
	<p>
		<label for="autoplay">Autoplay : </label>
		<select id="autoplay" name="autoplay">
			<option value="0">No</option>
			<option value="1">Yes</option>
		</select>
	</p>
	
	<p><a class="add" href="javascript:Video.insert(Video.e)">insert into post</a></p>
</form>
<!--/*************************************/ -->

<?php } elseif( $page == 'twitter' ){ ?>

	<script type="text/javascript">
		var AddTwitter = {
			e: '',
			init: function(e) {
				AddTwitter.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
				
				var title = jQuery('#twitterTitle').val();
				var username = jQuery('#twitterUsername').val();
				var key = jQuery('#twitterKey').val();
				var secret = jQuery('#twitterSecret').val();
				var token = jQuery('#twitterToken').val();
				var tokensecret = jQuery('#twitterTokensecret').val();
				var limit = jQuery('#twitterLimit').val();
				
				var output = '[twitter ';
				
				if(title) {
					output += 'title="'+title+'" ';
				}	
				if(username) {
					output += ' twitter_username="'+username+'" ';
				}	
				if(key) {
					output += ' consumer_key="'+key+'" ';
				}				
				if(secret) {
					output += ' consumer_secret="'+secret+'" ';
				}				
				if(token) {
					output += ' access_token="'+token+'" ';
				}
				if(tokensecret) {
					output += ' access_token_secret="'+tokensecret+'" ';
				}
				if(limit) {
					output += ' limit="'+limit+'" ';
				}

				output += ' /]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(AddTwitter.init, AddTwitter);

	</script>
	<title>Add twitter listing</title>

</head>
<body>
<form id="GalleryShortcode">
	<p>
		<label for="twitterTitle">Title : </label>
		<input id="twitterTitle" name="twitterTitle" type="text" value="" />
	</p>
	<p>
		<label for="twitterUsername">Username : </label>
		<input id="twitterUsername" name="twitterUsername" type="text" value="" />
	</p>
	<p>
		<label for="twitterKey">Consumer Key : </label>
		<input id="twitterKey" name="twitterKey" type="text" value="" />
	</p>
	<p>
		<label for="twitterSecret">Consumer Secret : </label>
		<input id="twitterSecret" name="twitterSecret" type="text" value="" />
	</p>
	<p>
		<label for="twitterToken">Access Token : </label>
		<input id="twitterToken" name="twitterToken" type="text" value="" />
	</p>
	<p>
		<label for="twitterTokensecret">Access Token secret : </label>
		<input id="twitterTokensecret" name="twitterTokensecret" type="text" value="" />
	</p>
	<p>
		<label for="twitterLimit">Limit : </label>
		<select id="twitterLimit" name="twitterLimit">
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			<option value="6">6</option>
			<option value="7">7</option>
		</select>
	</p>
	<p><small>Check this <a href="http://www.youtube.com/watch?v=iL4b3Tiwy1I" target="blank">video guide</a> if you are not sure where to find the details listed.</small></p>
	
	<p><a class="add" href="javascript:AddTwitter.insert(AddTwitter.e)">insert into post</a></p>
</form>
<!--/*************************************/ -->
<?php } elseif( $page == 'shvideo' ){ ?>

	<script type="text/javascript">
		var shVideo = {
			e: '',
			init: function(e) {
				shVideo.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
				
				var title = jQuery('#videoTitle').val();
				var poster = jQuery('#videoPoster').val();
				var mp4Url = jQuery('#mp4Url').val();
				var m4vUrl = jQuery('#m4vUrl').val();
				var ogvUrl = jQuery('#ogvUrl').val();

				var output = '[shvideo ';
				
				if(title) {
					output += 'title="'+title+'" ';
				}	
				if(poster) {
					output += ' poster="'+poster+'" ';
				}	
				if(mp4Url) {
					output += 'mp4="'+mp4Url+'" ';
				}				
				if(m4vUrl) {
					output += 'm4v="'+m4vUrl+'" ';
				}				
				if(ogvUrl) {
					output += 'ogv="'+ogvUrl+'" ';
				}

				output += ' /]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(shVideo.init, shVideo);

	</script>
	<title>Add Native video</title>

</head>
<body>
<form id="GalleryShortcode">
	<p>
		<label for="videoTitle">Title : </label>
		<input id="videoTitle" name="videoTitle" type="text" value="" />
	</p>
	<p>
		<label for="videoPoster">Poster image : </label>
		<input id="videoPoster" name="videoPoster" type="text" value="" />
	</p>
	<p>
		<label for="mp4Url">Mp4 file Url : </label>
		<input id="mp4Url" name="mp4Url" type="text" value="" />
	</p>
	<p>
		<label for="m4vUrl">M4V file Url : </label>
		<input id="m4vUrl" name="m4vUrl" type="text" value="" />
	</p>
	<p>
		<label for="ogvUrl">OGV file Url : </label>
		<input id="ogvUrl" name="ogvUrl" type="text" value="" />
	</p>

	
	<p><a class="add" href="javascript:shVideo.insert(shVideo.e)">insert into post</a></p>
</form>

<!--/*************************************/ -->


<?php } elseif( $page == 'slider' ){ ?>
	
	<script type="text/javascript">
		var Slider = {
			e: '',
			init: function(e) {
				Slider.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
			
				var output = "[slider ";
				var interval = jQuery('#interval').val();
				var animation = jQuery('#slideranimationtype').val();
				if(interval) {
					output += 'interval="'+interval+'"';
				}
				if(animation) {
					output += ' animation="'+animation+'"';
				}
				output += "]";
				
				jQuery("input[id^=slide_title]").each(function(intIndex, objValue) {
					output +='[slideritem title="'+jQuery(this).val()+'"';
					var obj = jQuery('input[id^=slide_image]').get(intIndex);
					output += ' image="'+ obj.value +'"]';
					output += "[/slideritem]";
				});
				
				
				output += '[/slider]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(Slider.init, Slider);

		jQuery(document).ready(function() {
			jQuery("#add-slide").click(function() {
				jQuery('#SlideShortcodeContent').append('<p><label for="slide_title[]">Slide Title</label><input id="slide_title[]" name="slide_title[]" type="text" value="" /></p><p><label for="slide_image[]">Slide Image URL</label><input id="slide_image[]" name="slide_image[]" type="text" value="http://" /></p>	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />');
			});
		});
		
	</script>
	<title>Add Slider</title>

</head>
<body>

<form id="SliderShortcode">
<div id="SlideShortcodeContent">
	<p>
		<label for="interval">Interval (in milliseconds)</label>
		<input id="interval" name="interval" type="text" value="5000" />
	</p>
	<p>
		<label for="slideranimationtype">Animation type</label>
		<select id="slideranimationtype" name="slideranimationtype">
			<option value="fade">Fade</option>
			<option value="slide">Slide</option>
		</select>
	</p>
	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />
	<p>
		<label for="slide_title[]">Slide Title</label>
		<input id="slide_title[]" name="slide_title[]" type="text" value="" />
	</p>
	<p>
		<label for="slide_image[]">Slide Image URL</label>
		<input id="slide_image[]" name="slide_image[]" type="text" value="" />
	</p>
	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />
</div>
	<strong><a style="cursor: pointer;" id="add-slide">+ Add Slide</a></strong>
	<p><a class="add" href="javascript:Slider.insert(Slider.e)">insert into post</a></p>
</form>
<!--/*************************************/ -->
<?php } elseif( $page == 'oslider' ){ ?>
	
	<script type="text/javascript">
		var Oslider = {
			e: '',
			init: function(e) {
				Oslider.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
			
				var output = "[oslider ";
				var interval = jQuery('#interval').val();
				
				if(interval) {
					output += 'interval="'+interval+'"';
				}
				
				output += "]";
				
				jQuery("input[id^=slide_title]").each(function(intIndex, objValue) {
					output +='[oslideritem title="'+jQuery(this).val()+'"';
					var obj = jQuery('input[id^=slide_image]').get(intIndex);
					output += ' image="'+ obj.value +'"]';
					output += "[/oslideritem]";
				});
				
				
				output += '[/oslider]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(Oslider.init, Oslider);

		jQuery(document).ready(function() {
			jQuery("#add-slide").click(function() {
				jQuery('#SlideShortcodeContent').append('<p><label for="slide_title[]">Slide Title</label><input id="slide_title[]" name="slide_title[]" type="text" value="" /></p><p><label for="slide_image[]">Slide Image URL</label><input id="slide_image[]" name="slide_image[]" type="text" value="http://" /></p>	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />');
			});
		});
		
	</script>
	<title>Add Orbit Slider</title>

</head>
<body>

<form id="SliderShortcode">
<div id="SlideShortcodeContent">
	<p>
		<label for="interval">Interval (in milliseconds)</label>
		<input id="interval" name="interval" type="text" value="4000" />
	</p>
	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />
	<p>
		<label for="slide_title[]">Slide Title</label>
		<input id="slide_title[]" name="slide_title[]" type="text" value="" />
	</p>
	<p>
		<label for="slide_image[]">Slide Image URL</label>
		<input id="slide_image[]" name="slide_image[]" type="text" value="" />
	</p>
	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />
</div>
	<strong><a style="cursor: pointer;" id="add-slide">+ Add Slide</a></strong>
	<p><a class="add" href="javascript:Oslider.insert(Oslider.e)">insert into post</a></p>
</form>
<!--/*************************************/ -->

<?php } elseif( $page == 'carousel' ){ ?>
	
	<script type="text/javascript">
		var Carousel = {
			e: '',
			init: function(e) {
				Carousel.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
			
				var output = "[carousel ";
				var title = jQuery('#carouselTitle').val();
				
				if(title) {
					output += 'title="'+title+'"';
				}
				
				output += "]";
				
				jQuery("textarea[id^=carousel_content]").each(function(intIndex, objValue) {
					output +='[caritem]'+jQuery(this).val()+'"[/caritem]';
				});
				
				output += '[/carousel]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(Carousel.init, Carousel);

		jQuery(document).ready(function() {
			jQuery("#add-carousel").click(function() {
				jQuery('#SlideShortcodeContent').append('<p><label for="carousel_content[]">Slide Content</label><textarea style="height:100px; width:400px;" id="carousel_content[]" name="carousel_content[]" type="text" value="" ></textarea></p>');
			});
		});
		
	</script>
	<title>Add Carousel slide</title>

</head>
<body>

<form id="CarouselShortcode">
<div id="SlideShortcodeContent">
	<p>
		<label for="carouselTitle">Carousel title</label>
		<input id="carouselTitle" name="carouselTitle" type="text" value="" />
	</p>
	<p>
		<label for="carousel_content[]">Slide Content</label>
		<textarea style="height:100px; width:400px;" id="carousel_content[]" name="carousel_content[]" type="text" value="" ></textarea>
	</p>
</div>
	<strong><a style="cursor: pointer;" id="add-carousel">+ Add slide</a></strong>
	<p><a class="add" href="javascript:Carousel.insert(Carousel.e)">insert into post</a></p>
</form>
<!--/*************************************/ -->

<?php } elseif( $page == 'contact' ){ ?>
	<script type="text/javascript">
		
		var Contact = {
			e: '',
			init: function(e) {
				Contact.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {

				var address = jQuery('#Contactaddress').val();
				var tel = jQuery('#Contacttel').val();
				var email = jQuery('#Contactemail').val();
				var skype = jQuery('#Contactskype').val();
				var Content = jQuery('#Contactcontent').val();

				var output = '[contact ';
				
				if(address) {
					output += 'address="'+address+'" ';
				}
				
				if(tel) {
					output += 'tel="'+tel+'" ';
				}
				
				if(email) {
					output += 'email="'+email+'" ';
				}
				
				if(skype) {
					output += 'skype="'+skype+'" ';
				}
				
				output += ']'+Content+'[/contact]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(Contact.init, Contact);

	</script>
	<title>Insert contact details</title>

</head>
<body>

<form id="GalleryShortcode">
	<p>
		<label for="Contactaddress">Address</label>
		<input id="Contactaddress" name="Contactaddress" type="text" value="" />
	</p>
	<p>
		<label for="Contacttel">Telephone</label>
		<input id="Contacttel" name="Contacttel" type="text" value="" />
	</p>
	<p>
		<label for="Contactemail">E-mail</label>
		<input id="Contactemail" name="Contactemail" type="text" value="" />
	</p>
	<p>
		<label for="Contactskype">Skype</label>
		<input id="Contactskype" name="Contactskype" type="text" value="" />
	</p>
	<p>
		<label for="Contactcontent">Content : </label>
		<textarea id="Contactcontent" name="Contactcontent" col="20" style="width:200px; height:50px"></textarea>
	</p>
	
	<p><a class="add" href="javascript:Contact.insert(Contact.e)">insert into post</a></p>
</form>
<!--/*************************************/ -->

<?php } elseif($page=='fblock') {?>
    <script type="text/javascript">
        var fblock={
            e: '',
            init: function(e){
                fblock.e=e,
                tinyMCEPopup.resizeToInnerSize();
            },
            insert: function createGalleryShortcode(e){
                var Title=jQuery('#fblockTitle').val();
                var Icon=jQuery('#fblockIcon').val();
                var Fcontent=jQuery('#fblockContent').val();
                var image=jQuery('#fblockImage').val();
				var animation = jQuery('#fblockAnimation').val();
				var color = jQuery('#fblockIconColor').val();
				var link =jQuery('#fblockLink').val();
				
                
                var output='[fblock';
                if(Title){
                    output+=' title="'+Title+'"';
                }
				if(image){
                    output+=' image="'+image+'"';
                }
				if(color){
                    output+=' color="'+color+'"';
                }
				if(link){
                    output+=' link="'+link+'"';
                }
                if(Icon){
                    output+=' icon="'+Icon+'"';
                }
				if(animation){
                    output+=' animation="'+animation+'"';
                }
                
                output+=']'+Fcontent+'[/fblock]';
                tinyMCEPopup.execCommand('mceReplaceContent', false, output);
		tinyMCEPopup.close();
            }
        }
        tinyMCEPopup.onInit.add(fblock.init, fblock);
    </script>
    <title>Insert Featured Block</title>
</head>
<body>
    <form id="GalleryShortcode">
		<p>
            <label for="fblockTitle">Block Title:</label>
            <input type="text" id="fblockTitle">
        </p>
		
        <p>
            <label for="fblockIcon">Block Icon:</label>
            <input type="text" id="fblockIcon">
            <small>Choose your icon from <a href="http://fontawesome.io/icons/" target="blank">this list</a>.</small>
        </p>
		<p>
            <label for="fblockIconColor">Icon color:</label>
            <input type="text" id="fblockIconColor">
            <small>Applies only for font awesome list icons above.</small>
        </p>
		<p>
            <label for="fblockImage">Block Image:</label>
            <input type="text" id="fblockImage">
            <small>If set, will override the icon parameter. Specify an absolute url.</small>
        </p>
		<p>
            <label for="fblockLink">Icon Link:</label>
            <input type="text" id="fblockLink" />
            <small>Optional</small>
        </p>
		<p>
			<label for="fblockAnimation">Animation Type :</label>
			<select id="fblockAnimation" name="fblockAnimation">
				<option value="fadeLeft">Fade left</option>
				<option value="fadeRight">Fade right</option>
				<option value="fadeTop">Fade top</option>
				<option value="fadeBottom">Fade bottom</option>		
			</select>
		</p>
        <p>
            <label for="fblockContent">Block Content:</label>
            <textarea id="fblockContent" style="width:200px; height:50px"></textarea>
			<small>This field is optional</span>
        </p>
      
        <p><a class="add" href="javascript:fblock.insert(fblock.e)">insert into post</a></p>
        
    </form>
<!--/*************************************/ -->

<?php } elseif($page=='tblock'){ ?>
<script type="text/javascript">
    var tblock={
        e:'',
        init:function(e){
            tblock.e=e;
            tinyMCEPopup.resizeToInnerSize();
        },
        insert:function createGalleryShortcode(e){
            var Title=jQuery('#tblockTitle').val();
            var Image=jQuery('#tblockImage').val();
            
            var output='[tblock';
            if(Title){
                output+=' title="'+Title+'"';
            }
           
            if(Image){
                output+=' image="'+Image+'"';
            }
            output+='/]';
            tinyMCEPopup.execCommand('mceReplaceContent', false, output);
            tinyMCEPopup.close();
        }
    }
    tinyMCEPopup.onInit.add(tblock.init, tblock);
</script>
<title>Add Title Block</title>
</head>
<body>
    <form id="GalleryShortcode">
        <p>
            <label for="tblockTitle">Block Title:</label>
            <input type="text" id="tblockTitle">
        </p>
        
        <p>
            <label for="tblockImage">Block Icon (Ex. fa-copy, fa-globe, fa-comment):</label>
            <input type="text" id="tblockImage">
            <small>Choose your icon from <a href="http://fontawesome.io/icons/" target="blank">this list</a>.</small>
        </p>
        <p><a class="add" href="javascript:tblock.insert(tblock.e)">insert into post</a></p>
        
    </form>
<!--/*************************************/ -->
<?php } elseif($page=='reveal') { ?>
<script type="text/javascript">
    var reveal={
        e:'',
        init:function(e){
            reveal.e=e;
            tinyMcePopup.resizeToInnerSize();
        },
        insert: function createGalleryShortcode(e){
            var ButtonColor = jQuery('#ButtonColor').val();
            var Buttonsize = jQuery('#Buttonsize').val();
            var Buttontype = jQuery('#ButtonType').val();
            var Buttontext = jQuery('#Buttontext').val();
            
            var RevTitle = jQuery('#revTitle').val();
            var RevContent = jQuery('#revContent').val();
            
            var output = '[reveal';
            if(ButtonColor) {
                output += ' color="'+ButtonColor+'" ';
            }
            if(Buttonsize) {
                output += ' size="'+Buttonsize+'" ';
            }
            if(Buttontype) {
                output += ' type="'+Buttontype+'" ';
            }
            if(Buttontext){
                output+=' button="'+Buttontext+'"';
            }
           
            if(RevTitle){
                output+=' revtitle="'+RevTitle+'"';
            }
            

            output += ']'+RevContent+'[/reveal]';
            tinyMCEPopup.execCommand('mceReplaceContent', false, output);
            tinyMCEPopup.close();
	
	}
}
tinyMCEPopup.onInit.add(reveal.init, reveal);

</script>
<title>Add Reveal Box</title>
</head>
<body>
    <form id="GalleryShortcode">
	<p>
		<label for="ButtonColor">Button Color:</label>
		<select id="ButtonColor" name="ButtonColor">
			<option value="">Default</option>
			<option value="btn-primary">Primary</option>
			<option value="btn-info">Info</option>
			<option value="btn-success">Success</option>
			<option value="btn-warning">Warning</option>
			<option value="btn-danger">Danger</option>
			<option value="btn-inverse">Inverse</option>
			<option value="btn-link">Link</option>
		</select>
	</p>
	<p>
		<label for="ButtonSize">Button Size :</label>
		<select id="ButtonSize" name="ButtonSize">
			<option value="btn-lg">Large</option>
			<option value="">Default</option>
			<option value="btn-sm">Small</option>
			<option value="btn-xs">Mini</option>	
		</select>
	</p>
	<p>
		<label for="ButtonType"> Button Shape:</label>
		<select id="ButtonType" name="ButtonType">
			<option value="">Round (default)</option>
			<option value="square">Square</option>
		</select>
	</p>
	<p>
		<label for="Buttontext">Button Text :</label>
		<input id="Buttontext" name="Buttontext" type="text" value="" />
	</p>
        <hr>
       
        <p>
            <label for="revTitle">Reveal Box Title</label>
            <input type="text" id="revTitle" name="revTitle">
        </p>
        <p>
            <label for="revContent">Reveal Box Content</label>
            <textarea id="revContent" name="revContent" col="20"></textarea>
        </p>

	<p><a class="add" href="javascript:reveal.insert(reveal.e)">insert into post</a></p>
</form>
<!--/*************************************/ -->


<!--/*************************************/ -->

<?php } elseif($page=='social') { ?>
<script type="text/javascript">
    var social={
        e:'',
        init:function(e){
            social.e=e;
            tinyMCEPopup.resizeToInnerSize();
        },
        insert: function createGalleryShortCode(e){
            
            var Icon=jQuery('#icon').val();
            var Link=jQuery('#link').val();
            
            var output = '[social]';
            jQuery("select[id^=icon]").each(function(intIndex, objValue) {
		output +='[soc_button icon="'+jQuery(this).val()+'"';
		var obj = jQuery('input[id^=link]').get(intIndex);
		output += ' link="'+obj.value+'" ';
		output += "/]";
            });
				
            output += '[social/]';
            tinyMCEPopup.execCommand('mceReplaceContent', false, output);
            tinyMCEPopup.close();
        }
    }
    tinyMCEPopup.onInit.add(social.init, social);
    jQuery(document).ready(function() {
        jQuery("#add-social").click(function() {
            jQuery('#SocShortcodeContent').append('<p><label for="icon[]">Social Button</label><select id="icon[]" name="icon[]"><option value="fa-bitbucket">Bitbucket</option><option value="fa-dribbble">Dribble</option><option value="fa-facebook">Facebook</option><option value="fa-flickr">Flickr</option><option value="fa-github">Github</option><option value="fa-google-plus">Google+</option><option value="fa-instagram">Instagram</option><option value="fa-linkedin">LinkedIn</option><option value="fa-pinterest">Pinterest</option><option value="fa-skype">Skype</option><option value="fa-stack-exchange">Stackexchange</option>        <option value="fa-tumblr">Tumblr</option><option value="fa-twitter">Twitter</option><option value="fa-vk">Vkontakte</option><option value="fa-youtube">Youtube</option></select></p><p><label for="link[]">Link to:</label><input type="text" id="link[]" name="link[]"></p><hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />');
    });
    });
</script>
<title>Add Social Button</title>
</head>
<body>
    <form id="GalleryShortcode">
        <div id="SocShortcodeContent">
            <p>
            <label for="icon[]">Social Button</label>
            <select id="icon[]" name="icon[]">
                <option value="fa-bitbucket">Bitbucket</option>
				<option value="fa-dribbble">Dribble</option>
				<option value="fa-facebook">Facebook</option>
                <option value="fa-flickr">Flickr</option>
				<option value="fa-github">Github</option>
				<option value="fa-google-plus">Google+</option>
                <option value="fa-instagram">Instagram</option>
				<option value="fa-linkedin">LinkedIn</option>
				<option value="fa-pinterest">Pinterest</option>
				<option value="fa-skype">Skype</option>
				<option value="fa-stackexchange">Stackexchange</option>        
				<option value="fa-tumblr">Tumblr</option>
				<option value="fa-twitter">Twitter</option>
                <option value="fa-vk">Vkontakte</option>
				<option value="fa-youtube">Youtube</option>
		    </select>
            </p>
            <p>
                <label for="link[]">Link to (without http):</label>
                <input type="text" id="link[]" name="link[]">
            </p>
            <p>
                <hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />  
            </p>
        </div>
        <strong><a style="cursor: pointer;" id="add-social">+ Add Social Button</a></strong>
        <p>
            <a class="add" href="javascript:social.insert(social.e)">Insert into post</a>
        </p>
    </form>
<!--/*************************************/ -->


<?php } elseif($page=='teammember') { ?>
<script type="text/javascript">
    var social={
        e:'',
        init:function(e){
            social.e=e;
            tinyMCEPopup.resizeToInnerSize();
        },
        insert: function createGalleryShortCode(e){
            
            var Icon=jQuery('#tmicon').val();
            var Link=jQuery('#tmlink').val();
			var name = jQuery('#memberName').val();
			var size = jQuery('#memberSize').val();
			var info = jQuery('#memberInfo').val();
			var photo = jQuery('#memberPhoto').val();

			var output = '[teammember';
			
			if(name) {
				output += ' name="'+name+'"';
			}
			if(size) {
				output += ' size="'+size+'"';
			}
			if(photo) {
				output += ' photo="'+photo+'"';
			}
			if(info) {
				output += ' info="'+info+'"';
			}
			output += ']';
			
			jQuery("select[id^=tmicon]").each(function(intIndex, objValue) {
		output +='[tmsocbutton icon="'+jQuery(this).val()+'"';
		var obj = jQuery('input[id^=tmlink]').get(intIndex);
		output += ' link="'+obj.value+'" ';
		output += "/]";
            });
				
            output += '[/teammember]';
            tinyMCEPopup.execCommand('mceReplaceContent', false, output);
            tinyMCEPopup.close();
        }
    }
    tinyMCEPopup.onInit.add(social.init, social);
    jQuery(document).ready(function() {
        jQuery("#add-social").click(function() {
            jQuery('#TeamMemberContent').append('<p><label for="icon[]">Social Button</label><select id="tmicon[]" name="tmicon[]"><option value="fa-bitbucket">Bitbucket</option><option value="fa-dribbble">Dribble</option><option value="fa-facebook">Facebook</option><option value="fa-flickr">Flickr</option><option value="fa-github">Github</option><option value="fa-google-plus">Google+</option><option value="fa-instagram">Instagram</option><option value="fa-linkedin">LinkedIn</option><option value="fa-pinterest">Pinterest</option><option value="fa-skype">Skype</option><option value="fa-stack-exchange">Stackexchange</option><option value="fa-tumblr">Tumblr</option><option value="fa-twitter">Twitter</option><option value="fa-vk">Vkontakte</option><option value="fa-youtube">Youtube</option></select></p><p><label for="tmlink[]">Link to:</label><input type="text" id="tmlink[]" name="tmlink[]"></p><hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />');
		});
    });
</script>
<title>Add Team Member</title>
</head>
<body>
    <form id="GalleryShortcode">
        <div id="TeamMemberContent">
			<p>
				<label for="memberName">Name</label>
				<input type="text" id="memberName" name="memberName" />
			</p>
			<p>
				<label for="memberPhoto">Photo</label>
				<input type="text" id="memberPhoto" name="memberPhoto" />
			</p>
			<p>
				<label for="memberInfo">Information : </label>
				<textarea id="memberInfo" name="memberInfo" style="height:100px; width:400px;"></textarea>
			</p>
			<p>
				<label for="memberSize">Size</label>
				<select id="memberSize" name="memberSize">
					<option value="big">Big</option>
					<option value="small">Small</option>
				</select>
            </p>
			
			<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />  
            <p>
				<label for="tmicon[]">Social Button</label>
				<select id="tmicon[]" name="tmicon[]">
					<option value="fa-bitbucket">Bitbucket</option>
					<option value="fa-dribbble">Dribbble</option>
					<option value="fa-facebook">Facebook</option>
					<option value="fa-flickr">Flickr</option>
					<option value="fa-github">Github</option>
					<option value="fa-google-plus">Google+</option>
					<option value="fa-instagram">Instagram</option>
					<option value="fa-linkedin">LinkedIn</option>
					<option value="fa-pinterest">Pinterest</option>
					<option value="fa-skype">Skype</option>
					<option value="fa-stack-exchange">Stackexchange</option>        
					<option value="fa-tumblr">Tumblr</option>
					<option value="fa-twitter">Twitter</option>
					<option value="fa-vk">Vkontakte</option>
					<option value="fa-youtube">Youtube</option>
				</select>
            </p>
            <p>
                <label for="tmlink[]">Link to (without http):</label>
                <input type="text" id="tmlink[]" name="tmlink[]">
            </p>
            <p>
                <hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />  
            </p>
        </div>
        <strong><a style="cursor: pointer;" id="add-social">+ Add Social Button</a></strong>
        <p>
            <a class="add" href="javascript:social.insert(social.e)">Insert into post</a>
        </p>
    </form>
<!--/*************************************/ -->
<?php } elseif($page=='sidenav') { ?>
<script type="text/javascript">
		var sidenav = {
			e: '',
			init: function(e) {
				sidenav.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
			
				var output = "[sidenav]";
				
				jQuery("input[id^=pageLink]").each(function(intIndex, objValue) {
					output +='[sideitem link="'+jQuery(this).val()+'"]';
					var obj = jQuery('input[id^=pageName]').get(intIndex);
					output += obj.value;
					output += "[/sideitem]";
				});
				
				
				output += '[/sidenav]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(sidenav.init, sidenav);

		jQuery(document).ready(function() {
			jQuery("#add-sideitem").click(function() {
				jQuery('#SideItemShortcodeContent').append('<p><label for="pageName[]">Menu Item Name</label><input id="pageName[]" name="pageName[]" type="text" value="" /></p><p><label for="pageLink[]">Menu Item Link</label><input  id="pageLink[]" name="pageLink[]" type="text" value="" /></p>	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />');
			});
		});

	</script>
	<title>Add Side Navigation</title>

</head>
<body>
<form id="GalleryShortcode">
<div id="SideItemShortcodeContent">
	<p>
		<label for="pagName[]">Menu Item Name</label>
		<input id="pageName[]" name="pageName[]" type="text" value="" />
	</p>
	<p>
		<label for="pageLink[]">Menu Item Link</label>
		<input  id="pageLink[]" name="pageLink[]" type="text" value="" />
	</p>
	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />
</div>
	<strong><a style="cursor: pointer;" id="add-sideitem">+ Add Navigation Item</a></strong>
	<p><a class="add" href="javascript:sidenav.insert(sidenav.e)">insert into post</a></p>
</form>
<!--/*************************************/ -->

<?php } ?>

</body>
</html>
