<?php /*** Portfolio Single Posts template. */ 

$get_meta = get_post_custom($post->ID);
$sidebar = weblusive_get_option('sidebar_portfolio');
$sidebarPos =  weblusive_get_option('sidebar_pos');
?>

<section class="section section-diff">
	<div class="container single-work single-work-ajax">
		<div class="project-nav">
			  <a href="#" class="project-close"><i class="fa fa-times-circle-o"></i></a>
		</div>
		<div class="project-title-alt"><h1><?php the_title(); ?></h1></div>
		
		<div class="row">
			<?php if (!empty($sidebar) && $sidebarPos==='left') get_sidebar($sidebar); ?>
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<div class="<?php if (isset($sidebarPos) && empty($sidebar)):?>col-md-12<?php else:?>col-md-8<?php endif?>">
					<?php the_content(); ?>
				</div>
			<?php endwhile; ?>	
			<?php if (!empty($sidebar) && $sidebarPos==='right') get_sidebar($sidebar); ?>
			<div class="clear"></div>
		</div>
	
	</div>
</section>