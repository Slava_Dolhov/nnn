<?php /** The default template for pages. **/ ?>

<?php get_header(); 
	wp_reset_postdata();

	 $args = array(
		'sort_order' => 'ASC',
		'sort_column' => 'menu_order',
		'hierarchical' => 1,
		'post_type' => 'page',
		'post_status' => 'publish'
	);

	$pages = get_pages($args); 
	
	$pageCounter = 1;
	$al_options = get_option('al_general_settings');
	
	$walker = new pages_from_nav;
	$menu_items = wp_nav_menu( array('container' => false,'items_wrap' => '%3$s', 'depth' => 0, 'menu' =>'primary_nav', 'echo'  => false,'walker' => $walker));
	$menu_items = strip_tags ($menu_items);
	
	$pages = explode(',', trim($menu_items));
	
	if (!empty($pages[0])):	
	?>
	<div id="sections">	
		<?php 	
		foreach ( $pages as $pag ) {
			$page = get_page($pag);
			
			$template = get_post_meta( $page->ID, '_wp_page_template', true );
			$get_meta = get_post_custom($page->ID);
			$layout = isset ($custom['_page_layout']) ? $custom['_page_layout'][0] : '1';
			
			$padding = isset( $get_meta['_weblusive_page_padding'][0]) ? ' first-section' : '';
			$notop = isset( $get_meta['_weblusive_page_notoppadding'][0]) ? ' nopaddingtop' : '';
			$nobottom = isset( $get_meta['_weblusive_page_nobottompadding'][0]) ? ' nopaddingbottom' : '';
			
			$overlay = isset( $get_meta['_weblusive_page_overlay'][0]) ? ' overlay' : '';
			$inversed = isset( $get_meta['_weblusive_page_iss'][0]) ? ' section-inverse' : '';
			$fixedbg = isset( $get_meta['_weblusive_page_fixedbg'][0]) ? ' section-bg-attached' : '';
			$diff = isset( $get_meta['_weblusive_page_diff'][0]) ? ' section-diff' : '';
			
			$slug = slugify($page->post_title);
			
			$tagline = isset( $get_meta['_weblusive_tagline'][0]) ? $get_meta["_weblusive_tagline"][0] : '';
			$headline = isset( $get_meta['_weblusive_alttitle'][0]) ? $get_meta["_weblusive_alttitle"][0] : '';
			$title = empty($headline) ?  $page->post_title :  $headline;
			
			$weblusive_sidebar_pos = isset( $get_meta['_weblusive_sidebar_pos'][0]) ? $get_meta["_weblusive_sidebar_pos"][0] : 'full';
			$pageType = isset ($get_meta['_weblusive_page_type']) ? $get_meta['_weblusive_page_type'][0] : '1';
			
			
			if ($pageType == '1')
			{
				switch ($template)
				{
					case 'contact-template.php':
						$get_meta = get_post_custom($post->ID);
						$content = $page->post_content;
						//get_template_part( 'library/includes/page-head' ); 
						$options = array(
							weblusive_get_option('contact_error'), 
							weblusive_get_option('contact_success'),
							weblusive_get_option('contact_subject'), 
							weblusive_get_option('contact_email'), 		
						);
						
						?>
						<div class="section <?php echo $diff.$overlay.$inversed.$fixedbg.$padding.$nobottom.$notop?>">
							<div id="<?php echo $slug?>" class="pagecustom-<?php echo $page->ID?>">
								<?php //get_template_part( 'library/includes/page-head' );  ?>
								<div class="container">
									<div class="section-header">
										<h1><span><?php echo $title?></span></h1>
										<div class="header-desc"><span><?php echo $tagline?></span></div>
									</div>
								</div>
								
								<div class="container clearfix" <?php if (isset($customBg) && !empty($customBg[0])):?>style="background-image:url(<?php echo $customBg[0]?>)"<?php endif?>>
									<div class="row contact-info">
										<div class="col-sm-7 fadeLeft">
											<?php $subj = weblusive_get_option('contact_subject'); ?>
											<h2><?php echo ($subj === '') ? 'Send us a message' : $subj ?></h2>
											<form id="contact-form" method="POST" class="contactform form-horizontal">
												<div id="message-input">
													<input name="contactname" id="contactname" type="text" placeholder="<?php _e( 'name', 'Ante' ); ?>" />
													<?php if(isset($nameError) && $nameError != ''): ?><span class="error"><?php echo $nameError;?></span><?php endif;?>
													<input name="contactemail" id="contactemail" type="text" placeholder="<?php _e( 'e-mail', 'Ante' ); ?>" />
													<?php if(isset($emailError) && $emailError != ''): ?><span class="error"><?php echo $emailError;?></span><?php endif;?>
													<input name="contactsubject" id="contactsubject" type="text" placeholder="<?php _e( 'subject', 'Ante' ); ?>">
												</div>
												<div id="message-textarea">												
													<textarea name="contactmessage" id="contactmessage" placeholder="<?php _e( 'message', 'Ante' ); ?>"></textarea>
													<?php if(isset($messageError) && $messageError != ''): ?><span class="errorarr"><?php echo $messageError;?></span><?php endif;?>
												</div>
												<div id="message-submit">		
													<input type="hidden" name = "options" value="<?php echo implode('|', $options) ?>" />
													<input type="submit" id="submit_contact" class="btn btn-large btn-success" value="<?php _e( 'Send message', 'Ante' ); ?>" name="send" />
													<div id="msg" class="message"></div>
												</div>
											</form>						
										</div>
										 <div class="col-sm-5 fadeRight resp-no-gap">
											<?php echo do_shortcode($content); ?>
										</div>
									</div>					
								</div>
							</div>
						</div>
						<?php $address = weblusive_get_option('contact_address'); if (!empty($address)):?>
							 <section><div id="map" class="fadeIn"></div></section>
						<?php endif?>
					<?php 
					break;
					
					case 'default': 
					case 'page.php':
						$content = $page->post_content;
						?>
						<div class="section <?php echo $diff.$overlay.$inversed.$fixedbg.$padding.$nobottom.$notop?>">
							<div id="<?php echo $slug?>" class="pagecustom-<?php echo $page->ID?>">
								<?php //get_template_part( 'library/includes/page-head' );  ?>
								<div class="container clearfix<?php echo $padding?>">
									<div class="section-header">
										<h1><span><?php echo $title?></span></h1>
										<div class="header-desc"><span><?php echo $tagline?></span></div>
									</div>
									<?php echo do_shortcode($content) ?>
								</div>
							</div>
						</div>
						<?php 
					break;
					
					case 'portfolio-template.php':
						
						$page_template_name = get_post_meta( $page->ID,'_wp_page_template',true); 
						$content = $page->post_content;
						$pageId = $page->ID;
						$itemsize = 'col-md-3';	
						$itemlayout = 'four-col';	
						$colnumber = 4;
						$thumbsize = 'portfolio-4-col';
					
						?>
						<div class="section portfolio-section non-paginated <?php echo $diff.$overlay.$inversed.$fixedbg.$padding.$nobottom.$notop?>" id="portfolio-section">
							<div id="<?php echo $slug?>" class="pagecustom-<?php echo $page->ID?>">
								<div class="clearfix" <?php if (isset($customBg) && !empty($customBg[0])):?>style="background-image:url(<?php echo $customBg[0]?>)"<?php endif?>>
									<div class="section-header">
										<h1><span><?php echo $title?></span></h1>
										<div class="header-desc"><span><?php echo $tagline?></span></div>
									</div>
									<div id="project-container"></div>
								
									<div class="row">
										<div class="col-sm-12 resp-no-gap">
											<div class="portfolio-filter">
												<ul id="filters">
													<li><?php _e('Filter projects:', 'Ante')?></li>
													<li><a href="#" class="active" data-filter="*"><?php _e('All', 'Ante')?></a></li>
													<?php 
														$cats = get_post_meta($pageId, "_page_portfolio_cat", $single = true);                                                 
														$MyWalker = new PortfolioWalker();
														$args = array( 'taxonomy' => 'portfolio_category', 'hide_empty' => '0', 'include' => $cats, 'title_li'=> '', 'walker' => $MyWalker, 'show_count' => '1');
														$categories = wp_list_categories ($args);
													?>
												</ul>            
											</div>
										</div>
									</div>
									<div class="portfolio-items-container <?php echo $itemlayout?>">
										<?php 
										$items_per_page = 777;
										$loop = new WP_Query(array('post_type' => 'portfolio', 'posts_per_page' => $items_per_page)); 
										$cats = get_post_meta($pageId, "_page_portfolio_cat", $single = true);
										if( $cats == '' ): ?>
											<p><?php _e('No categories selected. To fix this, please login to your WP Admin area and set
												the categories you want to show by editing this page and setting one or more categories 
												in the multi checkbox field "Portfolio Categories".', 'Kappe')?>
											</p>
										<?php else: ?>		
											<?php 
												// If the user hasn't set a number of items per page, then use JavaScript filtering
												if( $items_per_page == 777 ) : endif; 
												$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
												//  query the posts in selected terms
												$portfolio_posts_to_query = get_objects_in_term( explode( ",", $cats ), 'portfolio_category');
											 ?>
											 <?php if (!empty($portfolio_posts_to_query)):			
												$wp_query = new WP_Query( array( 'post_type' => 'portfolio', 'orderby' => 'menu_order', 'order' => 'ASC', 'post__in' => $portfolio_posts_to_query, 'paged' => $paged, 'showposts' => $items_per_page) ); 
												
												if ($wp_query->have_posts()):  ?>
												<?php while ($wp_query->have_posts()) : 							
													$wp_query->the_post();
													$custom = get_post_custom($post->ID);
														 
													// Get the portfolio item categories
													$cats = wp_get_object_terms($post->ID, 'portfolio_category');
																		   
																			
													if ($cats):
														$cat_slugs = '';
														foreach( $cats as $cat ) {$cat_slugs .= $cat->slug . " ";}
													endif;
													?>
										
													<?php $link = ''; $thumbnail = get_the_post_thumbnail($post->ID, $thumbsize); ?>
											
											<div class="project-post <?php echo $cat_slugs; ?>">														<?php if (!empty($thumbnail)): ?>
											<?php the_post_thumbnail($thumbsize, array('class' => 'cover')); ?>
											<?php else :?>
											 <img src="http://placehold.it/400x300" alt="<?php _e ('No preview image', 'Kappe') ?>" />
											<?php endif?>
											<div class="hover-box">
											<div class="project-title">
																<h4><?php the_title() ?></h4>                                    
																<span><?php echo $cat_slugs; ?></span>
																<?php if( !empty ( $custom['_portfolio_video'][0] ) ) : $link = $custom['_portfolio_video'][0]; ?>
																	<a href="<?php echo $link ?>" class="zoom video" data-rel="prettyPhoto" title="<?php the_title() ?>"><i class="fa fa-film"></i></a>
																<?php elseif( isset($custom['_portfolio_link'][0]) && $custom['_portfolio_link'][0] != '' ) : ?>
																	<a href="<?php echo $custom['_portfolio_link'][0] ?>"  title="<?php the_title() ?>">
																		<i class="fa fa-external-link"></i>
																	</a>
																<?php elseif(  isset( $custom['_portfolio_no_lightbox'][0] )  &&  $custom['_portfolio_no_lightbox'][0] !='' ) : $link = get_permalink(get_the_ID());  ?>
			<a href="<?php echo $link; ?>"class="inner-link" style="outline:0;border:none;width:100%;height:200%;margin-top:-35%;position:absolute" title="<?php the_title() ?>"></a> <a href="<?php echo $link; ?>" class="inner-link" title="<?php the_title() ?>"><i class="fa fa-arrow-right"></i></a>
																<?php elseif(  isset( $custom['_portfolio_inner_lightbox'][0] )  &&  $custom['_portfolio_inner_lightbox'][0] !='' ) : 
																	$id = get_the_ID(); $link = '#portfolio-item-'.$id;  ?>
																	<a href="<?php echo $link; ?>" class="link-project button btn-icon" data-rel="prettyPhoto" >
																		<i class="fa fa-location-arrow icon-large"></i>
																	</a>
																	<div id="portfolio-item-<?php echo $id?>" class="hide">
																		<?php the_content()?>
																	</div>
																<?php else : ?>
																	<?php  
																		$full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false);
																		$argsThumb = array(
																			'order'          => 'ASC',
																			'posts_per_page'  => 99,
																			'post_type'      => 'attachment',
																			'post_parent'    => $post->ID,
																			'post_mime_type' => 'image',
																			'post_status'    => null,
																			'exclude' => get_post_thumbnail_id()
																		);														
																		$link = $full_image[0];
																	?>
																	<a href="<?php echo $link ?>" class="zoom-image" data-rel="prettyPhoto[ppgal<?php echo $post->ID?>]" title="<?php the_title() ?>"><i class="fa fa-search"></i></a>
																	<?php 
																		$attachments = get_posts($argsThumb);
																		if ($attachments) {
																			foreach ($attachments as $attachment) {
																				echo '<a href="'.wp_get_attachment_url($attachment->ID, 'full', false, false).'" data-rel="prettyPhoto[ppgal'.$post->ID.']" class="gallery-link" title="'.get_the_title($post->ID).'"></a>';
																			}
																		}
																	?>
																<?php endif; ?>  
															 </div>   
														</div>	
													</div>		
												<?php endwhile; ?>
											<?php endif;?>
										<?php endif;?>
									<?php endif?>
									</div>
									<div class="clearfix"></div>
									<?php echo do_shortcode($content) ?>
								</div>
							</div>
						</div>
						<?php 
					break;
					
					case 'blog-template.php':
						$newId = $page->ID;
						?>
						<div class="section blog-section blog-mini <?php echo $diff.$overlay.$inversed.$fixedbg.$padding.$nobottom.$notop?>">
							<div id="<?php echo $slug?>" class="pagecustom-<?php echo $page->ID?>">
								<?php //get_template_part( 'library/includes/page-head' );  ?>
								<div class="container clearfix" <?php if (isset($customBg) && !empty($customBg[0])):?>style="background-image:url(<?php echo $customBg[0]?>)"<?php endif?>>
									<div class="section-header">
										<h1><span><?php echo $title?></span></h1>
										<div class="header-desc"><span><?php echo $tagline?></span></div>
									</div>
									<div class="row">		
										<?php if ($weblusive_sidebar_pos == 'left'): get_sidebar(); ?><?php endif?>
										<div class="<?php if ($weblusive_sidebar_pos == 'full'):?>col-md-12<?php else:?>col-md-9<?php endif?>">
											<?php 
											$postlimit = weblusive_get_option('blog_op_posts');
											if (empty($postlimit)) $postlimit = 3;
											$pagination = weblusive_get_option('blog_op_pagination');
											
											$blog = get_page($newId);
											$temp = $wp_query;
											$wp_query= null;
											$wp_query = new WP_Query();
											
											$wp_query->query('posts_per_page='.$postlimit.'&paged='.$paged);			
											get_template_part( 'loop-mini', 'index' );
										
											wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'Ante' ), 'after' => '</div>' ) ); 
											?>
										</div>
										
										<?php if ($weblusive_sidebar_pos == 'right'): get_sidebar(); ?><?php endif?>
										
										<?php 
											setup_postdata($blog);
											the_content() ;
										
											rewind_posts();
										?>
									</div>
								</div>
							</div>
						</div>
						<?php 
						
					break;
				}
			}
			$pageCounter++;
		}
	?>
	<script type="text/javascript">
		<?php $address = weblusive_get_option('contact_address'); if (!empty($address)):?>
		jQuery(function(){
			jQuery('#map').gmap3({
				action: 'addMarker',
				address: "<?php echo htmlspecialchars($address)?>",
				
				marker:{
					options:{
						icon : new google.maps.MarkerImage('<?php echo get_template_directory_uri()?>/images/mapmarker.png')
					}
				},
				
				map:{
					center: true,
					zoom: 14,
					},
				},
				{action: 'setOptions', args:[{scrollwheel:false, mapTypeControl: true, streetViewControl: true, panControl: false, zoomControl: true, zoomControlOptions: {style: google.maps.ZoomControlStyle.SMALL}, mapTypeId: google.maps.MapTypeId.ROADMAP}]}		
				);	  
			});
		<?php endif?>
		jQuery(document).ready(function(){
			jQuery(".contactform").validate({
				submitHandler: function() {
				
					var postvalues =  jQuery(".contactform").serialize();
					
					jQuery.ajax
					 ({
					   type: "POST",
					  url: "<?php echo get_template_directory_uri()  ?>/contact-form.php",
					   data: postvalues,
					   success: function(response)
					   {
						 jQuery(".message").html(response).fadeIn('slow').delay(4000).fadeOut('slow');
						 jQuery('#contactmessage, #contactemail, #contactname, #contactsubject').val("");
					   }
					 });
					return false;
					
				},
				focusInvalid: true,
				focusCleanup: false,
				//errorLabelContainer: jQuery("#registerErrors"),
				rules: 
				{
					contactname: {required: true},
					contactemail: {required: true, minlength: 6,maxlength: 50, email:true},
					contactmessage: {required: true, minlength: 6}
				},
				
				messages: 
				{
					contactname: {required: "<?php _e( 'Name is required', 'Ante' ); ?>"},
					contactemail: {required: "<?php _e( 'E-mail is required', 'Ante' ); ?>", email: "<?php _e( 'Please provide a valid e-mail', 'Ante' ); ?>", minlength:"<?php _e( 'E-mail address should contain at least 6 characters', 'Ante' ); ?>"},
					contactmessage: {required: "<?php _e( 'Message is required', 'Ante' ); ?>"}
				},
				
				errorPlacement: function(error, element) 
				{
					error.insertBefore(element);
					jQuery('<span class="errorarr"></span>').insertAfter(element);
				},
				invalidHandler: function()
				{
					//jQuery("body").animate({ scrollTop: 0 }, "slow");
				}
				
			});
		});
	</script>
	</div>
	<?php else: ?>
		<?php $get_meta = get_post_custom($post->ID);
		$weblusive_sidebar_pos = isset( $get_meta['_weblusive_sidebar_pos'][0]) ? $get_meta["_weblusive_sidebar_pos"][0] : 'right';
		?>

		<div class="content-dedicated pagecustom-<?php echo $post->ID?>">
			<div class="container">
				<div class="row-fluid">			
					<?php if ($weblusive_sidebar_pos == 'left'): get_sidebar(); ?><?php endif?>
					<div class="<?php if ($weblusive_sidebar_pos == 'full'):?>col-md-12<?php else:?>col-md-8<?php endif?>">
						<?php 
						$temp = $wp_query;
						$wp_query= null;
						$wp_query = new WP_Query();
						$pp = get_option('posts_per_page');
						$wp_query->query('posts_per_page='.$pp.'&paged='.$paged);			
						get_template_part( 'loop', 'index' );
					
						wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'Ante' ), 'after' => '</div>' ) ); 
						?>
					</div>
					
					<?php if ($weblusive_sidebar_pos == 'right'): get_sidebar(); ?><?php endif?>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	<?php endif ?>
<?php	get_footer(); ?>
