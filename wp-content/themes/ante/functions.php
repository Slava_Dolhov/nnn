<?php
/******************* DEFINE NAME/VERSION ********************/

$themename = "Ante";
$themefolder = "ante";

define ('theme_name', $themename );
define ('theme_ver' , 1 );

/************************************************************/


/********************* DEFINE MAIN PATHS ********************/

define('Ante_PLUGINS',  get_template_directory() . '/addons' ); // Shortcut to the /addons/ directory
$adminPath 	=  get_template_directory() . '/library/admin-panel/';
$funcPath 	=  get_template_directory() . '/library/functions/';
$incPath 	=  get_template_directory() . '/library/includes/';

require_once ($funcPath . 'helper-functions.php');
require_once ($incPath . 'the_breadcrumb.php');
require_once ($incPath . 'OAuth.php');
require_once ($incPath . 'twitteroauth.php');
require_once ($incPath . 'portfolio_walker.php');
require_once ($funcPath . 'post-types.php');
require_once ($funcPath . 'widgets.php');
require_once ($funcPath . '/shortcodes/shortcode.php');

include (get_template_directory() . '/library/admin-panel/admin-ui.php');
include (get_template_directory() . '/library/admin-panel/pricing-tables.php');
include (get_template_directory() . '/library/admin-panel/admin-functions.php');
include (get_template_directory() . '/library/admin-panel/post-options.php');
include (get_template_directory() . '/library/admin-panel/custom-slider.php');
//include (get_template_directory() . '/library/admin-panel/notifier/update-notifier.php');

/************************************************************/


/*********** LOAD ALL REQUIRED SCRIPTS AND STYLES ***********/

function Ante_load_styles() {  
	wp_enqueue_style('bootstrap-main',  get_template_directory_uri().'/css/bootstrap.css');
	wp_enqueue_style('main-styles', get_stylesheet_directory_uri().'/style.css');
	wp_enqueue_style('responsive-styles',  get_template_directory_uri().'/css/responsive.css');
	wp_enqueue_style('prettyPhoto-styles',  get_template_directory_uri().'/css/prettyPhoto.css');
	wp_enqueue_style('font-awesome',  get_template_directory_uri().'/addons/fontawesome/css/font-awesome.min.css');
	wp_enqueue_style('player-styles',  get_template_directory_uri().'/css/YTPlayer.css');
	wp_enqueue_style('dynamic-styles',  get_template_directory_uri().'/css/dynamic-styles.php');
}
function Ante_load_scripts()
{
	wp_enqueue_script('jquery');		
	wp_enqueue_script('bootstrap',  get_template_directory_uri(). '/js/bootstrap.min.js', array('jquery'), '3.0.1' );
	wp_enqueue_script('modernizr', get_template_directory_uri() .'/js/modernizr-2.6.2-respond-1.1.0.min.js');
	wp_enqueue_script('skrollr', get_template_directory_uri() .'/js/skrollr.min.js');
	wp_enqueue_script('jquery-cond',  get_template_directory_uri(). '/js/jquery.ba-cond.min.js', array('jquery'), '3.2', true);
	wp_enqueue_script('prettyphoto',  get_template_directory_uri(). '/js/jquery.prettyPhoto.js', array('jquery'), '3.2', true);
	wp_enqueue_script('isotope', get_template_directory_uri() .'/js/jquery.isotope.min.js', array('jquery'), '3.2', true);
	wp_enqueue_script('mobile-menu',  get_template_directory_uri().'/js/jquery.mobilemenu.min.js', array('jquery'), '3.2', true);
	wp_enqueue_script('waypoints',  get_template_directory_uri().'/js/waypoints.min.js', array('jquery'), '3.2', true);
	wp_enqueue_script('YTPlayer',  get_template_directory_uri().'/js/jquery.mb.YTPlayer.js', array('jquery'), '3.2', true);	
	wp_enqueue_script('flex-slider', get_template_directory_uri().'/js/jquery.flexslider.js', array('jquery'), '3.2', true);
	wp_enqueue_script('bx-slider', get_template_directory_uri().'/js/jquery.bxslider.min.js', array('jquery'), '3.2', true);
	wp_enqueue_script('count-script', get_template_directory_uri().'/js/jquery.countTo.js', array('jquery'), '3.2', true);
	wp_enqueue_script('flickr', get_template_directory_uri().'/addons/flickr/jflickrfeed.min.js', array('jquery'), '3.2', true);
	wp_enqueue_script('Validate',  get_template_directory_uri().'/js/jquery.validate.min.js', array('jquery'), '3.2', true);
	wp_enqueue_script('custom-scripts', get_template_directory_uri() .'/js/script.js', array('jquery'), '3.2', true);        
	if (is_page_template('under-construction.php'))
	{
		wp_enqueue_script('Under-construction',  get_template_directory_uri().'/js/jquery.countdown.js', array('jquery'), '3.2', true);
	}
	$address = weblusive_get_option('contact_address');
	if (!empty($address))
	{
		wp_enqueue_script('Google-map-api',  'http://maps.google.com/maps/api/js?sensor=false');
		wp_enqueue_script('Google-map',  get_template_directory_uri().'/js/gmap3.min.js', array('jquery'), '3.2', true);
	}			
}

add_action( 'wp_enqueue_scripts', 'Ante_load_styles' );
add_action( 'wp_enqueue_scripts', 'Ante_load_scripts' );

// Load Google Fonts
function Ante_fonts() {
    $protocol = is_ssl() ? 'https' : 'http';
    wp_enqueue_style( 'Ante-opensans', "$protocol://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" );
	wp_enqueue_style( 'Ante-montserrat', "$protocol://fonts.googleapis.com/css?family=Montserrat:400,700" );
}
add_action( 'wp_enqueue_scripts', 'Ante_fonts' );

/*-----------------------------------------------------------------------------------*/
# Typography Elements Array
/*-----------------------------------------------------------------------------------*/
$custom_typography = array(
	"body, p, li"                                   =>	"typography_general",
	"a.navbar-brand"                                =>	"typography_site_title",
	"#banner .bxslider li, #banner .bxslider p"     =>	"typography_main_nav",
	".page-title h1, .page-title h1 span"			=>  "typography_page_title",
	"#breadcrumbs a"                  				=>  "typography_breadcrumbs",
	"#sidebar .widget h3"                           =>	"typography_widgets_title",
	".footer-block h3"                              =>	"typography_footer_widgets_title",
	"h1, h1 span"                                   =>	"typography_heading1",
	"h2, h2 span"                                   =>	"typography_heading2",
	"h3, h3 span"                                   =>	"typography_heading3",
	"h4, h4 span"                                   =>	"typography_heading4",
	"h5, h5 span"                                   =>	"typography_heading5",
);
	
add_action('wp_enqueue_scripts', 'ante_typography');
function ante_typography(){
	global $custom_typography;
    require_once get_template_directory() . '/library/admin-panel/google-fonts-function.php';
	foreach( $custom_typography as $selector => $value){
		$option = weblusive_get_option( $value );
		
		if( !empty($option['font']))
			weblusive_enqueue_font( $option['font'] ) ;
	}
}

/*******************  THEME SETUP  *************************/
add_action( 'after_setup_theme', 'ante_setup' );
function ante_setup() {

	/************** ADD SUPPORT FOR LOCALIZATION ***************/

	load_theme_textdomain( 'Ante',  get_template_directory() . '/languages' );

		$locale = get_locale();

		$locale_file =  get_template_directory() . "/languages/$locale.php";
		if ( is_readable( $locale_file ) )
			require_once( $locale_file );

	/************************************************************/


	/************* ADD SUPPORT FOR WORDPRESS 3 MENUS ************/

	add_theme_support( 'menus' );

	//Register Navigations
	add_action( 'init', 'my_custom_menus' );
	function my_custom_menus() {
		register_nav_menu('primary_nav',__( 'Header Menu', 'Ante'));
	}
	/************************************************************/

	/******************** POST FORMAT SUPPORT *******************/

	add_theme_support('post-formats', array('gallery','link','image','quote','video', 'audio' ) );

	/**************** ADD SUPPORT FOR POST THUMBS ***************/

	add_theme_support( 'post-thumbnails');

	// Define various thumbnail sizes
	add_image_size('portfolio-4-col', 400, 300, true);
	add_image_size('portfolio-3-col', 356, 267, true);  
	add_image_size('blog-thumb', 59, 59, true);

	/************************************************************/

}
/******* FIX THE PORTFOLIO CATEGORY PAGINATION ISSUE ********/

$option_posts_per_page = get_option( 'posts_per_page' );
add_action( 'init', 'my_modify_posts_per_page', 0);
function my_modify_posts_per_page() {
    add_filter( 'option_posts_per_page', 'my_option_posts_per_page' );
}
function my_option_posts_per_page( $value ) {
    global $option_posts_per_page;
    if ( is_tax( 'portfolio_category') ) {
		$pageId = get_page_ID_by_page_template('portfolio-template3.php');
		if ($pageId)
		{
			$custom =  get_post_custom($pageId);
			$items_per_page = isset ($custom['_page_portfolio_num_items_page']) ? $custom['_page_portfolio_num_items_page'][0] : '777';
			return $items_per_page;
		}
		else
		{
			return 4;
		}
    } else {
        return $option_posts_per_page;
    }
}

/************************************************************/


function weblusive_get_option( $name ) {
	$get_options = get_option( 'weblusive_options' );
	
	if( !empty( $get_options[$name] ))
		return $get_options[$name];
		
	return false ;
}

//Docs Url
$docs_url = "http://weblusive-themes.com/documentation/".$themefolder;

// Redirect To Theme Options Page on Activation
if (is_admin() && isset($_GET['activated'])){
	wp_redirect(admin_url('admin.php?page=panel'));
}

/************************************************************/


/****************** REGISTER SIDEBARS ***********************/

if ( function_exists('register_sidebar') )
{
	$footer_widget_count = weblusive_get_option('footer_widgets');
	if($footer_widget_count !== 'none')
	{
		$columns = 'col-md-3';
		switch($footer_widget_count)
		{
			case '4':
			$columns = 'col-md-3';
			break;
			case '3':
			$columns = 'col-md-4';
			break;
			case '2':
			$columns = 'col-md-6';
			break;
		}
		for($i = 1; $i<= $footer_widget_count; $i++)
		{
			if ( function_exists('register_sidebar') )
			register_sidebar(array(
				'name' => 'Footer Widget '.$i,
				'id'	=> 'footer-sidebar-'.$i,
				'before_widget' => '<div class="'.$columns.' footer-widget footer-block">',
				'after_widget' => '</div>',
				'before_title' => '<h3>',
				'after_title' => '</h3>',
			));
		}
	}
}

add_filter('widget_text', 'do_shortcode');
add_filter('the_excerpt', 'do_shortcode');

add_action( 'widgets_init', 'ante_widgets_init' );
function ante_widgets_init() {
	$before_widget =  '<div id="%1$s" class="widget %2$s">';
	$after_widget  =  '</div></div>';
	$before_title  =  '<h5 class="small-header"><span>';
	$after_title   =  '</span></h5>
						<div class="widget-container">';
					
	register_sidebar( array(
		'name' =>  __( 'Primary Widget Area', 'Ante' ),
		'id' => 'primary-widget-area',
		'description' => __( 'The Primary widget area', 'Ante' ),
		'before_widget' => $before_widget , 'after_widget' => $after_widget , 'before_title' => $before_title , 'after_title' => $after_title ,
	) );

	//Custom Sidebars
	$sidebars = weblusive_get_option( 'sidebars' ) ;
	if($sidebars){
		foreach ($sidebars as $sidebar) {
			register_sidebar( array(
				'name' => $sidebar,
				'id' => sanitize_title($sidebar),
				'before_widget' => $before_widget , 'after_widget' => $after_widget , 'before_title' => $before_title , 'after_title' => $after_title ,
			) );
		}
	}
}

/************************************************************/


/****************** CUSTOM LOGIN LOGO ***********************/

function ante_login_logo(){
	if( weblusive_get_option('dashboard_logo') )
    echo '<style  type="text/css"> h1 a {  background-image:url('.weblusive_get_option('dashboard_logo').')  !important; } </style>';  
}  
add_action('login_head',  'ante_login_logo'); 

/************************************************************/


/******************** CUSTOM GRAVATAR ***********************/

function ante_custom_gravatar ($avatar) {
	$weblusive_gravatar = weblusive_get_option( 'gravatar' );
	if($weblusive_gravatar){
		$custom_avatar = weblusive_get_option( 'gravatar' );
		$avatar[$custom_avatar] = "Custom Gravatar";
	}
	return $avatar;
}
add_filter( 'avatar_defaults', 'ante_custom_gravatar' ); 

/************************************************************/


/********************* CUSTOM TAG CLOUDS ********************/

function Ante_custom_tag_cloud_widget($args) {
	$args['number'] = 0; //adding a 0 will display all tags
	$args['largest'] = 18; //largest tag
	$args['smallest'] = 12; //smallest tag
	$args['unit'] = 'px'; //tag font unit
	$args['format'] = 'list'; //ul with a class of wp-tag-cloud
	return $args;
}
add_filter( 'widget_tag_cloud_args', 'Ante_custom_tag_cloud_widget' );

/************************************************************/


/****************** ENABLE SESSIONS *************************/

function ante_admin_init() {
	if (!session_id())
	session_start();
}

add_action('init', 'ante_admin_init');

/************************************************************/


/******************* FILL EMPTY WIDGET TITLE ****************/

function Ante_fill_widget_title($title){
	if( empty( $title ) )
		return ' ';
	else return $title;
}
add_filter('widget_title', 'Ante_fill_widget_title');

add_theme_support( 'automatic-feed-links' );
if ( ! isset( $content_width ) ) $content_width = 960;

/************************************************************/


function slugify($text)
{ 
  // replace non letter or digits by -
  $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

  // trim
  $text = trim($text, '-');

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // lowercase
  $text = strtolower($text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  if (empty($text))
  {
    return 'n-a';
  }

  return $text;
}

add_filter('wp_get_attachment_link', 'rc_add_rel_attribute');
function rc_add_rel_attribute($link) {
	global $post;
	return str_replace('<a href', '<a data-rel="prettyPhoto[pp_gal]" href', $link);
}

/************************************************************/
?>