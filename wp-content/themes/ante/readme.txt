Refer to documentation folder on download package for theme documentation.

Version 1.1 List of changes:
----------------------------
Added full compatibility with Wordpress version 4.
Fixed standalone page featured image issues
Other fixes and improvements