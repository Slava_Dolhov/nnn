<?php /* Template Name: Contact Form */ 

get_header('standalone');

$get_meta = get_post_custom($post->ID);
$weblusive_sidebar_pos = isset( $get_meta['_weblusive_sidebar_pos'][0]) ? $get_meta["_weblusive_sidebar_pos"][0] : 'full';
$tagline = isset( $get_meta['_weblusive_tagline'][0]) ? $get_meta["_weblusive_tagline"][0] : '';
$headline = isset( $get_meta['_weblusive_alttitle'][0]) ? $get_meta["_weblusive_alttitle"][0] : '';
get_template_part( 'library/includes/page-head' ); 
$options = array(
		weblusive_get_option('contact_error'), 
		weblusive_get_option('contact_success'),
		weblusive_get_option('contact_subject'), 
		weblusive_get_option('contact_email'), 		
	);
$address = weblusive_get_option('contact_address'); 
if (!empty($address)):?>
<script type="text/javascript">   
jQuery(function(){
	jQuery('#map').gmap3({
		action: 'addMarker',
		address: "<?php echo htmlspecialchars($address)?>",
		
		marker:{
			options:{
				icon : new google.maps.MarkerImage('<?php echo get_template_directory_uri()?>/images/mapmarker.png')
			}
		},
		
		map:{
			center: true,
			zoom: 14,
			},
		},
		{action: 'setOptions', args:[{scrollwheel:true}]}		
		);	  
	});
</script> 
<?php endif?> 
<div class="content-dedicated pagecustom-<?php echo $post->ID?>">
	<div id="page-header">
		<div class="container">
			<div class="page-title section-header sh-left">
				<?php get_template_part( 'library/includes/page-head' ); ?>
				<?php if (!weblusive_get_option('hide_titles')):?>
					<h1><span><?php echo (isset($headline) && !empty($headline)) ? $headline : get_the_title(); ?></span></h1>
					<?php echo (isset($tagline) && !empty($tagline)) ? '<div class="header-desc"><span>'.$tagline.'</span></div>' : ''; ?>
				<?php endif?>
				<?php if (!weblusive_get_option('hide_breadcrumbs')):?>
					<nav id="breadcrumbs">
						<?php if(class_exists('the_breadcrumb')){ $albc = new the_breadcrumb; } ?>
					</nav>
				<?php endif?>
			</div>
		</div>
	</div>
	<!-- Set the Google Map if available -->
	<?php if (!empty($address)):?>
		<div class="map map2 cdmap" id="map"></div>
	<?php endif?>
	<section class="page-content">
		<section class="section">
			<div class="container">
				<div class="row-fluid">
					<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
						<div class="<?php if ($weblusive_sidebar_pos == 'full'):?>col-md-12<?php else:?>col-md-8<?php endif?>">
							<div class="row-fluid">
								<div class="col-md-6">
									<?php the_content(); ?>
								</div>
								<div class="col-md-6 contact-full">
									<?php $subj = weblusive_get_option('contact_subject'); ?>
									<h2><?php echo ($subj === '') ? 'Send us a message' : $subj ?></h2>
									<form id="contact-form" method="POST" class="contactform form-horizontal">
										<div id="message-input">
											<input name="contactname" id="contactname" type="text" placeholder="<?php _e( 'name', 'Ante' ); ?>" />
											<?php if(isset($nameError) && $nameError != ''): ?><span class="error"><?php echo $nameError;?></span><?php endif;?>
											<input name="contactemail" id="contactemail" type="text" placeholder="<?php _e( 'e-mail', 'Ante' ); ?>" />
											<?php if(isset($emailError) && $emailError != ''): ?><span class="error"><?php echo $emailError;?></span><?php endif;?>
											<input name="contactsubject" id="contactsubject" type="text" placeholder="<?php _e( 'subject', 'Ante' ); ?>">
										</div>
										<div id="message-textarea">												
											<textarea name="contactmessage" id="contactmessage" placeholder="<?php _e( 'message', 'Ante' ); ?>"></textarea>
											<?php if(isset($messageError) && $messageError != ''): ?><span class="errorarr"><?php echo $messageError;?></span><?php endif;?>
										</div>
										<div id="message-submit">		
											<input type="hidden" name = "options" value="<?php echo implode('|', $options) ?>" />
											<input type="submit" id="submit_contact" class="btn btn-large btn-success" value="<?php _e( 'Send message', 'Ante' ); ?>" name="send" />
											<div id="msg" class="message"></div>
										</div>
									</form>			
								</div>
							</div>
						</div>
					<?php endwhile; ?>	
					<?php get_sidebar(); ?>
					<div class="clear"></div>
				</div>
			</div>
		</section>
	</section>
</div>

<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery(".contactform").validate({
			submitHandler: function() {
			
				var postvalues =  jQuery(".contactform").serialize();
				
				jQuery.ajax
				 ({
				   type: "POST",
				  url: "<?php echo get_template_directory_uri()  ?>/contact-form.php",
				   data: postvalues,
				   success: function(response)
				   {
					 jQuery(".message").html(response).fadeIn('slow').delay(4000).fadeOut('slow');
					 jQuery('#contactmessage, #contactemail, #contactname, #contactsubject').val("");
				   }
				 });
				return false;
				
			},
			focusInvalid: true,
			focusCleanup: false,
			//errorLabelContainer: jQuery("#registerErrors"),
			rules: 
			{
				contactname: {required: true},
				contactemail: {required: true, minlength: 6,maxlength: 50, email:true},
				contactmessage: {required: true, minlength: 6}
			},
			
			messages: 
			{
				contactname: {required: "<?php _e( 'Name is required', 'Ante' ); ?>"},
				contactemail: {required: "<?php _e( 'E-mail is required', 'Ante' ); ?>", email: "<?php _e( 'Please provide a valid e-mail', 'Ante' ); ?>", minlength:"<?php _e( 'E-mail address should contain at least 6 characters', 'Ante' ); ?>"},
				contactmessage: {required: "<?php _e( 'Message is required', 'Ante' ); ?>"}
			},
			
			errorPlacement: function(error, element) 
			{
				error.insertBefore(element);
				jQuery('<span class="errorarr"></span>').insertAfter(element);
			},
			invalidHandler: function()
			{
				//jQuery("body").animate({ scrollTop: 0 }, "slow");
			}
			
		});
	});
</script>


<?php get_footer(); ?>