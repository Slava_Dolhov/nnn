<?php
 
// Do not delete these lines
if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
die ('Please do not load this page directly. Thanks!');
 
if ( post_password_required() ) : ?>
	<p class="nocomment"><?php _e( 'This post is password protected. Enter the password to view any comments.', 'Ante' ); ?></p>

<?php
		/* Stop the rest of comments.php from being processed,
		 * but don't kill the script entirely -- we still have
		 * to fully load the template.
		 */
		return;
	endif;
?>
<!-- You can start editing here. -->
<h5 class="small-header">
	<span>
	<?php printf( _n( 'Comments: %1$s', 'Comments: %1$s', get_comments_number(), 'Ante' ),
		number_format_i18n( get_comments_number() ), '<em>' . get_the_title() . '</em>' );
	?>
	</span>
</h5>
<div class="comments-container">
	<?php if ( have_comments() ) : ?>
		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
			<div class="navigation">
				<div class="nav-previous"><?php previous_comments_link( __( '<span class="meta-nav">&larr;</span> Older Comments', 'Ante' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( __( 'Newer Comments <span class="meta-nav">&rarr;</span>', 'Ante' ) ); ?></div>
			</div> <!-- .navigation -->
		<?php endif; ?>
	 
		<ol class="commentlist">
			<?php wp_list_comments('callback=Ante_comment'); ?>
		</ol>
	 
		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
			<div class="navigation">
				<div class="nav-previous"><?php previous_comments_link( __( '<span class="meta-nav">&larr;</span> Older Comments', 'Ante' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( __( 'Newer Comments <span class="meta-nav">&rarr;</span>', 'Ante' ) ); ?></div>
			</div><!-- .navigation -->
		<?php endif; // check for comment navigation ?>
	<?php else : // this is displayed if there are no comments so far ?>
		<?php if ('open' == $post->comment_status) : ?>
			<!-- If comments are open, but there are no comments. -->
		 
		<?php else : // comments are closed ?>
			<!-- If comments are closed. -->
			<p class="nocomments"><?php _e( 'Comments are closed.', 'Ante' ); ?></p>
		
		<?php endif; ?>
	<?php endif; ?>

	<?php if ('open' == $post->comment_status) : ?>

	<div id="respond" class="leave-comment">
		 <div class="title"><h5><?php comment_form_title( 'Leave a Comment', 'Leave a Reply to %s' ); ?></h5></div>
		<div class="cancel-comment-reply bottom10">
			<?php cancel_comment_reply_link(); ?>
		</div>
	 
		<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
			<p class="bottom20">
				You must be <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>">logged in</a> 
				to post a comment.
			</p>
		<?php else : ?> 
			<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="respond-form" class="form-horizontal">
				<div class="contact_form">     
					<div class="row-fluid">
						<?php if ( $user_ID ) : ?>
							<p class="com_logined_text">
								<?php _e('Logged in as', 'Ante')?> 
								<a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. 
								<a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Log out of this account"><?php _e('Log out', 'Ante')?> &raquo;</a>
							</p>
						<?php else : ?>
							 <div id="message-input">
								<input type="text" name="author" id="author" placeholder="<?php _e('Name', 'Ante')?>" value="<?php echo $comment_author; ?>" size="22" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> />
								<input type="text" name="email" id="email" placeholder="<?php _e('Email', 'Ante')?>" value="<?php echo $comment_author_email; ?>" size="22" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> />
								<input type="text" name="website" id="website" placeholder="<?php _e('Website', 'Ante')?>" value="<?php echo isset($comment_author_website) ? $comment_author_website : ''; ?>" size="22" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> />
							</div>
						<?php endif; ?>
						
						<div id="message-textarea">
							<textarea name="comment" placeholder="<?php _e('Message...', 'Ante')?>" cols="39" rows="6" class="requiredField label-better" id="comment"  tabindex="4"></textarea>
						</div>
						<div id="message-input">						
							 <input name="Send" type="submit" id="Send" tabindex="5" value="<?php _e('Send message', 'Ante')?>" class="btn-submit btn">
						</div>
						<?php comment_id_fields(); ?>
						<?php do_action('comment_form', $post->ID); ?>
					</div>
				</div>
			</form>
			<div class="clear"></div>
		</div>
		<?php endif; // If registration required and not logged in ?>
	<?php endif;  ?>
</div>