<?php get_header('standalone');

$sidebar = weblusive_get_option('sidebar_archive');
$sidebarPos =  weblusive_get_option('sidebar_pos');
?>

<div class="content-dedicated">
	<div id="page-header">
		<div class="container">
			<div class="page-title section-header sh-left">
				<?php get_template_part( 'library/includes/page-head' ); ?>
				<?php if (!weblusive_get_option('hide_titles')):?>
					<h1>
						<span>
							<?php if ( is_day() ) : ?>
								<?php printf( __( 'Daily Archives: %s', 'Ante' ), get_the_date() ); ?>
							<?php elseif ( is_month() ) : ?>
								<?php printf( __( 'Monthly Archives: %s', 'Ante' ), get_the_date('F Y') ); ?>
							<?php elseif ( is_year() ) : ?>
								<?php printf( __( 'Yearly Archives: %s', 'Ante' ), get_the_date('Y') ); ?>
							<?php elseif ( is_category() ) : ?>
								<?php single_cat_title();?>
							<?php else : ?>
								<?php _e( 'Blog Archives', 'Ante' ); ?>
							<?php endif; ?>
						</span>
					</h1>
				<?php endif?>
				<?php if (!weblusive_get_option('hide_breadcrumbs')):?>
					<nav id="breadcrumbs">
						<?php if(class_exists('the_breadcrumb')){ $albc = new the_breadcrumb; } ?>
					</nav>
				<?php endif?>
			</div>
		</div>
	</div>
	<section class="page-content">
		<section class="section">
			<div class="container">
				<div class="row-fluid">	
					<?php if (!empty($sidebar) && $sidebarPos==='left') get_sidebar($sidebar); ?>
					<div class="<?php echo empty($sidebar) ? 'col-md-12' : 'col-md-9';?>">
						<?php
							if ( have_posts() ) the_post();
							rewind_posts();       
							get_template_part( 'loop', 'archive' );
						?>
					</div>
					<?php if (!empty($sidebar) && $sidebarPos==='right') get_sidebar($sidebar); ?>
				</div>
			</div>
		</section>
	</section>
</div>

<?php get_footer(); ?>