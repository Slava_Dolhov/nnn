<?php /* Template Name: Regular page */ 

get_header('standalone');?>

<?php
	$get_meta = get_post_custom($post->ID);
	$weblusive_sidebar_pos = isset( $get_meta['_weblusive_sidebar_pos'][0]) ? $get_meta["_weblusive_sidebar_pos"][0] : 'full';
	$tagline = isset( $get_meta['_weblusive_tagline'][0]) ? $get_meta["_weblusive_tagline"][0] : '';
	$headline = isset( $get_meta['_weblusive_alttitle'][0]) ? $get_meta["_weblusive_alttitle"][0] : '';
	
	$image_id = get_post_thumbnail_id($post->ID);  
	$image_url = wp_get_attachment_image_src($image_id,'large');  
	$image_url = $image_url[0];
	
	if ($image_url):?>
		<style type="text/css">
			.pagecustom-<?php echo $post->ID?> #page-header{
				background: url(<?php echo $image_url; ?>) no-repeat center center fixed !important; 
				-webkit-background-size: cover !important;
				-moz-background-size: cover !important;
				-o-background-size: cover !important;
				background-size: cover !important;
				filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo $image_url; ?>', sizingMethod='scale');
				-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo $image_url; ?>', sizingMethod='scale')";
			}
		</style>
	<?php endif?>
<div class="content-dedicated pagecustom-<?php echo $post->ID?>">
	<div id="page-header">
		<div class="container">
			<div class="page-title section-header sh-left">
				<?php get_template_part( 'library/includes/page-head' ); ?>
				<?php if (!weblusive_get_option('hide_titles')):?>
					<h1><span><?php echo (isset($headline) && !empty($headline)) ? $headline : get_the_title(); ?></span></h1>
					<?php echo (isset($tagline) && !empty($tagline)) ? '<div class="header-desc"><span>'.$tagline.'</span></div>' : ''; ?>
				<?php endif?>
				<?php if (!weblusive_get_option('hide_breadcrumbs')):?>
					<nav id="breadcrumbs">
						<?php if(class_exists('the_breadcrumb')){ $albc = new the_breadcrumb; } ?>
					</nav>
				<?php endif?>
			</div>
		</div>
	</div>
	<section class="page-content">
		<section class="section">
			<div class="container">
				<div class="row-fluid">
					<?php if ($weblusive_sidebar_pos == 'left') get_sidebar(); ?>
					<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
						<div class="<?php if ($weblusive_sidebar_pos == 'full'):?>col-md-12<?php else:?>col-md-8<?php endif?>">
							<?php the_content(); ?>
							<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'Ante' ), 'after' => '</div>' ) ); ?>
						</div>
					<?php endwhile; ?>	
					<?php if ($weblusive_sidebar_pos == 'right') get_sidebar(); ?>
					<div class="clear"></div>
				</div>
			</div>
		</section>
	</section>
</div>

<?php get_footer();?>