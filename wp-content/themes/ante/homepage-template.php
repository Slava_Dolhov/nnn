<?php /* Template Name: Homepage */ 
get_header();?>

<?php
	$get_meta = get_post_custom($post->ID);
	$weblusive_sidebar_pos = isset( $get_meta['_weblusive_sidebar_pos'][0]) ? $get_meta["_weblusive_sidebar_pos"][0] : 'full';
	$tagline = isset( $get_meta['_weblusive_tagline'][0]) ? $get_meta["_weblusive_tagline"][0] : '';
	$headline = isset( $get_meta['_weblusive_alttitle'][0]) ? $get_meta["_weblusive_alttitle"][0] : '';
	get_template_part( 'library/includes/page-head' ); 
?>
<div class="content-dedicated pagecustom-<?php echo $post->ID?>">
	<?php get_template_part( 'library/includes/page-head' ); ?>
	<div class="container">
		<div class="row-fluid">
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<div class="<?php if ($weblusive_sidebar_pos == 'full'):?>col-md-12<?php else:?>col-md-8<?php endif?>">
					<?php the_content(); ?>
				</div>
			<?php endwhile; ?>	
			<?php get_sidebar(); ?>
			<div class="clear"></div>
		</div>
	</div>
</div>

<?php get_footer();?>