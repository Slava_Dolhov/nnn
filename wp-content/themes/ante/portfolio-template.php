<?php /* Template Name: Portfolio */
get_header('standalone');

$pageId = $post->ID;
$_SESSION['Ante_page_id'] = $pageId;

$get_meta = get_post_custom($post->ID);
$weblusive_sidebar_pos = $get_meta["_weblusive_sidebar_pos"][0];
$portfolio_type = get_post_meta($post->ID, "_portfolio_type", $single = false);
$paginationEnabled = (isset($portfolio_type) && !(empty ($portfolio_type))) ? $portfolio_type[0] : 0;
$specialclass =  $paginationEnabled ? 'portfolio-paginated' : 'non-paginated';
$itemsize = 'col-md-3';	
$itemlayout = 'four-col';	
$colnumber = 4;
$thumbsize = 'portfolio-4-col';
// Check which layout was selected
$items_per_page = 777;
if( get_post_meta($post->ID, "_page_portfolio_num_items_page", $single = true) != '' && $paginationEnabled ) 
{ 
	$per_page = get_post_meta($post->ID, "_page_portfolio_num_items_page", $single = false);
	$items_per_page = $per_page[0];
} 

$loop = new WP_Query(array('post_type' => 'portfolio', 'posts_per_page' => $items_per_page)); 
?>
<div class="content-dedicated pagecustom-<?php echo $post->ID?>">
	<div id="page-header">
		<div class="container">
			<div class="page-title section-header sh-left">
				<?php get_template_part( 'library/includes/page-head' ); ?>
				<?php if (!weblusive_get_option('hide_titles')):?>
					<h1><span><?php echo (isset($headline) && !empty($headline)) ? $headline : get_the_title(); ?></span></h1>
					<?php echo (isset($tagline) && !empty($tagline)) ? '<div class="header-desc"><span>'.$tagline.'</span></div>' : ''; ?>
				<?php endif?>
				<?php if (!weblusive_get_option('hide_breadcrumbs')):?>
					<nav id="breadcrumbs">
						<?php if(class_exists('the_breadcrumb')){ $albc = new the_breadcrumb; } ?>
					</nav>
				<?php endif?>
			</div>
		</div>
	</div>
	<section class="page-content">
		<section class="section">
			<div class="container main-content <?php echo $specialclass ?>">
				<div class="row">
					<div class="col-sm-12 resp-no-gap">
						<?php echo do_shortcode(getPageContent($pageId)); ?>
						<div class="portfolio-filter">
							<ul class="filter" id="filters">
								<?php if ($paginationEnabled):?>
									<li><a href="<?php echo get_page_link($pageId) ?>" class="active"><?php _e('Show All', 'Ante')?></a></li>
									<?php 
										$cats = get_post_meta($post->ID, "_page_portfolio_cat", $single = true);
										$MyWalker = new PortfolioWalker2();
										$args = array( 'taxonomy' => 'portfolio_category', 'hide_empty' => '0', 'include' => $cats, 'title_li'=> '', 'walker' => $MyWalker, 'show_count' => '1');
										$categories = wp_list_categories ($args);
									?>
								<?php else: ?>
									<li><a href="#" class="active" data-filter="*"><?php _e('Show All', 'Ante')?></a></li>
									<?php 
										$cats = get_post_meta($post->ID, "_page_portfolio_cat", $single = true);                                                 
										$MyWalker = new PortfolioWalker();
										$args = array( 'taxonomy' => 'portfolio_category', 'hide_empty' => '0', 'include' => $cats, 'title_li'=> '', 'walker' => $MyWalker, 'show_count' => '1');
										$categories = wp_list_categories ($args);
									?>
								<?php endif ?>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="portfolio-items-container <?php echo $itemlayout?>">
							<?php 
							
							$loop = new WP_Query(array('post_type' => 'portfolio', 'posts_per_page' => $items_per_page)); 
							$cats = get_post_meta($pageId, "_page_portfolio_cat", $single = true);
							if( $cats == '' ): ?>
								<p><?php _e('No categories selected. To fix this, please login to your WP Admin area and set
									the categories you want to show by editing this page and setting one or more categories 
									in the multi checkbox field "Portfolio Categories".', 'Ante')?>
								</p>
							<?php else: ?>		
								<?php 
									// If the user hasn't set a number of items per page, then use JavaScript filtering
									if( $items_per_page == 777 ) : endif; 
									$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
									//  query the posts in selected terms
									$portfolio_posts_to_query = get_objects_in_term( explode( ",", $cats ), 'portfolio_category');
								 ?>
								 <?php if (!empty($portfolio_posts_to_query)):			
									$wp_query = new WP_Query( array( 'post_type' => 'portfolio', 'orderby' => 'menu_order', 'order' => 'ASC', 'post__in' => $portfolio_posts_to_query, 'paged' => $paged, 'showposts' => $items_per_page) ); 
									
									if ($wp_query->have_posts()):  ?>
									<?php while ($wp_query->have_posts()) : 							
										$wp_query->the_post();
										$custom = get_post_custom($post->ID);
											 
										// Get the portfolio item categories
										$cats = wp_get_object_terms($post->ID, 'portfolio_category');
															   
																
										if ($cats):
											$cat_slugs = '';
											foreach( $cats as $cat ) {$cat_slugs .= $cat->slug . " ";}
										endif;
										?>
							
										<?php $link = ''; $thumbnail = get_the_post_thumbnail($post->ID, $thumbsize); ?>
										
										<div class="project-post <?php echo $cat_slugs; ?>">
											<?php if (!empty($thumbnail)): ?>
												<?php the_post_thumbnail($thumbsize, array('class' => 'cover')); ?>
											<?php else :?>
												 <img src="http://placehold.it/405x311" alt="<?php _e ('No preview image', 'Ante') ?>" />
											<?php endif?>
											<div class="hover-box">
												<div class="project-title">
													<h4><?php the_title() ?></h4>                                    
													<span><?php echo $cat_slugs; ?></span>
													<?php if( !empty ( $custom['_portfolio_video'][0] ) ) : $link = $custom['_portfolio_video'][0]; ?>
														<a href="<?php echo $link ?>" class="zoom video" data-rel="prettyPhoto" title="<?php the_title() ?>"><i class="fa fa-film"></i></a>
													<?php elseif( isset($custom['_portfolio_link'][0]) && $custom['_portfolio_link'][0] != '' ) : ?>
														<a href="<?php echo $custom['_portfolio_link'][0] ?>"  title="<?php the_title() ?>">
															<i class="fa fa-external-link"></i>
														</a>
													<?php elseif(  isset( $custom['_portfolio_no_lightbox'][0] )  &&  $custom['_portfolio_no_lightbox'][0] !='' ) : $link = get_permalink(get_the_ID());  ?>
														<a href="<?php echo $link; ?>"  title="<?php the_title() ?>"><i class="fa fa-arrow-right"></i></a>
													<?php elseif(  isset( $custom['_portfolio_inner_lightbox'][0] )  &&  $custom['_portfolio_inner_lightbox'][0] !='' ) : 
														$id = get_the_ID(); $link = '#portfolio-item-'.$id;  ?>
														<a href="<?php echo $link; ?>" class="link-project button btn-icon" data-rel="prettyPhoto" >
															<i class="fa fa-location-arrow icon-large"></i>
														</a>
														<div id="portfolio-item-<?php echo $id?>" class="hide">
															<?php the_content()?>
														</div>
													<?php else : ?>
														<?php  
															$full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false);
															$argsThumb = array(
																'order'          => 'ASC',
																'posts_per_page'  => 99,
																'post_type'      => 'attachment',
																'post_parent'    => $post->ID,
																'post_mime_type' => 'image',
																'post_status'    => null,
																'exclude' => get_post_thumbnail_id()
															);														
															$link = $full_image[0];
														?>
														<a href="<?php echo $link ?>" class="zoom-image" data-rel="prettyPhoto[ppgal<?php echo $post->ID?>]" title="<?php the_title() ?>"><i class="fa fa-search"></i></a>
														<?php 
															$attachments = get_posts($argsThumb);
															if ($attachments) {
																foreach ($attachments as $attachment) {
																	echo '<a href="'.wp_get_attachment_url($attachment->ID, 'full', false, false).'" data-rel="prettyPhoto[ppgal'.$post->ID.']" class="gallery-link" title="'.get_the_title($post->ID).'"></a>';
																}
															}
														?>
													<?php endif; ?>  
												 </div>   
											</div>	
										</div>		
									<?php endwhile; ?>
								<?php endif;?>
							<?php endif;?>
						<?php endif?>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<?php if ($paginationEnabled ):?>
					<?php if ( $wp_query->max_num_pages > 1 ): ?>
						<div class="pagination-list blog-pagination">
							<?php include(Ante_PLUGINS . '/wp-pagenavi.php' ); wp_pagenavi(); ?> 
							<div class="clear"></div>
						</div>
					<?php endif?>
				<?php endif?>		
				<div class="clearfix"></div>
			</div>
		</section>
	</section>
</div>
<?php get_footer();?>