</div> <!--main content end -->

<footer>
	<div class="container footer-top">
		<?php $footer_widget_count = weblusive_get_option('footer_widgets');
			if($footer_widget_count !== 'none'):?>
				<section class="footer-widget">
					<div class="row-fluid">	
						<?php	
							for($i = 1; $i<= $footer_widget_count; $i++){
								if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Widget ".$i) ):endif;
							}			
						?>
					</div>	
				</section>	
		<?php endif; ?>	
	</div>	
	<div class="footer-bottom">
		<div class="back-to-top"><a href="#" class="btn btn-primary" data-scroll-goto="0"><?php _e('Back to top', 'Ante') ?></a></div>
				
		<?php 	
			$logo = weblusive_get_option('homepage_logo'); 
			$enablelogo = weblusive_get_option('footer_homepage_logo'); 
		?>
		<?php if (!empty($logo) && $enablelogo):?>
			  <a href="<?php echo site_url() ?>"><img src="<?php echo $logo ?>" class="footer-logo" alt="<?php echo get_bloginfo('name')?>"  /></a>
		<?php endif?>
		
		<span class="copyrights"><?php echo  htmlspecialchars_decode(do_shortcode(weblusive_get_option('footer_copyright')))?></span>
		<?php echo do_shortcode(weblusive_get_option('footer_social')); ?>
	</div>
</footer>
<?php if(weblusive_get_option('footer_code')) echo  htmlspecialchars_decode(weblusive_get_option('footer_code')); ?>
<?php wp_footer()?>
</body>
</html>