/*jshint jquery:true */
/*global $:true */

(function(d){function h(b){return"object"==typeof b?b:{top:b,left:b}}var n=d.scrollTo=function(b,c,a){return d(window).scrollTo(b,c,a)};n.defaults={axis:"xy",duration:1.3<=parseFloat(d.fn.jquery)?0:1,limit:!0};n.window=function(b){return d(window)._scrollable()};d.fn._scrollable=function(){return this.map(function(){if(this.nodeName&&-1==d.inArray(this.nodeName.toLowerCase(),["iframe","#document","html","body"]))return this;var b=(this.contentWindow||this).document||this.ownerDocument||this;return/webkit/i.test(navigator.userAgent)|| "BackCompat"==b.compatMode?b.body:b.documentElement})};d.fn.scrollTo=function(b,c,a){"object"==typeof c&&(a=c,c=0);"function"==typeof a&&(a={onAfter:a});"max"==b&&(b=9E9);a=d.extend({},n.defaults,a);c=c||a.duration;a.queue=a.queue&&1<a.axis.length;a.queue&&(c/=2);a.offset=h(a.offset);a.over=h(a.over);return this._scrollable().each(function(){function q(b){k.animate(e,c,a.easing,b&&function(){b.call(this,g,a)})}if(null!=b){var l=this,k=d(l),g=b,p,e={},s=k.is("html,body");switch(typeof g){case "number":case "string":if(/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(g)){g= h(g);break}g=d(g,this);if(!g.length)return;case "object":if(g.is||g.style)p=(g=d(g)).offset()}d.each(a.axis.split(""),function(b,d){var c="x"==d?"Left":"Top",m=c.toLowerCase(),f="scroll"+c,h=l[f],r=n.max(l,d);p?(e[f]=p[m]+(s?0:h-k.offset()[m]),a.margin&&(e[f]-=parseInt(g.css("margin"+c))||0,e[f]-=parseInt(g.css("border"+c+"Width"))||0),e[f]+=a.offset[m]||0,a.over[m]&&(e[f]+=g["x"==d?"width":"height"]()*a.over[m])):(c=g[m],e[f]=c.slice&&"%"==c.slice(-1)?parseFloat(c)/100*r:c);a.limit&&/^\d+$/.test(e[f])&& (e[f]=0>=e[f]?0:Math.min(e[f],r));!b&&a.queue&&(h!=e[f]&&q(a.onAfterFirst),delete e[f])});q(a.onAfter)}}).end()};n.max=function(b,c){var a="x"==c?"Width":"Height",h="scroll"+a;if(!d(b).is("html,body"))return b[h]-d(b)[a.toLowerCase()]();var a="client"+a,l=b.ownerDocument.documentElement,k=b.ownerDocument.body;return Math.max(l[h],k[h])-Math.min(l[a],k[a])}})(jQuery);

(function(a){a.isScrollToFixed=function(b){return !!a(b).data("ScrollToFixed")};a.ScrollToFixed=function(d,h){var k=this;k.$el=a(d);k.el=d;k.$el.data("ScrollToFixed",k);var c=false;var F=k.$el;var G;var D;var p;var C=0;var q=0;var i=-1;var e=-1;var t=null;var y;var f;function u(){F.trigger("preUnfixed.ScrollToFixed");j();F.trigger("unfixed.ScrollToFixed");e=-1;C=F.offset().top;q=F.offset().left;if(k.options.offsets){q+=(F.offset().left-F.position().left)}if(i==-1){i=q}G=F.css("position");c=true;if(k.options.bottom!=-1){F.trigger("preFixed.ScrollToFixed");w();F.trigger("fixed.ScrollToFixed")}}function m(){var H=k.options.limit;if(!H){return 0}if(typeof(H)==="function"){return H.apply(F)}return H}function o(){return G==="fixed"}function x(){return G==="absolute"}function g(){return !(o()||x())}function w(){if(!o()){t.css({display:F.css("display"),width:F.outerWidth(true),height:F.outerHeight(true),"float":F.css("float")});cssOptions={position:"fixed",top:k.options.bottom==-1?s():"",bottom:k.options.bottom==-1?"":k.options.bottom,"margin-left":"0px"};if(!k.options.dontSetWidth){cssOptions.width=F.width()}F.css(cssOptions);F.addClass("scroll-to-fixed-fixed");if(k.options.className){F.addClass(k.options.className)}G="fixed"}}function b(){var I=m();var H=q;if(k.options.removeOffsets){H=0;I=I-C}cssOptions={position:"absolute",top:I,left:H,"margin-left":"0px",bottom:""};if(!k.options.dontSetWidth){cssOptions.width=F.width()}F.css(cssOptions);G="absolute"}function j(){if(!g()){e=-1;t.css("display","none");F.css({width:"",position:D,left:"",top:p.top,"margin-left":""});F.removeClass("scroll-to-fixed-fixed");if(k.options.className){F.removeClass(k.options.className)}G=null}}function v(H){if(H!=e){F.css("left",q-H);e=H}}function s(){var H=k.options.marginTop;if(!H){return 0}if(typeof(H)==="function"){return H.apply(F)}return H}function z(){if(!a.isScrollToFixed(F)){return}var J=c;if(!c){u()}var H=a(window).scrollLeft();var K=a(window).scrollTop();var I=m();if(k.options.minWidth&&a(window).width()<k.options.minWidth){if(!g()||!J){n();F.trigger("preUnfixed.ScrollToFixed");j();F.trigger("unfixed.ScrollToFixed")}}else{if(k.options.bottom==-1){if(I>0&&K>=I-s()){if(!x()||!J){n();F.trigger("preAbsolute.ScrollToFixed");b();F.trigger("unfixed.ScrollToFixed")}}else{if(K>=C-s()){if(!o()||!J){n();F.trigger("preFixed.ScrollToFixed");w();e=-1;F.trigger("fixed.ScrollToFixed")}v(H)}else{if(!g()||!J){n();F.trigger("preUnfixed.ScrollToFixed");j();F.trigger("unfixed.ScrollToFixed")}}}}else{if(I>0){if(K+a(window).height()-F.outerHeight(true)>=I-(s()||-l())){if(o()){n();F.trigger("preUnfixed.ScrollToFixed");if(D==="absolute"){b()}else{j()}F.trigger("unfixed.ScrollToFixed")}}else{if(!o()){n();F.trigger("preFixed.ScrollToFixed");w()}v(H);F.trigger("fixed.ScrollToFixed")}}else{v(H)}}}}function l(){if(!k.options.bottom){return 0}return k.options.bottom}function n(){var H=F.css("position");if(H=="absolute"){F.trigger("postAbsolute.ScrollToFixed")}else{if(H=="fixed"){F.trigger("postFixed.ScrollToFixed")}else{F.trigger("postUnfixed.ScrollToFixed")}}}var B=function(H){if(F.is(":visible")){c=false;z()}};var E=function(H){z()};var A=function(){var I=document.body;if(document.createElement&&I&&I.appendChild&&I.removeChild){var K=document.createElement("div");if(!K.getBoundingClientRect){return null}K.innerHTML="x";K.style.cssText="position:fixed;top:100px;";I.appendChild(K);var L=I.style.height,M=I.scrollTop;I.style.height="3000px";I.scrollTop=500;var H=K.getBoundingClientRect().top;I.style.height=L;var J=(H===100);I.removeChild(K);I.scrollTop=M;return J}return null};var r=function(H){H=H||window.event;if(H.preventDefault){H.preventDefault()}H.returnValue=false};k.init=function(){k.options=a.extend({},a.ScrollToFixed.defaultOptions,h);k.$el.css("z-index",k.options.zIndex);t=a("<div />");G=F.css("position");D=F.css("position");p=a.extend({},F.offset());if(g()){k.$el.after(t)}a(window).bind("resize.ScrollToFixed",B);a(window).bind("scroll.ScrollToFixed",E);if(k.options.preFixed){F.bind("preFixed.ScrollToFixed",k.options.preFixed)}if(k.options.postFixed){F.bind("postFixed.ScrollToFixed",k.options.postFixed)}if(k.options.preUnfixed){F.bind("preUnfixed.ScrollToFixed",k.options.preUnfixed)}if(k.options.postUnfixed){F.bind("postUnfixed.ScrollToFixed",k.options.postUnfixed)}if(k.options.preAbsolute){F.bind("preAbsolute.ScrollToFixed",k.options.preAbsolute)}if(k.options.postAbsolute){F.bind("postAbsolute.ScrollToFixed",k.options.postAbsolute)}if(k.options.fixed){F.bind("fixed.ScrollToFixed",k.options.fixed)}if(k.options.unfixed){F.bind("unfixed.ScrollToFixed",k.options.unfixed)}if(k.options.spacerClass){t.addClass(k.options.spacerClass)}F.bind("resize.ScrollToFixed",function(){t.height(F.height())});F.bind("scroll.ScrollToFixed",function(){F.trigger("preUnfixed.ScrollToFixed");j();F.trigger("unfixed.ScrollToFixed");z()});F.bind("detach.ScrollToFixed",function(H){r(H);F.trigger("preUnfixed.ScrollToFixed");j();F.trigger("unfixed.ScrollToFixed");a(window).unbind("resize.ScrollToFixed",B);a(window).unbind("scroll.ScrollToFixed",E);F.unbind(".ScrollToFixed");k.$el.removeData("ScrollToFixed")});B()};k.init()};a.ScrollToFixed.defaultOptions={marginTop:0,limit:0,bottom:-1,zIndex:1000};a.fn.scrollToFixed=function(b){return this.each(function(){(new a.ScrollToFixed(this,b))})}})(jQuery);
!function(e){var t={position:"top",animationTime:500,easing:"ease-in-out",offset:20,hidePlaceholderOnFocus:true};e.fn.animateLabel=function(t,n){var r=n.data("position")||t.position,i=0,s=0;e(this).css({left:"auto",right:"auto",position:"absolute","-webkit-transition":"all "+t.animationTime+"ms "+t.easing,"-moz-transition":"all "+t.animationTime+"ms "+t.easing,"-ms-transition":"all "+t.animationTime+"ms "+t.easing,transition:"all "+t.animationTime+"ms "+t.easing});switch(r){case"top":i=0;s=(e(this).height()+t.offset)*-1;e(this).css({top:"0",opacity:"1","-webkit-transform":"translate3d("+i+", "+s+"px, 0)","-moz-transform":"translate3d("+i+", "+s+"px, 0)","-ms-transform":"translate3d("+i+", "+s+"px, 0)",transform:"translate3d("+i+", "+s+"px, 0)"});break;case"bottom":i=0;s=e(this).height()+t.offset;e(this).css({bottom:"0",opacity:"1","-webkit-transform":"translate3d("+i+", "+s+"px, 0)","-moz-transform":"translate3d("+i+", "+s+"px, 0)","-ms-transform":"translate3d("+i+", "+s+"px, 0)",transform:"translate3d("+i+", "+s+"px, 0)"});break;case"left":i=(e(this).width()+t.offset)*-1;s=0;e(this).css({left:0,top:0,opacity:"1","-webkit-transform":"translate3d("+i+"px, "+s+"px, 0)","-moz-transform":"translate3d("+i+"px, "+s+"px, 0)","-ms-transform":"translate3d("+i+"px, "+s+"px, 0)",transform:"translate3d("+i+"px, "+s+"px, 0)"});break;case"right":i=e(this).width()+t.offset;s=0;e(this).css({right:0,top:0,opacity:"1","-webkit-transform":"translate3d("+i+"px, "+s+"px, 0)","-moz-transform":"translate3d("+i+"px, "+s+"px, 0)","-ms-transform":"translate3d("+i+"px, "+s+"px, 0)",transform:"translate3d("+i+"px, "+s+"px, 0)"});break}};e.fn.removeAnimate=function(t,n){var r=n.data("position")||t.position,i=0,s=0;e(this).css({top:"0",opacity:"0","-webkit-transform":"translate3d("+i+", "+s+"px, 0)","-moz-transform":"translate3d("+i+", "+s+"px, 0)","-ms-transform":"translate3d("+i+", "+s+"px, 0)",transform:"translate3d("+i+", "+s+"px, 0)"})};e.fn.label_better=function(n){var r=e.extend({},t,n),i=e(this),s="focus",o="blur";if(r.easing=="bounce")r.easing="cubic-bezier(0.175, 0.885, 0.420, 1.310)";i.each(function(t,n){var i=e(this),u=i.data("position")||r.position;i.wrapAll("<div class='lb_wrap' style='position:relative; display: inline;'></div>");if(i.val().length>0){var a=i.data("new-placeholder")||i.attr("placeholder");e("<div class='lb_label "+u+"'>"+a+"</div>").css("opacity","0").insertAfter(i).animateLabel(r,i)}i.bind(s,function(){if(i.val().length<1){var t=i.data("new-placeholder")||i.attr("placeholder"),n=i.data("position")||r.position;e("<div class='lb_label "+n+"'>"+t+"</div>").css("opacity","0").insertAfter(i).animateLabel(r,i)}if(r.hidePlaceholderOnFocus==true){i.data("default-placeholder",i.attr("placeholder"));i.attr("placeholder","")}i.parent().find(".lb_label").addClass("active")}).bind(o,function(){if(i.val().length<1){i.parent().find(".lb_label").bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",function(){e(this).remove()}).removeAnimate(r,i)}if(r.hidePlaceholderOnFocus==true){i.attr("placeholder",i.data("default-placeholder"));i.data("default-placeholder","")}i.parent().find(".lb_label").removeClass("active")})})}}(window.jQuery)

var $ = jQuery.noConflict();
(function(c,n){var l="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";c.fn.imagesLoaded=function(f){function m(){var b=c(i),a=c(h);d&&(h.length?d.reject(e,b,a):d.resolve(e));c.isFunction(f)&&f.call(g,e,b,a)}function j(b,a){b.src===l||-1!==c.inArray(b,k)||(k.push(b),a?h.push(b):i.push(b),c.data(b,"imagesLoaded",{isBroken:a,src:b.src}),o&&d.notifyWith(c(b),[a,e,c(i),c(h)]),e.length===k.length&&(setTimeout(m),e.unbind(".imagesLoaded")))}var g=this,d=c.isFunction(c.Deferred)?c.Deferred():
0,o=c.isFunction(d.notify),e=g.find("img").add(g.filter("img")),k=[],i=[],h=[];c.isPlainObject(f)&&c.each(f,function(b,a){if("callback"===b)f=a;else if(d)d[b](a)});e.length?e.bind("load.imagesLoaded error.imagesLoaded",function(b){j(b.target,"error"===b.type)}).each(function(b,a){var d=a.src,e=c.data(a,"imagesLoaded");if(e&&e.src===d)j(a,e.isBroken);else if(a.complete&&a.naturalWidth!==n)j(a,0===a.naturalWidth||0===a.naturalHeight);else if(a.readyState||a.complete)a.src=l,a.src=d}):m();return d?d.promise(g):
g}})(jQuery);

(function($) {
	 jQuery(".navbar-nav") .mobileMenu({switchWidth:768, topOptionText: ' ', prependTo: '.navmobile-container'});
	function is_touch_device() {
	  return !!('ontouchstart' in window) // works on most browsers 
		  || !!('onmsgesturechange' in window); // works on ie10

	};

	// SLIDERS INIT
    $(window).load(function() {
      $('.devices').flexslider({
        animation: "fade",
        directionNav: false,
        manualControls: ".devices-nav li",
        slideshow: false
      });
      $('#home-slider').flexslider({
        animation: "fade",
        controlNav: true,
        directionNav: false,
        slideshowSpeed: 5000,
        animationSpeed: 500
      });
      $('.testimonials-slider').flexslider({
        animation: "fade"
      });
      $('.tweet').flexslider({
        animation: "fade",
        selector: ".tweet_list > li"
      });
      $('.project-slider').flexslider({
        animation: "slide"
      });
      $('.clients-carousel').flexslider({
        animation: "slide",
        itemWidth: 190,
        controlNav: false,
        directionNav: false,
        useCSS: false
      });
	   $('.slideshow').flexslider({
        animation: "slide",
        useCSS: false
      });
    });

	// Fullscreen home
	function homeHeight() {
		var windowheight = $(window).height();
		var	navbarheight = $("#navbar").outerHeight();
		var	home = $('.homepage-section');

		if ( home.hasClass("home-fullscreen") && $("#navbar").hasClass("nav-home-bottom")) {
			home.css('height', windowheight - navbarheight);
		} else 
		if ( home.hasClass("home-fullscreen") ) {
			$('.homepage-section').css('height', windowheight);
		}
	};


	$(window).load(function() {
		var homeTextHeight = $("#home-slider").height()
		$(".home-text").css("height", homeTextHeight + 18 + 60)
	});

		// Fixed navbar
	function fixedNavbar() {
		/*
		var navbar = $("#navbar")
			sidebarTrigger = $("#st-trigger-effects")
		if (navbar.hasClass("nav-fixed")) {
			navbar.scrollToFixed();
		};
		if (sidebarTrigger.hasClass("nav-fixed")) {
			sidebarTrigger.scrollToFixed();
		};
		*/
  	};

	// Counting
	function counting() {
		$('.count-item').waypoint(function() {
	    	$('.counter').countTo();
	    	$('.counter').removeClass('counter');
		}, { offset: '90%' })
	};

	
  	// Contact form validation
  	function contactFormValid() {
		$('form#contact-form').submit(function() {
			$('form#contact-form .alert').remove();
			var hasError = false;
			$('.requiredField').each(function() {
				if(jQuery.trim($(this).val()) == '') {
	            	var labelText = $(this).prev('span').text();
	            	$(this).addClass('input-error');
	            	$(this).parent().find('span').addClass('input-error');
	            	hasError = true;
	            } else if($(this).hasClass('email')) {
	            	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	            	if(!emailReg.test(jQuery.trim($(this).val()))) {
	            		var labelText = $(this).prev('span').text();
	            		$(this).addClass('input-error');
	            	$(this).parent().find('span').addClass('input-error');
	            		hasError = true;
	            	}
	            }
			});
			if(!hasError) {
				$('form#contact-form .btn-submit').fadeOut(1, function() {
					$(this).parent().append('<input tabindex="5" value="Sending..." class="btn-submit btn">');
				});
				var formInput = $(this).serialize();
				$.post($(this).attr('action'),formInput, function(data){
					$('form#contact-form').slideUp("fast", function() {
						$(this).before('<div class="alert alert-success">Your email was successfully sent!</div>');
					});
				});
			}

			return false;
		});
	};



	// Portfolio filtering
	function filtering() {
		$(window).load(function(){  
			// cache container
			var $container = $('.portfolio-items-container');
			$container.isotope({
			  resizesContainer: true,
			});

			// filter items when filter link is clicked
			$('.non-paginated #filters a').click(function(){
			  var selector = $(this).attr('data-filter');
			  $container.isotope({ filter: selector });
			  
			  //active classes
			  $('.non-paginated #filters li a').removeClass('active');
			  $(this).addClass('active');
			  
			  return false;
			});
		});
	};



	// Contact form labels
	function labelBetter() {
	    $("input.label-better").label_better({
	    position: "top",
	    animationTime: 100,
	    // easing: "ease-in-out",
	     offset: 60,
	    // hidePlaceholderOnFocus: true
	    });
	    $("textarea.label-better").label_better({
	    position: "top",
	    animationTime: 100,
	    // easing: "ease-in-out",
	     offset: 160,
	    // hidePlaceholderOnFocus: true
	    });
    };

	$.fn.hasAttr = function(name) {  
			return this.attr(name) !== undefined;
	};

    function projectLoad() {
    	var projectItem = $('a.inner-link');
    		error = "<span class=\"project-error\">Project does not exist</span>";
    		loader = "<span class=\"project-loader\">Loading...</span>";

    	projectItem.click(function() {
    		var href = $(this).attr('href');
    			container = $("#project-container");	
    		
				if ($(this).children().hasClass("active-project")) {

					return false;

				} else {

					// $("body").scrollTo("#project-container", 500, function(){
						$('html, body').animate({ scrollTop: container.offset().top -40}, 500);
						container.slideUp(500).addClass("project");

						setTimeout(function() {
						  container.after(loader);
						  container.empty();
						}, 500);

						setTimeout(function() {
						container.load(href, { type: 'single'}, function(response, status) {
							if (status == "error") {
								container.append(error).slideDown(500);
							} else {
								container.slideDown(500);
							}
							$(".project-loader").remove();
						})
						}, 500);
					// });

				}

	    		projectItem.children().removeClass("active-project");
	    		$(this).children().addClass("active-project");
	    		return false;
	    		
    	});

    	var projectNav = $(".project-nav a");

    	projectNav.live('click', function() {	
			
    		container.slideUp(500);

			setTimeout(function() {
			  container.empty().removeClass("project");
			  projectItem.children().removeClass("active-project");
			}, 500);

    		return false;
    	});
    };
	
	function hideOnScroll() {
		$("#st-trigger-effects a").click(function(){
			var scrtop = $(window).scrollTop()
			scrtopvar = scrtop;
		});

		$(window).scroll(function() {
		var scrtop = $(window).scrollTop();
			scrolloffset = 10;
		if ($("#st-container").hasClass("st-menu-open")) {
	
			if (scrtopvar >= scrtop + scrolloffset || scrtopvar <= scrtop - scrolloffset) {
				$("#st-container").removeClass("st-menu-open");
			}
		}
	    });

	};   

	function animations() {
		var animOffset = '90%';
			animTime = 700;
if (screen.width > '977') {
		function fDelay(selector) {
			if ($(selector).hasAttr("fade-delay")) {
				fadeDelayAttr = $(selector).attr("fade-delay")
				fadeDelay = fadeDelayAttr;
			} else {
				fadeDelay = 0;
			}
		};

		$('.fadeLeft').each(function () {
			$(this).waypoint(function() {
				fDelay(this);
				$(this).delay(fadeDelay).animate({opacity:1,left:"0px"},animTime);
			}, { offset: animOffset });
		});		

		$('.fadeRight').each(function () {
			$(this).waypoint(function() {
				fDelay(this);
				$(this).delay(fadeDelay).animate({opacity:1,right:"0px"},animTime);
			}, { offset: animOffset });
		});	

		$('.fadeTop').each(function () {
			$(this).waypoint(function() {
				fDelay(this);
			  	$(this).delay(fadeDelay).animate({opacity:1,top:"0px"},animTime);
			}, { offset: animOffset });
		});	

		$('.fadeBottom').each(function () {
			$(this).waypoint(function() {
				fDelay(this);
				$(this).delay(fadeDelay).animate({opacity:1,bottom:"0px"},animTime);
			}, { offset: animOffset });
		});	

		$('.fadeIn').each(function () {
			$(this).waypoint(function() {
				fDelay(this);
				$(this).delay(fadeDelay).animate({opacity:1},animTime);
			}, { offset: animOffset });
		});	
		
	};
};

	function fitVid() {
		$(".fit-vid").fitVids();
	};

	function navBarf() {
		var navbar = $("#navbar")
		$(window).scroll(function() {
		if (navbar.hasClass("nav-fixed") && navbar.hasClass("nav-home-top") && $(window).scrollTop() >= 1) {
			navbar.addClass("nav-fixed-fixed");
		} else {
			navbar.removeClass("nav-fixed-fixed");
		}
		});

		if (navbar.hasClass("nav-home-top")) {
			$(".main-nav ul li:first-child a").addClass("active");
			$(".main-nav ul li ul li a").removeClass("active");
			$(".logo-home").css("display", "none");
		}

		if (navbar.hasClass("nav-home-bottom")) {
			navbar.addClass("submenu-up");
		}

		$(window).scroll(function() {
		if (navbar.hasClass("nav-home-bottom") && $(window).scrollTop() >= 130) {
			navbar.removeClass("submenu-up");
		} else if (navbar.hasClass("nav-home-bottom")) {
			navbar.addClass("submenu-up");
		}
		});
	};

	function homeBgPlayer() {
      $(".home-bg-player").mb_YTPlayer();
    };
	
	/* ---------------------------------------------------------------------- */
	/*	accordion
	/* ---------------------------------------------------------------------- */
	$(document).ready(function($) {
		try {
			$('#posts-3-in-1 a').click(function (e) {
			  e.preventDefault()
			  $(this).tab('show')
			})
		} catch(err) {
			alert ('Tabs are not working');
		}

		var acordDivHide = $('.accordion div');
		var	acordHeadClick = $('.accordion h2');
		acordDivHide.addClass('hide');
		acordHeadClick.on('click', function() {
			var $this = $(this);
			
			if ( !$this.hasClass('active')) {
				acordHeadClick.removeClass('active');
				$this.addClass('active').next().slideDown(300).siblings('.accordion div').slideUp(300);
			}
			else {
				$(this).removeClass('active').next().slideUp(300);
			}
		});
	});

	//Function Initializing
	homeHeight();
	counting();
	fixedNavbar();
	//scrollToNavbar();
	contactFormValid();
	labelBetter();
	filtering();
	projectLoad();
	//hideOnScroll();
	animations();
	//fitVid();
	navBarf();

	homeBgPlayer();

	if (!is_touch_device()) {
		var s = skrollr.init();
	}

	if (is_touch_device()) {
		$(".team-member-big, .team-member-small").click(function() {
			return false;
		});
	}

      
	$(window).resize(function(){	
		homeHeight();
		navBarf();
	});	

})(jQuery);

$(window).load(function() {
	$(".loader").delay(300).fadeOut();
	$("#page-loader").delay(500).fadeOut("slow");
});






$(document).ready(function($) {
	"use strict";
	$("a[data-rel^='prettyPhoto']").prettyPhoto({hook: 'data-rel'});
	
	
	/*-------------------------------------------------*/
	/* =   Smooth scroll
	/*-------------------------------------------------*/

	var currentLocation = location.href;
	
	var hashPosition = currentLocation.indexOf('#');
	if (hashPosition > 1) {
		currentLocation = currentLocation.substr(0, hashPosition);
	}
	
	//Get Sections top position
	function getTargetTop(elem){
		
		//gets the id of the section header
		//from the navigation's href e.g. ("#html")
		var id = elem.attr("href");
		
		id = id.substr(id.indexOf('#'));

		//Height of the navigation
		var offset = 100;

		//Gets the distance from the top and 
		//subtracts the height of the nav.
		return $(id).offset().top - offset;
	}

	//Smooth scroll when user click link that starts with #

	var elemHref = $('.navbar-right a[href^="#"], .navbar-right a[href^="' + currentLocation + '#"], .navbar-right a[href^="' + currentLocation + '/#"]')
elemHref = $($.grep(elemHref, function (section) {
  var hash = $(section).attr('href');
  hash = hash.substr(hash.indexOf('#'));
  return $(hash).length > 0;
        }));

	elemHref.click(function(event) {
		//alert (currentLocation);
		//gets the distance from the top of the 
		//section refenced in the href.
		var target = getTargetTop($(this));

		//scrolls to that section.
		$('html, body').animate({scrollTop:target}, 500);

		//prevent the browser from jumping down to section.
		event.preventDefault();
	});

	//Pulling sections from main nav.
	var sections = elemHref;

	// Go through each section to see if it's at the top.
	// if it is add an active class
	function checkSectionSelected(scrolledTo){
		
		//How close the top has to be to the section.
		var threshold = 101;

		var i;

		for (i = 0; i < sections.length; i++) {
			
			//get next nav item
			var section = $(sections[i]);

			//get the distance from top
			var target = getTargetTop(section);
			
			//Check if section is at the top of the page.
			if (scrolledTo > target - threshold && scrolledTo < target + threshold) {

				//remove all selected elements
				sections.removeClass("active");

				//add current selected element.
				section.addClass("active");
                                //var hash = section.attr('href');
                                //hash = hash.substr(hash.indexOf('#'));
                                //window.location.hash = hash;
			}

		};
	}

	//Check if page is already scrolled to a section.
	checkSectionSelected($(window).scrollTop());

	$(window).scroll(function(e){
		checkSectionSelected($(window).scrollTop())
	});
	
	$('.home-more a').click(function(){
		$('html, body').animate({
			scrollTop: $( $.attr(this, 'href') ).offset().top
		}, 500);
		return false;
	});
	
	// Toggles
	$(".toggle-container").hide();
	$(".toggle-trigger").click(function(e){
		e.preventDefault();
		$(this).toggleClass("open").parent().next().slideToggle(500);
		return false;
	});
	$(".nav-list.affix").affix();
	$(".alert").alert();

	/*-------------------------------------------------*/
	/* =  Banner slider
	/*-------------------------------------------------*/

	var sliderTestimonial = $('.bxslider');
	try{		
		sliderTestimonial.bxSlider({
			mode: 'vertical'
		});
	} catch(err) {
	}
	
	

	/* ---------------------------------------------------------------------- */
	/*	accordion
	/* ---------------------------------------------------------------------- */
		
		var acordDivHide = $('.accordion div'),
			acordHeadClick = $('.accordion h2');


		acordDivHide.addClass('hide');

		acordHeadClick.on('click', function() {
			var $this = $(this);
			
			if ( !$this.hasClass('active')) {
			acordHeadClick.removeClass('active');
			$this
				.addClass('active')
				.next()
					.slideDown(300)
					.siblings('.accordion div')
						.slideUp(300);
			}
			else {
				$(this).removeClass('active')
						.next()
						.slideUp(300);
			}
		});
});

/********* Contact Widget *************/
function checkemail(emailaddress){
	"use strict";
	var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i); 
	return pattern.test(emailaddress); 
}

jQuery(document).ready(function(){ 
	"use strict";
	jQuery('#registerErrors, .widgetinfo').hide();		
	var $messageshort = false;
	var $emailerror = false;
	var $nameshort = false;
	var $namelong = false;
	
	jQuery('#contactFormWidget input#wformsend').click(function(){ 
		var $name = jQuery('#wname').val();
		var $email = jQuery('#wemail').val();
		var $message = jQuery('#wmessage').val();
		var $contactemail = jQuery('#wcontactemail').val();
		var $contacturl = jQuery('#wcontacturl').val();
		var $subject = jQuery('#wsubject').val();
	
		if ($name !== '' && $name.length < 3){ $nameshort = true; } else { $nameshort = false; }
		if ($name !== '' && $name.length > 30){ $namelong = true; } else { $namelong = false; }
		if ($email !== '' && checkemail($email)){ $emailerror = true; } else { $emailerror = false; }
		if ($message !== '' && $message !== 'Message' && $message.length < 3){ $messageshort = true; } else { $messageshort = false; }
		
		jQuery('#contactFormWidget .loading').animate({opacity: 1}, 250);
		
		if ($name !== '' && $nameshort !== true && $namelong !== true && $email !== '' && $emailerror !== false && $message !== '' && $messageshort !== true && $contactemail !== ''){ 
			jQuery.post($contacturl, 
				{type: 'widget', contactemail: $contactemail, subject: $subject, name: $name, email: $email, message: $message}, 
				function(/*data*/){
					jQuery('#contactFormWidget .loading').animate({opacity: 0}, 250);
					jQuery('.form').fadeOut();
					jQuery('#bottom #wname, #bottom #wemail, #bottom #wmessage').css({'border':'0'});
					jQuery('.widgeterror').hide();
					jQuery('.widgetinfo').fadeIn('slow');
					jQuery('.widgetinfo').delay(2000).fadeOut(1000, function(){ 
						jQuery('#wname, #wemail, #wmessage').val('');
						jQuery('.form').fadeIn('slow');
					});
				}
			);
			
			return false;
		} else {
			jQuery('#contactFormWidget .loading').animate({opacity: 0}, 250);
			jQuery('.widgeterror').hide();
			jQuery('.widgeterror').fadeIn('fast');
			jQuery('.widgeterror').delay(3000).fadeOut(1000);
			
			if ($name === '' || $name === 'Name' || $nameshort === true || $namelong === true){ 
				jQuery('#wname').css({'border-left':'4px solid #red'}); 
			} else { 
				jQuery('#wname').css({'border-left':'4px solid #929DAC'}); 
			}
			
			if ($email === '' || $email === 'Email' || $emailerror === false){ 
				jQuery('#wemail').css({'border-left':'4px solid red'});
			} else { 
				jQuery('#wemail').css({'border-left':'4px solid #929DAC'}); 
			}
			
			if ($message === '' || $message === 'Message' || $messageshort === true){ 
				jQuery('#wmessage').css({'border-left':'4px solid red'}); 
			} else { 
				jQuery('#wmessage').css({'border-left':'4px solid #929DAC'}); 
			}
			
			return false;
		}
	});
});

$(".mnav").click(function(){
	$(".menu-mob").slideToggle(140);
});
$(".limen").click(function(){
	$(".menu-mob").slideToggle(140);
});
