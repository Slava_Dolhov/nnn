<?php /** The Template for displaying all single posts. **/

get_header('standalone');
$get_meta = get_post_custom($post->ID);
$weblusive_sidebar_pos = weblusive_get_option('sidebar_pos');
$sidebar = weblusive_get_option('sidebar_archive');

$showdate = weblusive_get_option('blog_show_date'); 
$showcomments = weblusive_get_option('blog_show_comments'); 
$showcategory = weblusive_get_option('blog_show_category'); 
?>
<div class="content-dedicated">
	<?php get_template_part( 'library/includes/page-head' ); ?>
	
	<!-- SECTION HEADER -->
	<div id="page-header">
		<div class="container">
			<div class="page-title section-header sh-left">
				<h1>
					<span><?php the_title(); ?></span>
				</h1>
				<div class="header-desc post-info">
					<ul>
						<?php if($showcategory): ?>				
							<?php if ( count( get_the_category() ) ) :
								$category = get_the_category();  if($category[0]):?>
								<li>
									<a href="<?php echo get_category_link($category[0]->term_id )?>"><?php echo $category[0]->cat_name?></a>
								</li>
								<?php endif?>
							<?php endif?>
						<?php endif?>
						<?php if( 'open' == $post->comment_status && $showcomments) : ?>   
							<li>
								<?php comments_popup_link( __( '0 Comments', 'Ante' ), __( '1 Comment', 'Ante' ), __( '% Comments', 'Ante' )); ?>
							</li>
						<?php endif?>
						<?php if($showdate): ?>
							<li>
								<a href="<?php echo get_day_link(get_the_time('Y'), get_the_time('m'), get_the_time('d')); ?>">
									<?php echo get_the_time('M d, Y'); ?>
								</a>
							</li>
						<?php endif?>
					</ul>
				</div>
				<?php if (!weblusive_get_option('hide_breadcrumbs')):?>
					<nav id="breadcrumbs">
						<?php if(class_exists('the_breadcrumb')){ $albc = new the_breadcrumb; } ?>
					</nav>
				<?php endif?>
			</div>
		</div>
	</div>
	<!-- SECTION HEADER End -->

	<section class="page-content">
		<div class="page-bg"></div>
		<section class="section">
			<div class="container">
				<div class="row">
					<?php if ($weblusive_sidebar_pos == 'left') get_sidebar($sidebar); ?>
					<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
						<div class="<?php if (empty($sidebar)):?>col-md-12<?php else:?>col-md-9<?php endif?>">
							<?php 
							$get_meta = get_post_custom($post->ID);
							$mediatype = isset($get_meta["_blog_mediatype"]) ? $get_meta["_blog_mediatype"][0] : 'image';
							$videoId = isset($get_meta["_blog_video"]) ? $get_meta["_blog_video"][0] : ''; 
							$autoplay =  isset($get_meta["_blog_videoap"]) ? $get_meta["_blog_videoap"][0] : '0';
							$thumbnail = get_the_post_thumbnail($post->ID, 'blog-list');
							?>
							<article id="post-<?php the_ID();?>" <?php post_class('single-post post-container'); ?>>	
								<div class="media-wrapper">
									<?php if($mediatype== 'image' && !empty($thumbnail)):?>
										<a href="<?php the_permalink()?>"><?php the_post_thumbnail('blog-list'); ?></a>
									<?php elseif ( $mediatype == "youtube" && $videoId):?>
										<iframe width="740" height="300" src="http://www.youtube.com/embed/<?php echo $videoId?>?autoplay=<?php echo $autoplay?>" class="vid iframe-youtube"></iframe>
									<?php elseif ( $mediatype == "vimeo" && $videoId):?>
										<iframe width="740" height="300" src="http://player.vimeo.com/video/<?php echo $videoId?>?autoplay=<?php echo $autoplay?>" class="vid iframe-vimeo"></iframe>
									<?php elseif ( $mediatype == "dailymotion" && $videoId):?>
										<iframe width="740" height="300" src="http://www.dailymotion.com/embed/video/<?php echo $videoId?>?autoplay=<?php echo $autoplay?>" class="vid dailymotion-vimeo"></iframe>
									<?php elseif ( $mediatype == "veoh" && $videoId):?>
										<iframe width="740" height="300" src="http://www.veoh.com/static/swf/veoh/SPL.swf?videoAutoPlay=<?php echo $autoplay?>&permalinkId=<?php echo $videoId?>" class="vid iframe-veoh"></iframe>
									<?php elseif ( $mediatype == "bliptv" && $videoId):?>
										<iframe width="740" height="300" src="http://a.blip.tv/scripts/shoggplayer.html#file=http://blip.tv/rss/flash/<?php echo $videoId?>?autoplay=<?php echo $autoplay?>" class="vid iframe-bliptv"></iframe>
									<?php elseif ( $mediatype == "viddler" && $videoId):?>
										<iframe width="740" height="300" src="http://www.viddler.com/embed/<?php echo $videoId?>e/?f=1&offset=0&autoplay=<?php echo $autoplay?>" class="vid iframe-viddler"></iframe>
									<?php elseif($mediatype== 'slider' && !empty($thumbnail)):?>
										<div class="flexslider">
											<ul class="slides">
											<?php 
											$argsThumb = array(
												'order'          => 'ASC',
												'post_type'      => 'attachment',
												'post_parent'    => $post->ID,
												'post_mime_type' => 'image',
												'post_status'    => null,
												//'exclude' => get_post_thumbnail_id()
											);
											$attachments = get_posts($argsThumb);
											if ($attachments) {
												foreach ($attachments as $attachment) {
													echo '<li><img src="'.wp_get_attachment_url($attachment->ID, 'full', false, false).'" alt="'.get_post_meta($attachment->ID, '_wp_attachment_image_alt', true).'" /></li>';
												}
											}?>
											</ul>
										</div>
									<?php endif?>
								</div>
								<div class="post-content">
									<?php the_content() ?>
									<?php if(isset($showtags)): ?>			
										<div class="tagcloud">
											<ul>
												<li>
												<?php $tags_list = get_the_tag_list( '<li>', '', '</li>' ); if ( $tags_list ) :  printf( __( '%s', 'Ante' ), $tags_list ); endif; ?>
												</li>
											</ul>
										</div>
									<?php endif?>
								</div>
							</article>
							<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'Ante' ), 'after' => '</div>' ) ); ?>
							<div class="comments-section">
								<?php if( 'open' == $post->comment_status) comments_template(); ?>
								<?php $test = false; if ($test) {comment_form();} ?>
							</div>	
							<div class="clear"></div>
						</div>
					<?php endwhile; ?>	
					<?php if ($weblusive_sidebar_pos == 'right') get_sidebar($sidebar); ?>
				</div>
			</div>
		</section>
	</section>
</div>
<?php get_footer(); ?>
