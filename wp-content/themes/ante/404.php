<?php get_header('standalone');?>

<div class="content-dedicated">
	<div id="page-header">
		<div class="container">
			<div class="page-title section-header sh-left">
				<?php if (!weblusive_get_option('hide_titles')):?>
					<h1><span><?php _e('Page not found', 'Ante')?></span></h1>
					<?php echo (isset($tagline) && !empty($tagline)) ? '<div class="header-desc"><span>'.$tagline.'</span></div>' : ''; ?>
				<?php endif?>
				<?php if (!weblusive_get_option('hide_breadcrumbs')):?>
					<nav id="breadcrumbs">
						<?php if(class_exists('the_breadcrumb')){ $albc = new the_breadcrumb; } ?>
					</nav>
				<?php endif?>
			</div>
		</div>
	</div>
	<section class="page-content">
		<section class="section">
			<div class="container main-content">
				<div class="row-fluid">
					<div class="col-md-6 col-md-offset-3">
						<div class="notfound"></div> 
						<p class="lost"></p>
						<p class="notfound_description">
							<?php _e('The page you are looking for seems to be missing.Go back, or return to yourcompany.com to choose a new direction.Please report any broken links to our team.', 'Ante')?>
						</p>
						<p class="text-center">	
							<a class="btn btn-large" href="javascript: history.go(-1)"><i class="fa fa-undo"></i><?php _e('Return to previous page', 'Ante')?></a>
						</p>
					</div>
				</div>
			</div>
		</section>
	</section>
</div>
<?php get_footer(); ?>
