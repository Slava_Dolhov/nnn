<?php 
	get_header('standalone');

	//global $paged;
 	$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
	
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	if ( get_query_var('paged') ) {
		$paged = get_query_var('paged');
	} elseif ( get_query_var('page') ) {
		$paged = get_query_var('page');
	} else {
		$paged = 1;
	}
	
	$pageId = $_SESSION['Ante_page_id'];
	if (!$pageId) $pageId = get_page_ID_by_page_template('portfolio-template-3columns.php');
	$get_meta = get_post_custom($pageId);
	$weblusive_sidebar_pos = $get_meta["_weblusive_sidebar_pos"][0];
	$portfolio_type = get_post_meta($pageId, "_portfolio_type", $single = false);
	$paginationEnabled = (isset($portfolio_type) && !(empty ($portfolio_type))) ? $portfolio_type[0] : 0;
	$specialclass =  $paginationEnabled ? 'portfolio-paginated' : 'non-paginated';
	$tagline = isset( $get_meta['_weblusive_tagline'][0]) ? $get_meta["_weblusive_tagline"][0] : '';
	$headline = isset( $get_meta['_weblusive_alttitle'][0]) ? $get_meta["_weblusive_alttitle"][0] : '';

	$itemsize = 'col-md-3';	
	$itemlayout = 'four-col';	
	$colnumber = 4;
	$thumbsize = 'portfolio-4-col';
?>
<div class="content-dedicated pagecustom-<?php echo $pageId?>">
	<?php get_template_part( 'library/includes/page-head' ); ?>
	<div id="page-header">
		<div class="container">
			<div class="page-title section-header sh-left">
				<?php if (!weblusive_get_option('hide_titles')):?>
					<h1><span><?php echo $term->slug; ?></span></h1>
					<?php echo (isset($tagline) && !empty($tagline)) ? '<div class="header-desc"><span>'.$tagline.'</span></div>' : ''; ?>
				<?php endif?>
				<?php if (!weblusive_get_option('hide_breadcrumbs')):?>
					<nav id="breadcrumbs">
						<?php if(class_exists('the_breadcrumb')){ $albc = new the_breadcrumb; } ?>
					</nav>
				<?php endif?>
			</div>
		</div>
	</div>
	<section class="page-content">
		<section class="section">
			<div class="container main-content <?php echo $specialclass ?>">
				<div class="row">
					<div class="col-sm-12 resp-no-gap">
						<?php echo do_shortcode(getPageContent($pageId)); ?>
						<div class="portfolio-filter">
							<ul class="filter" id="filters">
								<li><a href="<?php echo get_page_link($pageId) ?>"><?php _e('Show All', 'Ante')?></a></li>
								<?php 
									$cats = get_post_meta($pageId, "_page_portfolio_cat", $single = true);
									$MyWalker = new PortfolioWalker2();
									$args = array( 'taxonomy' => 'portfolio_category', 'hide_empty' => '0', 'include' => $cats, 'title_li'=> '', 'walker' => $MyWalker, 'show_count' => '1');
									$categories = wp_list_categories ($args);
								?>
								<!-- End Portfolio Navigation -->
							</ul>
						</div>
					</div>
				</div>
			</div>
		<div class="row">
			<div class="col-md-12">		
				<div class="portfolio-items-container <?php echo $itemlayout?>">
					<?php
					$counter=1;
					if ($wp_query->have_posts()):
						while ($wp_query->have_posts()) : 							
							$wp_query->the_post();
							$custom = get_post_custom($post->ID);
							// Get the portfolio item categories
							$cats = wp_get_object_terms($post->ID, 'portfolio_category');
							if ($cats):
								$cat_slugs = '';
								foreach( $cats as $cat ) {$cat_slugs .= $cat->slug . " ";}
							endif;
							$link = ''; $thumbnail = get_the_post_thumbnail($post->ID, $thumbsize);
							?>
							<div class="project-post <?php echo $cat_slugs; ?>">
								<?php if (!empty($thumbnail)): ?>
									<?php the_post_thumbnail($thumbsize, array('class' => 'cover')); ?>
								<?php else :?>
									 <img src="http://placehold.it/405x311" alt="<?php _e ('No preview image', 'Kappe') ?>" />
								<?php endif?>
								<div class="hover-box">
									<div class="project-title">
										<h4><?php the_title() ?></h4>                                    
										<span><?php echo $cat_slugs; ?></span>
										<?php if( !empty ( $custom['_portfolio_video'][0] ) ) : $link = $custom['_portfolio_video'][0]; ?>
											<a href="<?php echo $link ?>" class="zoom video" data-rel="prettyPhoto" title="<?php the_title() ?>"><i class="fa fa-film"></i></a>
										<?php elseif( isset($custom['_portfolio_link'][0]) && $custom['_portfolio_link'][0] != '' ) : ?>
											<a href="<?php echo $custom['_portfolio_link'][0] ?>"  title="<?php the_title() ?>">
												<i class="fa fa-external-link"></i>
											</a>
										<?php elseif(  isset( $custom['_portfolio_no_lightbox'][0] )  &&  $custom['_portfolio_no_lightbox'][0] !='' ) : $link = get_permalink(get_the_ID());  ?>
											<a href="<?php echo $link; ?>"  title="<?php the_title() ?>"><i class="fa fa-arrow-right"></i></a>
										<?php elseif(  isset( $custom['_portfolio_inner_lightbox'][0] )  &&  $custom['_portfolio_inner_lightbox'][0] !='' ) : 
											$id = get_the_ID(); $link = '#portfolio-item-'.$id;  ?>
											<a href="<?php echo $link; ?>" class="link-project button btn-icon" data-rel="prettyPhoto" >
												<i class="fa fa-location-arrow icon-large"></i>
											</a>
											<div id="portfolio-item-<?php echo $id?>" class="hide">
												<?php the_content()?>
											</div>
										<?php else : ?>
											<?php  
												$full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false);
												$argsThumb = array(
													'order'          => 'ASC',
													'posts_per_page'  => 99,
													'post_type'      => 'attachment',
													'post_parent'    => $post->ID,
													'post_mime_type' => 'image',
													'post_status'    => null,
													'exclude' => get_post_thumbnail_id()
												);														
												$link = $full_image[0];
											?>
											<a href="<?php echo $link ?>" class="zoom-image" data-rel="prettyPhoto[ppgal<?php echo $post->ID?>]" title="<?php the_title() ?>"><i class="fa fa-search"></i></a>
											<?php 
												$attachments = get_posts($argsThumb);
												if ($attachments) {
													foreach ($attachments as $attachment) {
														echo '<a href="'.wp_get_attachment_url($attachment->ID, 'full', false, false).'" data-rel="prettyPhoto[ppgal'.$post->ID.']" class="gallery-link" title="'.get_the_title($post->ID).'"></a>';
													}
												}
											?>
										<?php endif; ?>  
									 </div>   
								</div>	
							</div>			
						
							<?php $counter++; endwhile; ?>				
						<?php endif?>
					</div>
				</div>
			</div>
			<?php if ($paginationEnabled ):?>
				<?php if ( $wp_query->max_num_pages > 1 ): ?>
					<div class="pagination-list blog-pagination">
						<?php include(Ante_PLUGINS . '/wp-pagenavi.php' ); wp_pagenavi(); ?> 
						<div class="clear"></div>
					</div>
				<?php endif?>
			<?php endif?>		
			<div class="clear"></div>
		</section>
	</section>
</div>
<?php get_footer() ?>