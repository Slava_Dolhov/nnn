<!DOCTYPE html>
<!--[if IE 8]> 	<html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title><?php bloginfo('name'); ?><?php wp_title(); ?></title>  

	<link rel="alternate" type="application/rss+xml" title="RSS2.0" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        
   	<?php $favicon = weblusive_get_option('favicon'); if(!empty($favicon)):?>
		<link rel="shortcut icon" href="<?php echo $favicon ?>" /> 
 	<?php endif?>
	
	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/ie8.css" type="text/css" media="screen">
	<![endif]-->
	<?php if(weblusive_get_option('header_code')) echo  htmlspecialchars_decode(weblusive_get_option('header_code')); ?>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
 <!-- PAGE LOADER -->
 	<?php $loaderlogo = weblusive_get_option('loader_logo');  ?>
    <div id="page-loader">
      <img style="width:257px" src="<?php echo empty($loaderlogo) ? '/wp-content/9xgsnys.jpg' : $loaderlogo?>" alt="" />
      <div class="loader">
        <div class="spinner">
          <div class="spinner-container container1">
            <div class="circle1"></div>
            <div class="circle2"></div>
            <div class="circle3"></div>
            <div class="circle4"></div>
          </div>
          <div class="spinner-container container2">
            <div class="circle1"></div>
            <div class="circle2"></div>
            <div class="circle3"></div>
            <div class="circle4"></div>
          </div>
          <div class="spinner-container container3">
            <div class="circle1"></div>
            <div class="circle2"></div>
            <div class="circle3"></div>
            <div class="circle4"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- PAGE LOADER End -->
<?php if (!is_page_template('under-construction.php')):?>
			
<div id="container">
	<header class="clearfix">
		<!-- Static navbar -->
		<?php 
			$menu_class =  weblusive_get_option('menu_type'); 
			switch ($menu_class)
			{
				case 1:
					$menu_class='nav-fixed hidden-xs hidden-sm nav-home-top scroll-to-fixed-fixed';
				break;
				
				case 2:
					$menu_class='nav-fixed hidden-xs hidden-sm nav-home-bottom submenu-up';
				break;
				
				case 3:
					$menu_class='nav-fixed hidden-xs hidden-sm';
				break;
				
				case 4:
					$menu_class='sidebar';
				break;
			}
		?>
		
		<div class="<?php echo $menu_class?>" id="navbar">
			<div class="container">
				
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<?php 
						$logo = weblusive_get_option('logo'); 
						$logosettings =  weblusive_get_option('logo_setting');
					?>
					<a href="<?php echo home_url() ?>" id="logo" class="navbar-brand">
						<?php if($logosettings == 'logo' && !empty($logo)):?>
							<img src="<?php echo $logo ?>" alt="<?php echo get_bloginfo('name')?>" id="logo-image" />
						<?php else:?>
							<?php echo get_bloginfo('name') ?>
						<?php endif?>
					</a>	
				</div>
				<!-- Main navigation -->
				<div class="navbar-collapse collapse">
					<?php $walker = new My_Walker;
						
						if(function_exists('wp_nav_menu')):
							wp_nav_menu( 
							array( 
								'menu' =>'primary_nav', 
								'container'=>'nav', 
								'container_class'=>'main-nav', 
								'depth' => 4, 
								'menu_class' => 'nav navbar-nav navbar-right ',
								'walker' => $walker
								)  
							); 
						else:
							?><ul class="sf-menu top-level-menu"><?php wp_list_pages('title_li=&depth=4'); ?></ul><?php
						endif; 
					?>				 
				</div>
				<div class="clearfix"></div>
			</div>
		</div>

	</header>
	
 <?php endif;?> 					
 <div class="navmobile-container"style="width: 100%; z-index: 100; position: fixed; height: 70px; background-color: #fff;">
                        <img src="wp-content/uploads/2015/03/logo2.png" style="height: 37px;margin-top: 15px;margin-left:10px"> 
                                <button class="mnav" style="margin-top:15px;">
                                <li class="icon-bar"></li>
                                <li class="icon-bar"></li>
                                <li class="icon-bar"></li>
                        </button>

                                        <div class="menu-mob"style="margin-top:18px">
<li><a class="limen" href="http://52.27.27.179/#home">HOME</a></li>
<li><a class="limen" href="http://52.27.27.179/#services">SERVICES</a></li>
<li><a class="limen" href="http://52.27.27.179/#portfolio">PORTFOLIO</a></li>
<li><a class="limen" href="http://52.27.27.179/#about">ABOUT</a></li>
<li><a class="limen" href="http://52.27.27.179/#contact">CONTACT</a></li>
<li><a class="limen" href="?page_id=2327">CAREER</a></li>
<li><a class="limen" href="?page_id=2395" >BLOG</a></li>
                                        </div>
</div>


