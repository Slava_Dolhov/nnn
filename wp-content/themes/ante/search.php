<?php /** The template for displaying Archive pages. **/
get_header('standalone');
$sidebar = weblusive_get_option('sidebar_archive');
$weblusive_sidebar_pos =  weblusive_get_option('sidebar_pos');
?>
<div class="content-dedicated">
	<div id="page-header">
		<div class="container">
			<div class="page-title section-header sh-left">
				<?php get_template_part( 'library/includes/page-head' ); ?>
				<?php if (!weblusive_get_option('hide_titles')):?>
					<h1>
						<span>
							<?php printf( __( 'Search Results for: %s', 'Ante' ), '<mark>' . get_search_query() . '</mark>' ); ?>
						</span>
					</h1>
				<?php endif?>
				<?php if (!weblusive_get_option('hide_breadcrumbs')):?>
					<nav id="breadcrumbs">
						<?php if(class_exists('the_breadcrumb')){ $albc = new the_breadcrumb; } ?>
					</nav>
				<?php endif?>
			</div>
		</div>
	</div>
	<section class="page-content">
		<section class="section">
			<div class="container">
				<div class="row-fluid">	
					<?php if ($weblusive_sidebar_pos == 'left'): get_sidebar(); ?><?php endif?>
					<div class="<?php echo empty($sidebar) ? 'col-md-12' : 'col-md-9';?>">
						<?php if ( have_posts() ) : ?>
							<?php get_template_part( 'loop', 'search' );?>
						<?php else : ?>
							<div id="post-0" class="post no-results not-found">
								<h4><?php _e( 'Nothing Found', 'Ante' ); ?></h4>
								<div class="entry-content">
									<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'Ante' ); ?></p>
								 </div><!-- .entry-content -->
							</div><!-- #post-0 -->
						<?php endif; ?>
					</div>
					<?php if ($weblusive_sidebar_pos == 'right'): get_sidebar(); ?><?php endif?>
				</div>
			</div>
		</section>
	</section>
</div>

<?php get_footer(); ?>