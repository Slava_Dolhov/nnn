<?php /* Template Name: carier2 */

get_header('standalone');
$catid = get_query_var('cat');
$cat = get_category($catid);

$get_meta = get_post_custom($post->ID);
$weblusive_sidebar_pos = isset( $get_meta['_weblusive_sidebar_pos'][0]) ? $get_meta["_weblusive_sidebar_pos"][0] : 'full';
$tagline = isset( $get_meta['_weblusive_tagline'][0]) ? $get_meta["_weblusive_tagline"][0] : '';
$headline = isset( $get_meta['_weblusive_alttitle'][0]) ? $get_meta["_weblusive_alttitle"][0] : '';
?>

<div class="content-dedicated pagecustom-<?php echo $post->ID?>">
	<div id="page-header">
		<div class="container">
			<div class="page-title section-header sh-left">
				<?php get_template_part( 'library/includes/page-head' ); ?>
				<?php if (!weblusive_get_option('hide_titles')):?>
					<h1><span><?php echo (isset($headline) && !empty($headline)) ? $headline : get_the_title(); ?></span></h1>
					<?php echo (isset($tagline) && !empty($tagline)) ? '<div class="header-desc"><span>'.$tagline.'</span></div>' : ''; ?>
				<?php endif?>
				<?php if (!weblusive_get_option('hide_breadcrumbs')):?>
					<nav id="breadcrumbs">
						<?php if(class_exists('the_breadcrumb')){ $albc = new the_breadcrumb; } ?>
					</nav>
				<?php endif?>
			</div>
		</div>
	</div>
	<section class="page-content">
		<section class="section">
			<div class="container">
				<div class="row-fluid">			
					<?php if ($weblusive_sidebar_pos == 'left'): get_sidebar(); ?><?php endif?>
					
					<div class="<?php if ($weblusive_sidebar_pos == 'full'):?>col-md-12<?php else:?>col-md-9<?php endif?>">
						<?php 
						
						$temp = $wp_query;
						$wp_query= null;
						$wp_query = new WP_Query();
						$pp = get_option('posts_per_page');
						$wp_query->query('posts_per_page='.$pp.'&paged='.$paged);			
						get_template_part( 'loop', 'index' );
					
						wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'Ante' ), 'after' => '</div>' ) ); 
						wp_reset_query();
						$blog = get_page($post->ID);
						setup_postdata($blog);
						the_content() ;
					
						rewind_posts();
						?>
					</div>
					
					<?php if ($weblusive_sidebar_pos == 'right'): get_sidebar(); ?><?php endif?>
					<div class="clear"></div>
				</div>
			</div>
		</section>
	</section>
</div>

<?php get_footer(); ?>

