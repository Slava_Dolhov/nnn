<?php /*** The loop that displays posts.***/ 

$get_meta = get_post_custom($post->ID);
$showdate = weblusive_get_option('blog_show_date'); 
$showcomments = weblusive_get_option('blog_show_comments'); 
$showcategory = weblusive_get_option('blog_show_category'); 
$showrmtext = weblusive_get_option('blog_show_rmtext'); 
$counter = 0;
?>

<?php if ( ! have_posts() ) : ?>
	<div id="post-0" class="post error404 not-found">
		<h1 class="entry-title"><?php _e( 'Not Found', 'Ante' ); ?></h1>
		<div class="entry-content">
			<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'Ante' ); ?></p>
			<?php get_search_form(); ?>
		</div><!-- .entry-content -->
	</div><!-- #post-0 -->
<?php else:?>
	
	<?php while ( have_posts() ) : the_post(); 
		$get_meta = get_post_custom($post->ID);
		
		$mediatype = isset($get_meta["_blog_mediatype"]) ? $get_meta["_blog_mediatype"][0] : 'image';
		$videoId = isset($get_meta["_blog_video"]) ? $get_meta["_blog_video"][0] : ''; 
		$autoplay =  isset($get_meta["_blog_videoap"]) ? $get_meta["_blog_videoap"][0] : '0';
		$thumbnail = get_the_post_thumbnail($post->ID, 'portfolio-4-col');
		
		if (!(has_post_thumbnail()) || empty($thumbnail)) $thumbnail = '<img src="http://placehold.it/400x300" alt="'.__('No Image', 'Ante').'" />';
		?>
		<?php if ($counter % 3 == 0):?><div class="row"><?php endif?>
		<div class="col-sm-4">
			<article id="post-<?php the_ID();?>" <?php post_class('latest-post-item fadeBottom'); ?>>
				
				<?php if($mediatype== 'image' && !empty($thumbnail)):?>
					<a href="<?php the_permalink()?>"><?php the_post_thumbnail('blog-mini'); ?></a>
				<?php elseif ( $mediatype == "youtube" && $videoId):?>
					<iframe width="360" height="270" src="http://www.youtube.com/embed/<?php echo $videoId?>?autoplay=<?php echo $autoplay?>" class="vid iframe-youtube"></iframe>
				<?php elseif ( $mediatype == "vimeo" && $videoId):?>
					<iframe width="360" height="270" src="http://player.vimeo.com/video/<?php echo $videoId?>?autoplay=<?php echo $autoplay?>" class="vid iframe-vimeo"></iframe>
				<?php elseif ( $mediatype == "dailymotion" && $videoId):?>
					<iframe width="360" height="270" src="http://www.dailymotion.com/embed/video/<?php echo $videoId?>?autoplay=<?php echo $autoplay?>" class="vid dailymotion-vimeo"></iframe>
				<?php elseif ( $mediatype == "veoh" && $videoId):?>
					<iframe width="360" height="270" src="http://www.veoh.com/static/swf/veoh/SPL.swf?videoAutoPlay=<?php echo $autoplay?>&permalinkId=<?php echo $videoId?>" class="vid iframe-veoh"></iframe>
				<?php elseif ( $mediatype == "bliptv" && $videoId):?>
					<iframe width="360" height="270" src="http://a.blip.tv/scripts/shoggplayer.html#file=http://blip.tv/rss/flash/<?php echo $videoId?>?autoplay=<?php echo $autoplay?>" class="vid iframe-bliptv"></iframe>
				<?php elseif ( $mediatype == "viddler" && $videoId):?>
					<iframe width="360" height="270" src="http://www.viddler.com/embed/<?php echo $videoId?>e/?f=1&offset=0&autoplay=<?php echo $autoplay?>" class="vid iframe-viddler"></iframe>
				<?php elseif($mediatype== 'slider' && !empty($thumbnail)):?>
					<div class="flexslider">
						<ul class="slides">
						<?php 
						$argsThumb = array(
							'order'          => 'ASC',
							'post_type'      => 'attachment',
							'post_parent'    => $post->ID,
							'post_mime_type' => 'image',
							'post_status'    => null,
							//'exclude' => get_post_thumbnail_id()
						);
						$attachments = get_posts($argsThumb);
						if ($attachments) {
							foreach ($attachments as $attachment) {
								echo '<li><img src="'.wp_get_attachment_url($attachment->ID, 'full', false, false).'" alt="'.get_post_meta($attachment->ID, '_wp_attachment_image_alt', true).'" /></li>';
							}
						}?>
						</ul>
					</div>
				<?php else:?>
					<?php  echo $thumbnail; ?>
				<?php endif?>
				<h4><a href="<?php the_permalink()?>"><?php the_title()?></a></h4>
				<ul class="post-info">
					<?php if($showcategory): ?>				
						<?php if ( count( get_the_category() ) ) :
							$category = get_the_category();  if($category[0]):?>
							<li>
								<a href="<?php echo get_category_link($category[0]->term_id )?>"><?php echo $category[0]->cat_name?></a>
							</li>
							<?php endif?>
						<?php endif?>
					<?php endif?>
					<?php if( 'open' == $post->comment_status && $showcomments) : ?>   
						<li>
							<?php comments_popup_link( __( '0 Comments', 'Ante' ), __( '1 Comment', 'Ante' ), __( '% Comments', 'Ante' )); ?>
						</li>
					<?php endif?>
					<?php if($showdate): ?>
						<li>
							<a href="<?php echo get_day_link(get_the_time('Y'), get_the_time('m'), get_the_time('d')); ?>">
								<?php echo get_the_time('M d, Y'); ?>
							</a>
						</li>
					<?php endif?>
				</ul>
				<p><?php echo do_shortcode(limit_words(get_the_excerpt(), 20)); ?></p>
				<div class="read-more-btn">
					<a href="<?php the_permalink()?>" class="btn btn-link">
						<?php echo (isset($showrmtext)&& !empty($showrmtext)) ? $showrmtext : 'Read More';?>
					</a>
				</div>
				
			</article>
		</div>
		<?php if ($counter > 0 && $counter % 2 == 0):?><div class="clearfix"></div></div><?php endif?>
	<?php $counter++; endwhile;?>
<?php endif; ?>
<?php $pagination = weblusive_get_option('blog_op_pagination'); if ( $wp_query->max_num_pages > 1 && $pagination == 1): ?>
	<div class="pagination-list blog-pagination">
		<?php include(Ante_PLUGINS . '/wp-pagenavi.php' ); wp_pagenavi(); ?> 
		<div class="clear"></div>
	</div>
<?php endif?>	
<div class="clear"></div>