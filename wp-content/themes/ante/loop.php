<?php /*** The loop that displays posts.***/ 

$get_meta = get_post_custom($post->ID);
$showdate = weblusive_get_option('blog_show_date'); 
$showcomments = weblusive_get_option('blog_show_comments'); 
$showcategory = weblusive_get_option('blog_show_category'); 
$showrmtext = weblusive_get_option('blog_show_rmtext'); 
?>

<?php if ( ! have_posts() ) : ?>
	<div id="post-0" class="post error404 not-found">
		<h1 class="entry-title"><?php _e( 'Not Found', 'Ante' ); ?></h1>
		<div class="entry-content">
			<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'Ante' ); ?></p>
			<?php get_search_form(); ?>
		</div><!-- .entry-content -->
	</div><!-- #post-0 -->
<?php else:?>
	<?php while ( have_posts() ) : the_post(); 
		$get_meta = get_post_custom($post->ID);
		
		$mediatype = isset($get_meta["_blog_mediatype"]) ? $get_meta["_blog_mediatype"][0] : 'image';
		$videoId = isset($get_meta["_blog_video"]) ? $get_meta["_blog_video"][0] : ''; 
		$autoplay =  isset($get_meta["_blog_videoap"]) ? $get_meta["_blog_videoap"][0] : '0';
		$thumbnail = get_the_post_thumbnail($post->ID, 'blog-list');
		?>
        <!-- -----------POST FORMAT IMAGE------------------ -->
        <?php if ( ( function_exists( 'get_post_format' ) && 'image' == get_post_format( $post->ID ) )  ) : ?> 
        <article <?php post_class('blog-post photo-post standard-post-format post-container'); ?>>
            <div class="media-wrapper">
                <?php the_post_thumbnail('blog-list'); ?>
                <?php 
                    $full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false); 
                    $link = $full_image[0];
                ?>
                <a  class="zoom" href="<?php echo $link ?>"><?php  _e( '+', 'Ante' ); ?></a>
            </div>
        </article>
        <!-------------POST FORMAT LINK-------------------->
        <?php elseif( ( function_exists( 'get_post_format' ) && 'link' == get_post_format( $post->ID ) )  ) : ?> 
        <article <?php post_class('post-container link-post-format'); ?>>
            <div class="post-header">
				<h2><?php the_title(); ?></h2>
				<?php echo get_the_content(); ?>
            </div>
        </article>
        <!-------------POST FORMAT ASIDE-------------------->
        <?php elseif( ( function_exists( 'get_post_format' ) && 'aside' == get_post_format( $post->ID ) )  ) : ?> 
        <div <?php post_class('blog-post aside-post'); ?>>
            <div class="inner-post">
                <div class="post-content">
                    <h2><?php the_title(); ?></h2>
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
        <!-------------POST FORMAT QUOTE-------------------->
        <?php elseif( ( function_exists( 'get_post_format' ) && 'quote' == get_post_format( $post->ID ) )  ) : ?> 
        <div <?php post_class('blog-post quote-post post-container quote-post-format'); ?>>
            <div class="post-header">
				<?php the_content(); ?>
            </div>
        </div>
        <!-------------POST FORMAT VIDEO-------------------->
        <?php elseif( ( function_exists( 'get_post_format' ) && 'video' == get_post_format( $post->ID ) )  ) : ?> 
        <article <?php post_class('post-container video-post-format'); ?>>
			<div class="media-wrapper">
                <?php if ( $mediatype == "youtube" && $videoId):?>
                    <iframe src="http://www.youtube.com/embed/<?php echo $videoId?>?autoplay=<?php echo $autoplay?>" class="videoembed"></iframe>
                <?php elseif ( $mediatype == "vimeo" && $videoId):?>
                    <iframe src="http://player.vimeo.com/video/<?php echo $videoId?>?autoplay=<?php echo $autoplay?>" class="videoembed"></iframe>
				<?php elseif ( $mediatype == "dailymotion" && $videoId):?>
                    <iframe  src="http://www.dailymotion.com/embed/video/<?php echo $videoId?>?autoplay=<?php echo $autoplay?>" class="videoembed"></iframe>
				<?php elseif ( $mediatype == "veoh" && $videoId):?>
                    <iframe  src="http://www.veoh.com/static/swf/veoh/SPL.swf?videoAutoPlay=<?php echo $autoplay?>&permalinkId=<?php echo $videoId?>" class="videoembed"></iframe>
				<?php elseif ( $mediatype == "bliptv" && $videoId):?>
                    <iframe  src="http://a.blip.tv/scripts/shoggplayer.html#file=http://blip.tv/rss/flash/<?php echo $videoId?>?autoplay=<?php echo $autoplay?>" class="videoembed"></iframe>
				<?php elseif ( $mediatype == "viddler" && $videoId):?>
                    <iframe  src="http://www.viddler.com/embed/<?php echo $videoId?>e/?f=1&offset=0&autoplay=<?php echo $autoplay?>" class="videoembed"></iframe>
                <?php endif;?>
			</div>
			<div class="post-header">
				<h2><a href="<?php the_permalink()?>"><?php the_title()?></a></h2>
				<ul class="post-info">
                     <?php if($showcategory): ?>
                        <li><a class="autor" href="#"><?php the_author() ?></a></li>
                    <?php endif?>
                     <?php if( 'open' == $post->comment_status && $showcomments) : ?>
                     <li>                
                         <?php comments_popup_link( __( '0 Comments', 'Ante' ), __( '1 Comment', 'Ante' ), __( '% Comments', 'Ante' )); ?>                              
                     </li>
                     <?php endif?>
                    <?php if($showdate): ?>
                        <li><a class="date" href="#"><?php echo get_the_time('M d, Y'); ?></a></li>
					<?php endif; ?>
				</ul>
			</div>
			<p><?php echo do_shortcode(get_the_excerpt()); ?></p>
            <div class="read-more-btn">
				<a class="btn btn-link" href="<?php the_permalink()?>"><?php echo (isset($showrmtext)&& !empty($showrmtext)) ? $showrmtext : 'Read More';?></a>				
            </div>
        </article>
		
		<!------------- POST FORMAT AUDIO -------------------->
        <?php elseif( ( function_exists( 'get_post_format' ) && 'audio' == get_post_format( $post->ID ) )  ) : ?> 
        <article <?php post_class('post-container audio-post-format'); ?>>
			<div class="media-wrapper">
                <?php 
					$file = '';
					$media = get_attached_media( 'audio', $post->ID); 
					foreach($media as $m) {
					  $file = wp_get_attachment_url($m->ID);
					}
					echo do_shortcode('[audio mp3="'.$file.'"]');
				?>
			</div>
			<div class="post-header">
				<h2><a href="<?php the_permalink()?>"><?php the_title()?></a></h2>
				<ul class="post-info">
                     <?php if($showcategory): ?>
                        <li><a class="autor" href="#"><?php the_author() ?></a></li>
                    <?php endif?>
                     <?php if( 'open' == $post->comment_status && $showcomments) : ?>
                     <li>                
                         <?php comments_popup_link( __( '0 Comments', 'Ante' ), __( '1 Comment', 'Ante' ), __( '% Comments', 'Ante' )); ?>                              
                     </li>
                     <?php endif?>
                    <?php if($showdate): ?>
                        <li><a class="date" href="#"><?php echo get_the_time('M d, Y'); ?></a></li>
					<?php endif; ?>
				</ul>
			</div>
			<p><?php echo do_shortcode(get_the_excerpt()); ?></p>
            <div class="read-more-btn">
				<a class="btn btn-link" href="<?php the_permalink()?>"><?php echo (isset($showrmtext)&& !empty($showrmtext)) ? $showrmtext : 'Read More';?></a>				
            </div>
        </article>
		
		
         <!-------------POST FORMAT GALLERY-------------------->
        <?php elseif( ( function_exists( 'get_post_format' ) && 'gallery' == get_post_format( $post->ID ) )  ) : ?> 
        <article <?php post_class('post-container slideshow-post-format'); ?>>
           <div class="media-wrapper">
                <?php
                $full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false); 
				$argsThumb = array(
                    'order'          => 'ASC',
                    'posts_per_page'  => 99,
                    'post_type'      => 'attachment',
                    'post_parent'    => $post->ID,
                    'post_mime_type' => 'image',
                    'post_status'    => null,
                    //'exclude' => get_post_thumbnail_id()
				);
				?>
                <div class="flexslider slideshow">
                    <ul class="slides">
						<?php 
						$attachments = get_posts($argsThumb);
						if ($attachments) {
                            foreach ($attachments as $attachment) {?>
								<li>
									<a href="<?php the_permalink()?>">
										<img src="<?php echo wp_get_attachment_url($attachment->ID, 'full', false, false)?>" alt="<?php echo get_the_title($post->ID)?>">
									</a>
								</li>
							<?php }
						}
						?>
                    </ul>
                </div>
			</div>
            <div class="post-header">
				<h2><a href="<?php the_permalink()?>"><?php the_title()?></a></h2>
				<ul class="post-info">
                     <?php if($showcategory): ?>
                        <li><a class="autor" href="#"><?php the_author() ?></a></li>
                    <?php endif?>
                     <?php if( 'open' == $post->comment_status && $showcomments) : ?>
                     <li>             
                         <?php comments_popup_link( __( '0 Comments', 'Ante' ), __( '1 Comment', 'Ante' ), __( '% Comments', 'Ante' )); ?>                              
                     </li>
                     <?php endif?>
                    <?php if($showdate): ?>
                        <li><a class="date" href="#"><?php echo get_the_time('M d, Y'); ?></a></li>
					<?php endif; ?>
				</ul>
			</div>
			<p><?php echo do_shortcode(get_the_excerpt()); ?></p>
			<div class="read-more-btn">
				<a class="btn btn-link" href="<?php the_permalink()?>"><?php echo (isset($showrmtext)&& !empty($showrmtext)) ? $showrmtext : 'Read More';?></a>				
            </div>
        </article>
        <?php else : ?>
        <article <?php post_class('post-container standard-post-format'); ?>>
            <div class="media-wrapper">
               <?php $thumbnail = get_the_post_thumbnail();
				if(!empty($thumbnail)) : ?>
					<?php the_post_thumbnail('blog-list'); ?>
                <?php else : ?>
					<img src = "http://placehold.it/850x400" alt="'.__('No image', 'Ante').'" />
				<?php endif; ?>
			</div>
            <div class="post-header">
				<h2><a href="<?php the_permalink()?>"><?php the_title()?></a></h2>
				<ul class="post-info">
                     <?php if($showcategory): ?>
                        <li><a class="autor" href="#"><?php the_author() ?></a></li>
                    <?php endif?>
                     <?php if( 'open' == $post->comment_status && $showcomments) : ?>
                     <li>             
                         <?php comments_popup_link( __( '0 Comments', 'Ante' ), __( '1 Comment', 'Ante' ), __( '% Comments', 'Ante' )); ?>                              
                     </li>
                     <?php endif?>
                    <?php if($showdate): ?>
                        <li><a class="date" href="#"><?php echo get_the_time('M d, Y'); ?></a></li>
					<?php endif; ?>
				</ul>
			</div>
			<p><?php echo do_shortcode(get_the_excerpt()); ?></p>
			<div class="read-more-btn">
				<a class="btn btn-link" href="<?php the_permalink()?>"><?php echo (isset($showrmtext)&& !empty($showrmtext)) ? $showrmtext : 'Read More';?></a>				
            </div>
        </article>
        <?php endif; ?>
	<?php endwhile;?>
<?php endif; ?>
<?php if ( $wp_query->max_num_pages > 1 ): ?>
	<div class="pagination-list blog-pagination">
		<?php include(Ante_PLUGINS . '/wp-pagenavi.php' ); wp_pagenavi(); ?> 
		<div class="clear"></div>
	</div>
<?php endif?>	
<div class="clear"></div>