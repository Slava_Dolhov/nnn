<?php /* Template Name: Under Construction */ ?>

<?php get_header('standalone');
if (weblusive_get_option('uc_launchdate')):
   $date = explode('/', weblusive_get_option('uc_launchdate'));
	?>
	
	<style type="text/css">body {height:auto !important}</style>
	<script type="text/javascript">
		jQuery(document).ready(function(){ 			
			jQuery('div#clock').countdown("<?php echo $date[0]?>/<?php echo $date[1]?>/<?php echo $date[2]?>", function(event) {
				var $this = jQuery(this);
				switch(event.type) {
					case "seconds":
					case "minutes":
					case "hours":
					case "days":
					case "weeks":
					case "daysLeft":
						$this.find('span#'+event.type).html(event.value);
					break;
					case "finished":
					$this.hide();
						break;
				}
			});
		}); 
	</script> 
	<?php 
endif;
?>

<?php
	$get_meta = get_post_custom($post->ID);
	$weblusive_sidebar_pos = isset( $get_meta['_weblusive_sidebar_pos'][0]) ? $get_meta["_weblusive_sidebar_pos"][0] : 'full';
	
?>
<div class="content-dedicated pagecustom-<?php echo $post->ID?>">
	<div id="uc-content" class="content ">
		<div class="container">
			<?php get_template_part( 'library/includes/page-head' ); ?>
			<div class="row-fluid">
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
					<div class="<?php if ($weblusive_sidebar_pos == 'full'):?>col-md-8<?php else:?>col-md-4<?php endif?> col-md-offset-2">
						
						<?php if(weblusive_get_option( 'uc_maintitle' )):?>
							<h1 class="uc-maintitle">
								<?php echo weblusive_get_option( 'uc_maintitle' ) ?>
							</h1>
						<?php endif?>
						<?php if(weblusive_get_option( 'uc_text' )):?>
							<div class="uc-text">
								<?php echo weblusive_get_option( 'uc_text' ) ?>
							</div>
						<?php endif?>
						<?php if(weblusive_get_option( 'uc_progress' )):?>
							<div class="progress progress-striped active">
								<div class="progress-bar progress-bar-success" style="width:<?php echo weblusive_get_option( 'uc_progress' )?>"><i class="icon-gears"></i></div>
							</div>
						<?php endif?>
						<div id="clock">
							<div class="row-fluid">
								<div class="col-md-2">
									<p><span id="weeks"></span><?php _e('Weeks', 'Ante')?></p>
								</div>
								<div class="col-md-2">
									<p><span id="daysLeft"></span><?php _e('Days', 'Ante')?></p>
								</div>                    
								<div class="col-md-2">
									<p><span id="hours"></span><?php _e('Hours', 'Ante')?></p>
								</div>
								<div class="col-md-2">
									<p><span id="minutes"></span><?php _e('Minutes', 'Ante')?></p>
								</div>
								<div class="col-md-2">
									<p><span id="seconds"></span><?php _e('Seconds', 'Ante')?></p>
								</div>
							</div>
						</div>
						
						<?php the_content(); ?>
					</div>
				<?php endwhile; ?>	
				<?php get_sidebar(); ?>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>
<?php get_footer()?>