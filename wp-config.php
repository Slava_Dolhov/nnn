<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'kindgeek');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7~zf!aokjD<q2d[U-nW*!i2xg|s;,ahU)/fk@CoR8!Th;Yt+{uZZeNrddTqi+I#}');
define('SECURE_AUTH_KEY',  'BT-j~3](9ZNy4D@?@Lu0~o5p rgaazQs9=I{lSia,+/166DA)jBzr`;+h/q*@w/v');
define('LOGGED_IN_KEY',    'hEw6q*I.;)Cf[aD5L#JBkJ$G _w)Z:njg]?#w{n0u#kd L(ZLA@ygy.+(fj[h|n-');
define('NONCE_KEY',        'y0ggyc!1Kf.PK3t1C>Sh#|R1JZ:Sz:8<YL+TkGqW(lb>TF5+O8B^E^X0.&g{Yo1F');
define('AUTH_SALT',        '2]Ec)/k[Npc*J,GJ(#x67d8:w$&:AM=2Qh[?j)ONrM@WjlHPd&!GTiS37E5|_;ox');
define('SECURE_AUTH_SALT', '~G2QY}0$~(m1<ho8Y=7@2#Vu6374Rv]c?)cTq:5Ki?uT<5||rHQV6L%Nu8Z;:Mj(');
define('LOGGED_IN_SALT',   'E3uM6|d{4?g0ahd%]-|Th {92OJbbIRs1wcKnR!I#QvsVYJ*/v}pLZ0yw=QBqy~4');
define('NONCE_SALT',       'S29e:8|>7_4OHb>-8p(W?L>ui/@GYFYm>xL$NeNGS2Ms6D{v0z>>hhZns~k*w,a{');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'kg_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
